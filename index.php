<?php
include("init.php");

$par1 = "";
$par2 = "";
$par3 = "";
$par4 = "";
$par5 = "";
$par6 = "";
$par7 = "";
$par8 = "";

foreach($_GET as $k => $v){
	${$k} = $v;
}

switch($par1){

	case "index":
		include(ROOT_DIR."templates/index.php");
		break;

	case "preview":
		$idBanner = $par2;
		include(ROOT_DIR."templates/preview.php");
		break;

	case "admin":
		$page= "admin";
		if(!empty($par2)){
			switch ($par2){
				case "projects":
					$listCampagna = new CampagnaList($connection);
					$listCampagna->init();
					
					$arrayProject = $listCampagna->toArray();
					
					$subpage = "projects";
					break;
				case 'new':
					$subpage = "new";
					break;
				case "new-campaign":
					$subpage = "new-campaign";
					break;
				case "new-banner":
					$subpage = "new-banner";
					break;
				case "new-static":
					$subpage = "new-static";
					break;
				case "campaign":
					$subpage = "campaign";
					break;
				case "banner":
					$subpage = "banner";
					break;
				case "static":
					$subpage = "static";
					break;
				default:
					$subpage = "banner-single";
					$objCampagna = new Campagna($connection);
					$objCampagna->init($par2);
					
					$arrayBanner = $objCampagna->getArrayBanner();
					$objBanner = array_values($arrayBanner)[0];
					
					if(count($arrayBanner)>1){
						$subpage = "banner-campaign";
					}
					
					break;
			}
		
			if(!empty($par3)){
				
				if(is_numeric($par3)){
					$objCampagna = new Campagna();
					$objCampagna->init($par3);
					$campagnaVar= $objCampagna->getVariables();
				}
				switch($par3){
					case "preview":
						$subpage = "preview";
						$tempList = new TemplateList();
						$tempList->init();
						$arrayTemplate = $tempList->toArray();
						break;
					case "export":
						$subpage = "export";
						break;
					default:
						switch($par2){
							case "new-campaign":
								$subpage = "new-campaign";							
								break;
							case "new-banner":
								$subpage = "new-banner";
								$arrayBanner = $objCampagna->getArrayBanner();
								$objBanner = array_values($arrayBanner)[0];
								break;
							case "new-static":
								$subpage = "new-static";
								break;
							default:
								$subpage = "banner-single";
								break;
						}						
					break;
				}
			}
		} else{
			$subpage = "dashboard";
			$listBanner = new BannerList($connection);
			$listBanner->init();
		}
		include(ROOT_DIR."templates/admin.php");
		break;
	case "canvas":
	
		if($par2 && $par4 != null && count(explode("x",$par4)) == 2){
			$sizes = explode("x",$par4);
			$width = $sizes[0];
			$height = $sizes[1];
			if(is_numeric($width) && is_numeric($height) && $width >= 0 && $height >= 0){
				$template = $par3;
				include(ROOT_DIR."banner/bannerCanvas.php");
			}else{
				include(ROOT_DIR."templates/404.php");
			}
		}else{
			include(ROOT_DIR."templates/404.php");
		}
		break;
	case "banner":
		$scriptVariable = "banner=".$par2."&test=" . IS_TEST;
		include(ROOT_DIR."banner/bannerCanvas.php");
		break;
	case "camp":
		$scriptVariable = "camp=".$par2 . parseVariableContinuos($par3,$par4) . parseVariableContinuos($par5,$par6). parseVariableContinuos($par7,$par8)."&test=" . IS_TEST;
		include(ROOT_DIR."banner/bannerCanvas.php");
		break;
	default:
		$objBanner = new Banner($connection);
		$objBanner->init($par1);
		if($objBanner->isLoaded() && $par3 != null && count(explode("x",$par3)) == 2){
			$sizes = explode("x",$par3);
			$width = $sizes[0];
			$height = $sizes[1];
			if(is_numeric($width) && is_numeric($height) && $width >= 0 && $height >= 0){
				$template = $par2;
				include(ROOT_DIR."banner/banner.php");
			}else{
				include(ROOT_DIR."templates/404.php");
			}
		}else{
			include(ROOT_DIR."templates/404.php");
		}
		break;
}

function parseVariableContinuos($pK = null,$pV = null){
	$r = "";
	if(isset($pK) && $pK != null && isset($pV) && $pV != null){
		switch($pK){
			case "width":
				if(is_numeric($pV)){
					$r = "&w=".intval($pV);
				}
				break;
			case "height":
				if(is_numeric($pV)){
					$r = "&h=".intval($pV);
				}
				break;
			case "template":
				if(is_numeric($pV)){
					$r = "&template=".intval($pV);
				}
				break;
		}
	}
	return $r;
}