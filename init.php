<?php
session_start();
ob_start();
header('Content-type: text/html; charset=utf-8');

define("ROOT_DIR", dirname(__FILE__)."/");
include(ROOT_DIR."config.php");


/*inclusione classi*/
require_once(ROOT_DIR."classes/Database.class.php");
require_once(ROOT_DIR."classes/DatabaseTable.class.php");
require_once(ROOT_DIR."classes/Utility.class.php");
require_once(ROOT_DIR."classes/NullClass.class.php");
require_once(ROOT_DIR."classes/Banner.class.php");
require_once(ROOT_DIR."classes/BannerPreview.class.php");
require_once(ROOT_DIR."classes/Formato.class.php");
require_once(ROOT_DIR."classes/Campaign.class.php");



/*inizializzazione db*/
$connection = new Database();
$connection->connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);


define("ADMIN_PATH", ROOT_PATH . "admin/");
define("ASSET_PATH", ROOT_PATH . "assets/");
define("ASSET_DIR", ROOT_DIR . "assets/");