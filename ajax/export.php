<?php
include("../init.php");
error_reporting(0);

function get_data($url) {
	$ch = curl_init();
	$timeout = 120;
	$useragent = 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:10.0.2) Gecko/20100101 Firefox/10.0.2';

	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_AUTOREFERER, true);
	curl_setopt($ch, CURLOPT_REFERER, 'http://www.google.com');
	curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_FAILONERROR, true);
	curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
	
	$data = curl_exec($ch);
	#$data = @iconv(mb_detect_encoding($data, mb_detect_order(), true), "UTF-8", $data);
	curl_close($ch);
	return $data;
}

function generateBanner($array_size, $fileFormat, $timefolder){
	$min_vw = 320;
	$min_vh = 160;
	
	
	foreach($array_size as $k=>$v){
	
		$array_size = explode("x", $v);
	
		$w = $array_size[0];
		$h = $array_size[1];
	
		//check and fix min width/height due to api limit
		if($min_vw>$w){
			$ratio = ($min_vw/$w);
			$w = $min_vw; //320
			$h = round($h * $ratio); //106,6
				
			if($min_vh > $h){
				$ratio = ($min_vh/$h);
				$h=$min_vh; //160
				$w=round($w*$ratio);
			}
		}
	
		if($min_vh>$h){
			$ratio = ($min_vh/$h);
			$h = $min_vh;
			$w = round($w * $ratio);
	
			if($min_vw > $w){
				$ratio = ($min_vw/$w);
				$w=$min_vw;
				$h=round($h*$ratio);
			}
		}
		
		//rest pack api
		$image = get_data('https://restpack.io/api/screenshot/v2/capture?access_token=tiEkIFk7hfZ6SZ6q5OClZJgV7vAlN0geCUjzuxCQToTOscJm&url='.ROOT_PATH."camp/".$_REQUEST['bannerId'].'/width/'.$w.'/height/'.$h.'&format='.$fileFormat.'&width='.$w.'&height='.$h.'&fresh=true');
		#echo $image;
		file_put_contents(ROOT_DIR.'assets/export/images/'.$timefolder.'/'.$fileFormat.'/'.$v.'.'.$fileFormat, $image);
	
		//image smaller than viewport
		if($w>$array_size[0] && $h >$array_size[1]){
			$src=ROOT_DIR.'assets/export/images/'.$timefolder.'/'.$fileFormat.'/'.$v.'.'.$fileFormat;
			$typ='c';
			$siz = $array_size;
			$overwrite=1;
			$qua = 100;
			$log = false;
				
			include(ROOT_DIR."imageresample_core.php");
				
		}
		$GLOBALS['status']['current']++;
		$fp = fopen($GLOBALS['statusFilename'], "w");
		fwrite($fp, utf8_encode(json_encode($GLOBALS['status'])));
		fclose($fp);
		copy('status1.json', 'status.json');
	}
}

//json progress circle
$GLOBALS['statusFilename'] = 'status1.json';
$fp = fopen($GLOBALS['statusFilename'], "w");
$GLOBALS['status'] = array('total'=>'0', 'current'=>'0');
fwrite($fp, json_encode($GLOBALS['status']));
fclose($fp);

$totalSizeFormat = count($_POST['formSizeFormat']);
foreach($_POST['formFileFormat'] as $fileFormat){
	switch($fileFormat){
		case "jpg":
			$GLOBALS['status']['total']+= $totalSizeFormat;
			break;
		case "png":
			$GLOBALS['status']['total']+= $totalSizeFormat;
			break;
		case 'public-url':
			$GLOBALS['status']['total']++;
			break;
	}
}

$timefolder = time();
mkdir(ROOT_DIR.'assets/export/images/'.$timefolder, 0777);

foreach($_POST['formFileFormat'] as $fileFormat){
	switch($fileFormat){
		case "jpg":
			mkdir(ROOT_DIR.'assets/export/images/'.$timefolder.'/jpg', 0777);
			generateBanner($_POST['formSizeFormat'], "jpg", $timefolder);
			break;
		case "png":
			mkdir(ROOT_DIR.'assets/export/images/'.$timefolder.'/png', 0777);
			generateBanner($_POST['formSizeFormat'], "png", $timefolder);
			break;
		case "public-url":
			file_put_contents(ROOT_DIR.'assets/export/images/'.$timefolder.'/public-url.txt', ROOT_PATH."assets/export/files/archive_$timefolder.zip");
			$GLOBALS['status']['current']++;
			$fp = fopen($GLOBALS['statusFilename'], "w");
			fwrite($fp, utf8_encode(json_encode($GLOBALS['status'])));
			fclose($fp);
			copy('status1.json', 'status.json');
			break;
	}
}

//zip folder
// Get real path for our folder
$rootPath = ROOT_DIR."assets/export/images/$timefolder";

// Initialize archive object
$zip = new ZipArchive();
$zip->open(ROOT_DIR.'assets/export/files/archive_'.$timefolder.'.zip', ZipArchive::CREATE | ZipArchive::OVERWRITE);

// Create recursive directory iterator
/** @var SplFileInfo[] $files */
$files = new RecursiveIteratorIterator(
		new RecursiveDirectoryIterator($rootPath),
		RecursiveIteratorIterator::LEAVES_ONLY
		);

foreach ($files as $name => $file)
{
	// Skip directories (they would be added automatically)
	if (!$file->isDir())
	{
		// Get real and relative path for current file
		$filePath = $file->getRealPath();
		$relativePath = substr($filePath, strlen($rootPath) + 1);

		// Add current file to archive
		$zip->addFile($filePath, $relativePath);
	}
}

// Zip archive will be created only after closing object
$zip->close();
#array_map('unlink', glob(ROOT_DIR.'tmp/images/*'));

echo "assets/export/files/archive_$timefolder.zip";

$fp = fopen($GLOBALS['statusFilename'], "w");
$GLOBALS['status'] = array('total'=>'100', 'current'=>'0');
fwrite($fp, json_encode($GLOBALS['status']));
fclose($fp);

copy('status1.json', 'status.json');

?>