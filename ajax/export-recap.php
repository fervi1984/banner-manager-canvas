<?php include("../init.php");
$objCampagna = new Campagna($connection);
$objCampagna->initByAlpha($_REQUEST['bannerId']);

if($objCampagna->getType()=='campaign'){	
	$lstFormati = $objCampagna->getArrayBanner();
}else{
	$lstFormati = new FormatoList ( $connection );
	$lstFormati->initAllVisible();
	$lstFormati = $lstFormati->toArray ();
}

?>
<div class="container fancy-content-default export-recap">
	<div class="row">
	<?php if (count($lstFormati)>0){ ?>
		<div class="col col-3 text-left">
			<input type="hidden" name="bannerId" id="bannerId" value="<?php echo $_REQUEST['bannerId'] ?>" />
			<form method="post" action="" id="size-format-form">
				<ul>
				<?php					
					foreach($lstFormati as $objFormato){
						?>
						<li>
							<input type="checkbox" name="size-format[]" value="<?php echo $objFormato->getWidth(); ?>x<?php echo $objFormato->getHeight(); ?>">
							<label><?php echo $objFormato->getWidth(); ?>x<?php echo $objFormato->getHeight(); ?></label>
						</li>
						<?php
					}
				?>
				</ul>
			</form>
		</div>
	<?php } ?>
		<div class="col <?php echo (count($lstFormati)>0)?"col-3":"col-6" ?>">
			<form method="post" action="" id="file-format-form">
				<ul>
					<li>
						<input type="checkbox" name="file-format[]" value="jpg">
						<label>JPG</label>
					</li>
					<li>
						<input type="checkbox" name="file-format[]" value="png">
						<label>PNG</label>
					</li>
					<li>
						<input type="checkbox" name="file-format[]" value="pdf">
						<label>PDF</label>
					</li>
					<li>
						<input type="checkbox" name="file-format[]" value="embed-code">
						<label>EMBED CODE</label>
					</li>
					<li>
						<input type="checkbox" name="file-format[]" value="public-url">
						<label>PUBLIC URL</label>
					</li>
				</ul>
			</form>
		</div>
		<div class="col col-6 d-flex">
			<div class="percircle">
				<div id="bluecircle" class="c100 p0 big">
                    <span class="percNumb">0%</span>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>
            </div>
			<div class="align-self-end col-12">
				<div class="row"><div class="col-6"><div id="close" data-fancybox-close class="col-12 btn btn-secondary">CANCEL</div></div><div class="col-6"><div id="export-selected" class="col-12 btn btn-success">EXPORT SELECTED</div></div></div>
			</div>
		</div>
	</div>
</div>
<script style="display: none">
$(function(){
	updatePerCircle();

	function updateStatus(){      
        $.getJSON(ROOT_PATH+'ajax/status.json', function(data){ 
            var items = [];
            pbvalue = 0;
            
            if(data){
                var total = data['total'];                
                var current = data['current']; 
                console.log('total:'+total+',current:'+current);
                var pbvalue = Math.floor((current / total) * 100); 
                if(pbvalue>0){ 
                	$(".percircle .percNumb").html(pbvalue+"%");
                	$(".percircle #bluecircle").attr('class',$('#bluecircle').attr('class').replace(/\bp.*?\b/g, ''));
                	$(".percircle #bluecircle").addClass('p'+pbvalue);
                	updatePerCircle();
                } 
            } 

            if(pbvalue < 100){ 
                t = setTimeout(updateStatus(), 200); 
            } 
        });   
    }
    
	$("#export-selected").off().on('click', function(event) {
		event.stopPropagation(); // Stop stuff happening
		event.preventDefault(); // Totally stop stuff happening
		var urlAjax = ROOT_PATH+'ajax/export.php';
		var formSizeFormat = [];
		<?php if(count($lstFormati)>0){ ?>
			formSizeFormat = $("#size-format-form").find("input[name='size-format[]']:checked").map(function(){
			    return this.value;
			}).get();
		<?php } else { 
			$arrayBanner = $objCampagna->getArrayBanner();
			$objBanner = array_values($arrayBanner)[0];
			?>
			formSizeFormat.push("<?php echo $objBanner->getWidth()."x".$objBanner->getHeight(); ?>");
		<?php } ?>
		
		var formFileFormat = $("#file-format-form").find("input[name='file-format[]']:checked").map(function(){
		    return this.value;
		}).get();

		$.ajax({
			url: urlAjax, 
			type:"POST", 
			data: {formSizeFormat:  formSizeFormat, formFileFormat: formFileFormat, bannerId: '<?php echo $_REQUEST['bannerId'] ?>' },
			success: function(archivePath){
				window.location.href = ROOT_PATH+"autodownload.php?archive="+archivePath;
			}
		});

		/*t = setTimeout(function(){
		updateStatus()
	}, 2000);*/

		
		return false;
	});	
});

</script>