<?php

define("NOLANG",true);
require_once("../init.php");

if(array_key_exists("fileName",$_POST) && array_key_exists("width",$_POST) && array_key_exists("height",$_POST)){
    //recupero i dati del file
    $filePath = ROOT_DIR ."/assets/".$_POST["fileName"];
    if(file_exists($filePath)){
      $objXML = simplexml_load_file($filePath);
      if($objXML->getName() == "svg"){
        
        $attributesObj = $objXML->attributes();
        $attributesArray = (array) $attributesObj;
        $attributesArray = $attributesArray["@attributes"];
        
        if(array_key_exists("width",$attributesArray) && array_key_exists("height",$attributesArray)){
          //ci sono tutti gli elementi ..
          print_r("ci sono tutti gli elementi ..");
          exit(0);
        }else{
            if(!array_key_exists("width",$attributesArray)){
                $objXML->addAttribute("width",$_POST["width"]."px");
            }
            if(!array_key_exists("height",$attributesArray)){
                $objXML->addAttribute("height",$_POST["height"]."px");
            }
            if($objXML->asXML($filePath)){
                echo "file salvato";
            }else{
                echo "file NON salvato";
            }
            exit(0);
        }
        
      }
      
    }else{
      echo "non trovo il file $filePath";
      exit(0);
    }
    
}


echo "nessun file";