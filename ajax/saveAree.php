<?php
include("../init.php");

$idAsset = $_REQUEST["idAsset"];
$arrAree = $_REQUEST["arrAree"];
$objAsset = new Asset($connection);
$objAsset->init($idAsset);
$objAsset->removeAree();

foreach($arrAree as $area){
	$objAsset->addArea($area["topleftX"], $area["topleftY"], $area["bottomrightX"], $area["bottomrightY"], 1);
}

$response["success"] = true;			
$response["result"] = "OK";

echo json_encode($response);
?>