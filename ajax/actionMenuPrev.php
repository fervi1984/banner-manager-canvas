<?php
include("../init.php");
$bannerPreview = new BannerPreview($connection);
if(array_key_exists('idPreview', $_POST)){
	$bannerPreview->init($_POST['idPreview']);
}

switch($_REQUEST['type']){
	case 'delete-preview':
		$bannerPreview->delete();
		break;
	case 'save-preview':
		echo $bannerPreview->saveUpdate($_POST);
		break;
	case 'open-preview':
		$bannerPreviewList = new BannerPreviewList($connection);
		$bannerPreviewList->initByBannerId($_GET['bannerId']);
		$bannerList = $bannerPreviewList->toArray();
		
		include("../templates/open-preview.php");
		break;
	case 'get-saved-preview':
		echo json_encode(array('html' => $bannerPreview->getHtml(), 'url' => $bannerPreview->getUrl()));
		break;
}