<?php
include("../init.php");

$campagna = new Campagna();
$banner = new Banner();



switch($_REQUEST['type']){
	case 'banner':
		
		switch($_REQUEST['action']){
			case 'delete':
				$campagna->init($_REQUEST['idBanner']);
				$campagna->delete();
				break;
			case 'save-single':
				if(array_key_exists("idBanner", $_REQUEST))
					$banner->init($_REQUEST['idBanner']);
				
				if(array_key_exists("idFormato", $_REQUEST)){
					$banner->setIdFormato($_REQUEST['idFormato']);
				}
				if(array_key_exists("isCustom", $_REQUEST)){
					$banner->setCustom($_REQUEST['isCustom']);
				}
				if(array_key_exists("width", $_REQUEST))
					$banner->setWidth($_REQUEST['width']);
				
				if(array_key_exists("height", $_REQUEST))
					$banner->setHeight($_REQUEST['height']);
			
				$idBanner = $banner->save();
				
				echo $idBanner;
				
				break;
			case 'save':
				$bgUrlArray = explode("assets/",$_REQUEST['fileBanner']);
				$bgUrl = rawurldecode(end($bgUrlArray));
				$logoUrlArray = explode("assets/",$_REQUEST['fileLogo']);
				$logoUrl = rawurldecode(end($logoUrlArray));
				
				$headText = '["'.$_REQUEST['headText'].'"]';
				$subHeadText = '["'.$_REQUEST['subHeadText'].'"]';
				
				$webSiteText = '["'.$_REQUEST['webSiteText'].'"]';
				$ctaText = '["'.$_REQUEST['ctaText'].'"]';
				
				if(!empty($_REQUEST['idCampagna']))
					$campagna->init($_REQUEST['idCampagna']);
				
				$campagna->setType('banner');
				$campagna->setName($_REQUEST['campaignName']);
				$campagna->setBgUrl($bgUrl);
				$campagna->setLogoUrl($logoUrl);
				$campagna->setColorBg($_REQUEST['bgBanner']);
				$campagna->setHeadText($headText);
				$campagna->setSubHeadText($subHeadText);
				if(!empty($_REQUEST['webSiteText']))
					$campagna->setWebSiteText($webSiteText);
				$campagna->setWebSiteUrl($_REQUEST['webSiteUrl']);
				if(!empty($_REQUEST['ctaText']))
					$campagna->setCtaText($ctaText);
				$campagna->setCtaUrl($_REQUEST['ctaUrl']);
				$campagna->setCoordMainAreaBanner($_REQUEST['coordMainAreaBanner']);
				
				$idCamp = $campagna->save();
				
				if(!empty($_REQUEST['idBanner']))
					$banner->init($_REQUEST['idBanner']);
				
				$banner->setIdCampagna($idCamp);
				
				$idBanner = $banner->save();
				
				echo $idCamp;
				
				break;
		}
		
		break;
	case 'campaign':
		
		switch($_REQUEST['action']){
			case 'delete':
				$campagna->init($_REQUEST['idBanner']);					
				$campagna->delete();
				break;
			case 'delete-single':
				$banner->init($_REQUEST['idBanner']);
				$banner->delete();
			break;
			case 'save-single':
				if(array_key_exists("idBanner", $_REQUEST))
					$banner->init($_REQUEST['idBanner']);
				
				if(array_key_exists("idFormato", $_REQUEST)){
					$banner->setIdFormato($_REQUEST['idFormato']);
				}
				if(array_key_exists("isCustom", $_REQUEST)){
					$banner->setCustom($_REQUEST['isCustom']);
				}
				if(array_key_exists("width", $_REQUEST))
					$banner->setWidth($_REQUEST['width']);
				
				if(array_key_exists("height", $_REQUEST))
					$banner->setHeight($_REQUEST['height']);
			
				$idBanner = $banner->save();
				
				echo $idBanner;
			break;
			/*case 'save-coord':
				$campaign->saveCoord($_REQUEST['idBanner'], $_REQUEST['fieldName'], $_REQUEST['cropCoord']);
				break;*/			
			case 'save':
				$bgUrl = rawurldecode(end(explode("assets/",$_REQUEST['fileBanner'])));
				$logoUrl = rawurldecode(end(explode("assets/",$_REQUEST['fileLogo'])));
				
				$webSiteText = '["'.$_REQUEST['webSiteText'].'"]';
				$ctaText = '["'.$_REQUEST['ctaText'].'"]';
				$headText = '["'.$_REQUEST['headText'].'"]';
				$subHeadText = '["'.$_REQUEST['subHeadText'].'"]';
				
				if(!empty($_REQUEST['idCampagna']))
					$campagna->init($_REQUEST['idCampagna']);
				
				$campagna->setType('campaign');
				if(array_key_exists("idFormato", $_REQUEST))
					$campagna->setIdFormato(0);
				$campagna->setName($_REQUEST['campaignName']);
				$campagna->setBgUrl($bgUrl);
				$campagna->setLogoUrl($logoUrl);
				$campagna->setColorBg($_REQUEST['bgBanner']);
				$campagna->setHeadText($headText);
				$campagna->setSubHeadText($subHeadText);
				
				if(!empty($_REQUEST['webSiteText']))
					$campagna->setWebSiteText($webSiteText);
				$campagna->setWebSiteUrl($_REQUEST['webSiteUrl']);
				if(!empty($_REQUEST['ctaText']))
					$campagna->setCtaText($ctaText);
				$campagna->setCtaUrl($_REQUEST['ctaUrl']);
				$campagna->setCoordMainAreaBanner($_REQUEST['coordMainAreaBanner']);
				
				$idCamp = $campagna->save();
				
				if(isset($_REQUEST['formatList'])){
					foreach($_REQUEST['formatList'] as $formatData){
						if($formatData['width'] && $formatData['height']){
							$banner = new Banner();
							
							if($formatData['idBanner']>0)
								$banner->init($formatData['idBanner']);
							
							$banner->setIdCampagna($idCamp);
							$banner->setIdFormato(null);
							$banner->setCustom(1);
							$banner->setName($formatData['name']);
							$banner->setWidth($formatData['width']);
							$banner->setHeight($formatData['height']);
							
							$idBanner = $banner->save();
						}
					}		
				}
				
				
				
				echo $idCamp;
				
				break;
		}
		
		break;
}
