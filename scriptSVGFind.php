<?php
define("NOLANG",true);
require_once("init.php");
?>
<html>
  <head>
    <script src="http://code.jquery.com/jquery-latest.js"></script>
  </head>
  <body>
    
    <?php
    
    $fileName = isset($_GET["nomeFile"]) ? $_GET["nomeFile"] : false;
    if($fileName && strpos($fileName,".svg")!==false){
      $filePath = __DIR__ ."/assets/$fileName";
      if(file_exists($filePath)){
        print_r("esiste il file");
        print_r("<div id=\"p\">");
        $objXML = simplexml_load_file($filePath);
        if($objXML->getName() == "svg"){
          
          $attributesObj = $objXML->attributes();
          $attributesArray = (array) $attributesObj;
          $attributesArray = $attributesArray["@attributes"];
          
          if(array_key_exists("width",$attributesArray) && array_key_exists("height",$attributesArray)){
            //ci sono tutti gli elementi ..
            print_r("ci sono tutti gli elementi ..");
            exit(0);
          }
          
        }
        
        print_r(file_get_contents($filePath));
        print_r("</div>");
      }else{
        print_r("non esiste il file");
      }
    }
    
    ?>
    <script>
      var p = document.getElementById("p");
      var svg = p.childNodes[0];
      var dimension = svg.getBBox();
      console.log(dimension);
      console.log(dimension.x);
      if(dimension && dimension.width && dimension.height){
        $.ajax({
          url: "<?php ROOT_PATH?>ajax/update_svg_size.php",
          type:"post",
          data:{width:dimension.width,height:dimension.height,fileName: "<?php echo $fileName?>"},
          success:function(param){console.log("successo");console.log(param);},
          error:function(param){console.log("failed");console.log(param);},
          complete:function(){console.log("complete");}
        });
      }
      
    </script>
  </body>
</html>