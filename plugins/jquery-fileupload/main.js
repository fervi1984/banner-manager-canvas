/*jslint unparam: true, regexp: true */
/*global window, $ */
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = ROOT_PATH+'ajax/uploader.php',
        uploadButton = $('<button/>')
            .addClass('btn btn-primary')
            .prop('disabled', true)
            .text('Processing...')
            .on('click', function () {
                var $this = $(this),
                    data = $this.data();
                $this
                    .off('click')
                    .text('Abort')
                    .on('click', function () {
                        $this.remove();
                        data.abort();
                    });
                data.submit().always(function () {
                    $this.remove();
                });
            });
    
    $('.fileupload').each(function(){
    	var fileUploadID = $(this).attr('id');
    	$(this).fileupload({
	        url: url,
	        dataType: 'json',
	        autoUpload: false,
	        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
	        maxFileSize: 999000,
	        // Enable image resizing, except for Android and Opera,
	        // which actually support image resizing, but fail to
	        // send Blob objects via XHR requests:
	        disableImageResize: /Android(?!.*Chrome)|Opera/
	            .test(window.navigator.userAgent),
	        previewMaxWidth: 100,
	        previewMaxHeight: 100,
	        previewCrop: true
	    }).on('fileuploadadd', function (e, data) {
	    	$('#files-'+fileUploadID).html('');
	        data.context = $('<div/>').appendTo('#files-'+fileUploadID);
	        $.each(data.files, function (index, file) {
	            var node = $('<p/>')
	                    .append($('<span/>').text(file.name));
	            if (!index) {
	                node
	                    .append('<br>')
	                    .append(uploadButton.clone(true).data(data));
	            }
	            node.appendTo(data.context);
	        });
	        data.context.find('button').click();
	    }).on('fileuploadprocessalways', function (e, data) {
	        var index = data.index,
	            file = data.files[index],
	            node = $(data.context.children()[index]);
	        if (file.preview) {
	            node
	                .prepend('<br>')
	                .prepend(file.preview);
	        }
	        if (file.error) {
	            node
	                .append('<br>')
	                .append($('<span class="text-danger"/>').text(file.error));
	        }
	        if (index + 1 === data.files.length) {
	            data.context.find('button')
	                .text('Upload')
	                .prop('disabled', !!data.files.error);
	        }
	    }).on('fileuploadprogressall', function (e, data) {
	        var progress = parseInt(data.loaded / data.total * 100, 10);
	        $('#progress-'+fileUploadID+' .progress-bar').css(
	            'width',
	            progress + '%'
	        );
	    }).on('fileuploaddone', function (e, data) {
	        $.each(data.result.files, function (index, file) {
	            if (file.url) {
	                var link = $('<a>')
	                    .attr('target', '_blank')
	                    .prop('href', file.url);
	                $(data.context.children()[index])
	                    .wrap(link);
	                if($("#modalareaselect").length > 0){
		                $("#modalareaselect .modal-body").html("<img id='uploadedImg' width='100%' src='"+file.url+"' />");
		    	        $('#modalareaselect').modal('show');
		    	        
		    	        var x1=0, y1=0, x2=0, y2=0;
		    	        var windowWidth,originWidth,windowHeight,originHeight,propWidth,propHeight;
		    	        
		    	        //script luca per aggiungere dimensioni svg
		    	        
		    	        var svgFile = $("#files-logoUp a").attr('href');
		    	        
		    	        if(svgFile.slice((svgFile.lastIndexOf(".") - 1 >>> 0) + 2) == 'svg'){
			    	        var svgObj = $('<object data="'+svgFile+'" type="image/svg+xml" id="svgObj"></object>');
			    	        svgObj.appendTo('body');
			    	        
			    	        var a = document.getElementById("svgObj");
			    	        a.addEventListener("load",function(){
	
			                    // get the inner DOM of svg
			                    var svg = a.contentDocument;
			                    $("#files-logoUp").append($(svg).find('svg'));
			                    
			                    var dimension = $("#files-logoUp svg")[0].getBBox();
				    	        console.log(dimension);
				    	        console.log(dimension.x);
				    	        if(dimension && dimension.width && dimension.height){
				    	          $.ajax({
				    	            url: ROOT_PATH+"ajax/update_svg_size.php",
				    	            type:"post",
				    	            data:{width:dimension.width,height:dimension.height,fileName: file.name},
				    	            success:function(param){console.log("successo");console.log(param);},
				    	            error:function(param){console.log("failed");console.log(param);},
				    	            complete:function(){console.log("complete");}
				    	          });
				    	        }
				    	        $("#files-logoUp svg").remove();
				    	        svgObj.remove();
				    	        
			                }, false);
		    	        }       	        
		    	        //fine script luca
		    	        
		    	        var ias = $('img#uploadedImg').imgAreaSelect({
		    	        	instance: true,
		    	            handles: true,
		    	            onSelectEnd: function (img, selection) {
		    	            	windowWidth = $(img).width();
		    	            	originWidth =$(img).prop("naturalWidth");
		    	            	windowHeight = $(img).height();
		    	            	originHeight =$(img).prop("naturalHeight");
		    	            	
		    	            	propWidth = originWidth / windowWidth;
		    	            	propHeight = originHeight / windowHeight;
		    	            	
		    	            	x1 =  Math.ceil(selection.x1 * propWidth);
		    	            	y1 =  Math.ceil(selection.y1 * propHeight);
		    	            	x2 =  Math.ceil(selection.x2 * propWidth);
		    	            	y2 =  Math.ceil(selection.y2 * propHeight);
		    	            }
		    	        });
		    	        
		    	        $('#modalareaselect .btn-primary').off().on('click', function(){
		    	        	var urlAjax =  ROOT_PATH+'ajax/banner.php';
		    	        	var typeFile = fileUploadID;
		    	        	
		    	        	switch(typeFile){
		    	        		case 'bannerUp':
		    	        			$("#coordMainAreaBanner").val('{"x1":'+x1+',"x2":'+x2+',"y1":'+y1+',"y2":'+y2+'}');
		    	        		break;		    	        		
		    	        		case 'logoUp':
		    	        			$("#coordMainAreaLogo").val('{"x1":'+x1+',"x2":'+x2+',"y1":'+y1+',"y2":'+y2+'}');
		    	        		break;
		    	        	}
		    	        	
		    	        	$('#modalareaselect .btn-secondary').click();
		    	        	ias.cancelSelection();
		    	        });
	                }
	    	        
	            } else if (file.error) {
	                var error = $('<span class="text-danger"/>').text(file.error);
	                $(data.context.children()[index])
	                    .append('<br>')
	                    .append(error);
	            }
	        });
	       
	        
	    }).on('fileuploadfail', function (e, data) {
	        $.each(data.files, function (index) {
	            var error = $('<span class="text-danger"/>').text('File upload failed.');
	            $(data.context.children()[index])
	                .append('<br>')
	                .append(error);
	        });
	    }).prop('disabled', !$.support.fileInput)
	        .parent().addClass($.support.fileInput ? undefined : 'disabled');
    });
});