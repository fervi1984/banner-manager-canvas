<?php
if($log) {
	$log = fopen("error.log",'a+');
	fwrite($log,"rigenerato".chr(10));
	fclose($log);
}

$cnt=0;
while(isset($siz[$cnt])) {
	$cnt++;
	${"dim".$cnt}=$siz[$cnt-1];
}

$cnt=0;
while(isset($pars[$cnt])) {
	$cnt++;
	${"par".$cnt}=$pars[$cnt-1];
}

// The file
$filename = $src;
list($width, $height) = getimagesize($filename);

// default
$dst_x=0;
$dst_y=0;
$src_x=0;
$src_y=0;
$from_width=$width;
$from_height=$height;

switch($typ) {

	// s = stretch (ridimensiona secondo le misure indicate)
	case "s":
		$new_width = $dim1;
		$new_height = $dim2;
		break;

		// w = width (ridimensiona in proporzione mantenendo la larghezza)
	case "w":
		$new_width = $dim1;
		$new_height = $height / $width * $new_width;
		break;

		// h = height (ridimensiona in proporzione mantenendo l'altezza)
	case "h":
		$new_height = $dim1;
		$new_width = $width / $height * $new_height;
		break;

		// b = box (ridimensiona in proporzione entro le misure indicate)
	case "b":
		$new_width = $dim1;
		$new_height = $height / $width * $new_width;
		if($new_height>$dim2) {
			$new_height = $dim2;
			$new_width = $width / $height * $new_height;
		}
		break;

		// c = crop (taglia una parte dell'immagine grande quanto le misure indicate)
	case "c":
		$new_width = $dim1;
		$new_height = $dim2;

		if(isset($par2) and $par2=="r") {
			$from_width = $width;
			$from_height = $new_height / $new_width * $from_width;
			if($from_height>$height) {
				$from_height = $height;
				$from_width = $new_width / $new_height * $from_height;
			}
		}
		else {
			$from_width = $new_width;
			$from_height = $new_height;
		}
		switch($par1) {
			case "tl":
			case "tc":
			case "tr":
				$src_y = 0;
				break;

			case "cl":
			case "cc":
			case "cr":
				$src_y = floor(($height - $from_height) / 2);;
				break;

			case "bl":
			case "bc":
			case "br":
				$src_y = $height-$from_height;
				break;
		}
		switch($par1{1}) {
			case "l":
				$src_x = 0;
				break;
			case "c":
				$src_x = floor(($width - $from_width) / 2);
				break;
			case "r":
				$src_x = $width - $from_width;
				break;
		}

		break;

		// f = fit (visualizza il massimo possibile di una foto secondo le misure indicate)
	case "f":
		$new_width = $dim1;
		$new_height = $dim2;

		$from_ratio = $width/$height;
		$to_ratio = $new_width/$new_height;

		if($to_ratio>$from_ratio) {
			$from_height = $width / $to_ratio;
			$src_y = floor( ($height - $from_height)*$par1/100 );
		}
		else {
			$from_width = $height * $to_ratio;
			$src_x = floor( ($width - $from_width)*$par1/100 );
		}
		break;

	default:
		$new_width = $dim1;
		$new_height = $dim2;
		break;
}

// trova tipo di file
$arrTmp = explode(".",$filename);
$estensione=end($arrTmp);
switch(strtolower($estensione)) {
	case "jpg":
	case "jpeg":
		$image = imagecreatefromjpeg($filename);
		break;
	case "png":
		$image = imagecreatefrompng($filename);
		break;
	case "gif":
		$image = imagecreatefromgif($filename);
		break;
}

// Resample
$image_p = imagecreatetruecolor($new_width, $new_height);
imagecopyresampled($image_p, $image, $dst_x, $dst_y, $src_x, $src_y, $new_width, $new_height, $from_width, $from_height);

//Grayscale filter
if(isset($_GET['grayscale']))
    imagefilter($image_p, IMG_FILTER_GRAYSCALE);

// Output
if($overwrite){
	switch(strtolower($estensione)) {
		case "jpg":
		case "jpeg":
		imagejpeg($image_p, $filename, $qua);
		break;
		case "png":
		imagepng($image_p, $filename, (($qua/100)-1));
		break;
	}	
} else{
	imagejpeg($image_p, $cachedir.$cachenamefile, $qua);
}
//imagejpeg($image_p, null, $qua);
?>
