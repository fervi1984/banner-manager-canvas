<?php
class Database{
	private $resource_id;
	private $username;
	private $password;
	private $server;
	private $db_name;
	public $active;
	
	function __construct(){
		$this->resource_id = null;
		$this->username = null;
		$this->password = null;
		$this->server = null;
		$this->db_name = null;
		$this->active = false;
	}
	function getResource(){
		return $this->resource_id;
	}
	public function connect($server, $uid, $pwd, $db){
		if(
			$this->_checkVar($server) &&
			$this->_checkVar($uid) &&
			$this->_checkVar($pwd) &&
			$this->_checkVar($db)
		){
			//connecting
			$this->server = $server;
			$this->username = $uid;
			$this->password = $pwd;
			$this->db_name = $db;
			//$this->resource_id = mysql_connect($this->server, $this->username, $this->password);
			try{
				$this->resource_id = new PDO('mysql:host=' . $this->server . ';dbname=' . $this->db_name .';charset=utf8', $this->username, $this->password);
				$this->resource_id->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$this->active = true;
			}catch(PDOException $e){
				die('Connection failed: ' . $e->getMessage());
			}
		}else{
			echo "[Error Database::connect] unable to connect due to parameter problem";
			exit;
		}
		return $this->resource_id;
	}
	
	public function connectPersistent($server, $uid, $pwd, $db){
		if(
			$this->_checkVar($server) &&
			$this->_checkVar($uid) &&
			$this->_checkVar($pwd) &&
			$this->_checkVar($db)
		){
			//connecting
			$this->server = $server;
			$this->username = $uid;
			$this->password = $pwd;
			$this->db_name = $db;
			//$this->resource_id = mysql_connect($this->server, $this->username, $this->password);
			try{
				$this->resource_id = new PDO('mysql:host=' . $this->server . ';dbname=' . $this->db_name , $this->username, $this->password,array(
					PDO::ATTR_PERSISTENT => true
				));
				
				//$this->resource_id->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$this->active = true;
			}catch(PDOException $e){
				die('Connection failed: ' . $e->getMessage());
			}
		}else{
			echo "[Error Database::connect] unable to connect due to parameter problem";
			exit;
		}
		return $this->resource_id;
	}
	
	public function disconnect(){
		$this->resource_id = null;
		$this->active = false;
	}
	
	public function select($sql, $debug = false){
		if($debug){
			echo "<br/>" . $sql;
		}
		$results = array();
		
		try{
			$sth = $this->resource_id->prepare($sql);
			$sth->execute();
			$results = $sth->fetchAll(PDO::FETCH_ASSOC);
			$sth->closeCursor();
			$sth = null;
		}catch(PDOException $e){
			die ('Connection failed: ' . $e->getMessage());
		}
		
		
		return $results;
	}
	
	public function cleanString($str){
		return @$this->resource_id->quote($str);
	}
	
	public function format($var){
		if(is_bool($var)){
			if($var){
				return "1";
			}else{
				return "0";
			}
		}elseif(is_null($var)){
			return "NULL";
		}elseif (is_a($var, 'DateTime')) {
			return "'" . $var->format('Y-m-d H:i:s') . "'";
		}else{
			return $this->resource_id->quote($var);
		}
		return $var;
	}
	
	public function query($sql, $debug = false){
		if($debug){
			echo "<br/>" . $sql;
		}
		
		//TODO add lock/unlock tables
		try{
			$sth = $this->resource_id->prepare($sql);
			$sth->execute();
			$sth->closeCursor();
			$sth = null;
		}catch(PDOException $e){
			die ('Connection failed: ' . $e->getMessage());
		}
		return true;
	}
	
	/**
	 *function che mi permette di prendere l'ultimo valore di id inserito con autoincremento
	 *string $insertSql sql della insert da esefuire
	 *[bool $debug = false] valore identificativo di debug
	 */
	public function insert($insertSql, $debug = false){
		
		$last_id = false;
		try{
			
			$this->resource_id->exec($insertSql);
			$last_id = $this->resource_id->lastInsertId();
			
		}catch(PDOException $e){
			die ('Connection failed: ' . $e->getMessage());
		}
		
		return $last_id;
	}
	
	private function _checkVar($var){
		$check = false;
		if(isset($var) && !empty($var) && !is_null($var)){
			$check = true;
		}
		return $check;
	}
	
}

class DatabaseTableList extends Database{
//	private $header;
	private $table;
	private $results;
	private $fields;
	private $fieldsNames;
	private $fieldsLabel;
	
	private $where;
	private $orderBy;
	private $limit;
	
	private $IDField;
	
	public function __construct(){
		$this->table = null;
		$this->results = null;
		$this->fields = array();
		$this->fieldsNames = array();
		$this->fieldsLabel = array();
		$this->IDField = "ID";
	}
	
	public function setTable($t){
		$this->table = $t;
	}
	
	private function _setResults($res){
		$this->results = $res;
	}
	
	public function setFields($fields){
		$this->fields = $fields;
		foreach ($this->fields as $field => $label){
			array_push($this->fieldsNames, $field);
			array_push($this->fieldsLabel, $label);
		}
	}
	
	public function setWhere($parm){
		$this->where = "";
		$sWhere = "";
		if ($parm['sSearch'] != "") {
			$sWhere = "WHERE (";
			for ($i = 0; $i < count($this->fieldsNames); $i++) {
				$sWhere .= $this->fieldsNames[$i] . " LIKE '%" . mysql_real_escape_string($parm['sSearch']) . "%' OR ";
			}
			$sWhere = substr_replace($sWhere, "", -3);
			$sWhere .= ')';
		}
		
		/* Individual column filtering */
		for ($i = 0; $i < count($this->fieldsNames); $i++) {
			if ($parm['bSearchable_' . $i] == "true" && $parm['sSearch_' . $i] != '') {
				if ($sWhere == "") {
					$sWhere = "WHERE ";
				} else {
					$sWhere .= " AND ";
				}
				$sWhere .= $this->fieldsNames[$i] . " LIKE '%" . mysql_real_escape_string($parm['sSearch_' . $i]) . "%' ";
			}
		}
		$this->where = $sWhere;
	}
	
	public function setOrderBy($parm){
		if (isset($parm['iSortCol_0'])) {
			$sOrder = "ORDER BY  ";
			for ($i = 0; $i < intval($parm['iSortingCols']); $i++) {
				if ($_GET['bSortable_' . intval($parm['iSortCol_' . $i])] == "true") {
					$sOrder .= $this->fieldsNames[intval($parm['iSortCol_' . $i])] . "
				 	" . mysql_real_escape_string($parm['sSortDir_' . $i]) . ", ";
				}
			}
		
			$sOrder = substr_replace($sOrder, "", -2);
			if ($sOrder == "ORDER BY") {
				$sOrder = "";
			}
		}
		$this->orderBy = $sOrder;
	}
	
	public function setLimit($parm){
		$sLimit = "";
		if (isset($parm['iDisplayStart']) && $parm['iDisplayLength'] != '-1') {
			$sLimit = "LIMIT " . mysql_real_escape_string($parm['iDisplayStart']) . ", " .
					mysql_real_escape_string($parm['iDisplayLength']);
		}
		$this->limit = $sLimit;
	}
	
	public function getList($debug = false){
		$sqlFields = "";
	
		$dbList = new DatabaseTableList();

		foreach ($this->fields as $field => $label){
			$sqlFields .=  $field . ", ";
		}
		$sqlFields = substr($sqlFields, 0, strlen($sqlFields) - 2);
		if($debug){
			echo "SqlField: " . $sqlFields . "<br/>\n";
		}
		$sql = "SELECT " . $this->IDField . ", " . $sqlFields . " FROM " . $this->table;
		if(isset($this->where) && !empty($this->where)){
			$sql.= " " . $this->where;
		}
		if(isset($this->orderBy) && !empty($this->orderBy)){
			$sql.= " " . $this->orderBy;
		}
		if(isset($this->limit) && !empty($this->limit)){
			$sql.= " " . $this->limit;
		}
		$this->_setResults($this->select($sql));
	
		if($debug){
			echo $sql . "<br/>\n";
			echo "##############Query Results#################<br/>\n";
			print_r($this->results);
			echo "#######################################<br/>\n";
		}
	
	}
	
	private function getTotal($debug = false){
		foreach ($this->fields as $field => $label){
			//$sqlFields .=  $field . ", ";
			$fld = $field;
		}
		
		$sql = "SELECT COUNT(" . $fld . ") FROM " . $this->table;
		if(isset($this->where) && !empty($this->where)){
			$sql.= " " . $this->where;
		}
		if($debug){
			echo $sql;
		}
		$res = $this->select($sql);
		return $res[0][0];
	}
	
	public function toHTML(){
		$html = "";
		$html.= "<div class=\"col-sm-12\">\n";
		$html.= "\t<table class=\"table-grid table table-hover table-condensed\">\n";
		$html.= "\t\t<thead>\n";
		$html.= "\t\t\t<tr>\n";
		foreach ($this->fieldsLabel as $label){
			$html.= "\t\t\t\t<th>" . $label . "</th>\n";
		}
		$html.= "\t\t\t\t<th>Azioni</th>\n";
		$html.= "\t\t\t</tr>\n";
		$html.= "\t\t</thead>\n";
		$html.= "\t</table>\n";
		$html.= "</div>";
		
		return $html;
	}
	
	public function getJSONResults($sEcho = 0, $modModule = "", $eraseModule = "", $iFilteredTotal = null){
		if($iFilteredTotal == null){
			//$iFilteredTotal = sizeof($this->results);
			$iFilteredTotal = $this->getTotal();
		}
		
		$output = array(
				"sEcho" => $sEcho,
				"iTotalRecords" => sizeof($this->results),
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => array()
		);
		
		for($j = 0; $j < count($this->results); $j++){
			$row = array();
			for ($i = 0; $i < count($this->fieldsNames); $i++) {
				if ($this->fieldsNames[$i] == "version") {
					/* Special output formatting for 'version' column */
					$row[] = ($this->results[$j][$this->fieldsNames[$i]] == "0") ? '-' : $this->results[$j][$this->fieldsNames[$i]];
				} else if ($this->fieldsNames[$i] != ' ') {
					/* General output */
					$row[] = $this->results[$j][$this->fieldsNames[$i]];
				}
			}
			$btn = "<div style=\"white-space: nowrap\"><a class=\"btn btn-default-alt btn-xs\" href=\"" . $modModule . "&id=" . $this->results[$j][0] . "\" >modifica</a>";
			if($eraseModule != ""){
				$btn.= "&nbsp;&nbsp;<a class=\"btn btn-danger-alt btn-xs\" href=\"" . $eraseModule . "&id=" . $this->results[$j][0] . "\" >elimina</a>";
			}
			$btn.= "</div>";
			array_push($row, $btn);
			$output["aaData"][] = $row;
		}
		
		return json_encode($output);
	}
	
}

/*
 *	Depends from Database
 *	Need a database connection active 
 */
/*
class DatabaseTable{
	public $table;
	private $connection;
	
	public function __construct(Database $db){
		$this->connection = $db;
	}

}*/