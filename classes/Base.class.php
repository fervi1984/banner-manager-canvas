<?php

require_once ROOT_DIR."classes/DatabaseTable.class.php";

class Base extends DatabaseTable{
	
	public $id = NULL;
	public $columnList = array("id"=>"getId");
    
	const TABLE = "deafult";
    const INDEX = "id";
    
    public function __construct($db_conn = null, $ident = null){
		if(!$db_conn){
			if(isset($GLOBALS['connection'])){
				$db_conn = $GLOBALS['connection'];
			}else{
				$db_conn = new Database();
			}
		}
		parent::__construct($db_conn);
		$this->setTable(DB_PREF.$this::TABLE);
		$this->setStatus(DatabaseTable::NEW_RECORD);
        if(isset($ident)){
            $this->init();
        }
	}
    
    public function init($id){
		if(isset($id) && !empty($id)){
			$result = $this->select("SELECT * FROM " . $this->getTable(). " WHERE " . $this::INDEX . " = " . $id );
			if(count($result)>0){
				$this->initByRecord($result[0]);
				return $this;
			}else{
				return null;
			}
		}else{
			return null;
		}
	}
	
	public function getColumsList(){
		return array_keys($this->columnList);
	}
	
	public function haveColumn($name){
		return in_array($name,$this->getColumsList());
	}
	
	public function get($var,$utf8_encoded = false){
		
		$v = $this->{$this->columnList[$var]}();
		if($utf8_encoded && !($v instanceof DateTime)){
			$v = utf8_encode($v);
		}
		return $v;
	}
	
	public function getId(){
		return $this->id;
	}
	/**
	 *function che mi setta la variabile passate nelle varie lingue
	 *
	 *@param array $record record del db dove prendere le informazioni
	 *@param string $varible dove salvare le informazini
	 *@param string $recordName nome del record da cercare dentro record
	 *
	 */
	public function _setVariableAllLanguages($record,&$variable,$recordName){
		$variable2 = array();
		
		foreach(Lingua::getLanguages() as $est){
			if(array_key_exists("{$recordName}_{$est}",$record)){
				$variable2[$est] = $record["{$recordName}_{$est}"];	
			}elseif(array_key_exists($recordName,$record)){
				$variable2[$est] = $record[$recordName];	
			}
		}
		$variable=$variable2;
	}
	
	public function _getVariableLanguage($variable,$lingua = ""){
		$lingua = strlen($lingua)>0?$lingua:LANG;
		return $variable[$lingua];
	}
	
	public function isGeneric(){
		return get_class($this) == "Base";
	}
	
    public function exists(){
        return !is_null($this->getId());
    }
	
	public function getKey(){
		return $this->getId();
	}
	
	/**
	 *initByRecord($record[,$aliasTable = null])
	 *funzione che mi setta i vari parametri della classe definiti nella variabile columnList. Se non presente nella lista li inserisce come semplice variabili
	 *
	 *@param <array> $record - array contenente i valori del record solitamente presa da db
	 *@param <string> [$aliasTable] - alias usata se i valori sono stati presi dalla tabella
	 */
	public function initByRecord($record,$aliasTable = null){
		$this->setStatus(DatabaseTable::LOADED_RECORD);
        
		$prefixTable = "";
		if(!is_null($aliasTable)){
			$prefixTable = $aliasTable . ".";
		}
		
		//per ogni colonna dichiarata
		$columns = $this->getColumsList();
		for($i = 0; $i< count($columns); $i++){
			$name = $prefixTable . $columns[$i];
			if(array_key_exists($name,$record) && strlen($this->columnList[$columns[$i]]) > 0){
				//se ho una funzione di settaggio definita la uso
				$this->{$this->columnList[$columns[$i]]}($record[$name]);
			}else{
				//se NON ho una funzione di settaggio definita creo una variabile semplice
				$this->{$columns[$i]} = $record[$name];
			}
			//tolgo i record già settati
			unset($record[$name]);
		}
		
		//setto come semplici variabili gli altri record rimasti
		foreach($record as $cname => $cvalue ){
			$this->{$cname} = $cvalue;
		}
		
	}
	
	public static function getIstance(){
		//get_class
		return new static();
	}
	
	/**
	 *function che ritorna l'elenco dei campi della select
	 */
	public static function getSelectFieldAll($prefix=""){
		if(strlen($prefix)>0)$prefix.=".";
		$selectFields = "";
		$m = self::getIstance();
		$columns = $m->getColumsList();
		for($i = 0; $i< count($columns); $i++){
			$name = $columns[$i];
			$selectFields .= "{$prefix}{$name} as '{$prefix}{$name}',";
		}
		//tolgo l'ultima virgola
		$selectFields = substr($selectFields, 0, -1);
		
		return $selectFields;
	}
	
	public static function haveColumnName($name){
		$m = self::getIstance();
		return $m->haveColumn($name);
	}
}