<?php

abstract class CustomList extends DatabaseTable{
	private $items;
	private $keys;
	private $sortField;
	private $sortOrder;
	
	const TABLE = "deafult";
	
	public function __construct($db_conn = null){
		if(!$db_conn){
			if(isset($GLOBALS['connection'])){
				$db_conn = $GLOBALS['connection'];
			}else{
				$db_conn = new Database();
			}
		}
		parent::__construct($db_conn);
		$this->setTable(DB_PREF.$this::TABLE);
		$this->setStatus(DatabaseTable::NEW_RECORD);
		if(isset($ident)){
			$this->init();
		}
		$this->clear();
		$this->sortField = "id";
		$this->sortOrder = "ASC";
	}
	
	/**
	 *Funzione che mi aggiunge i valori alla mia lista
	 */
	public function add(/*OBJ*/ $a){
		$this->items[$a->getKey()] = $a;
	}
	
	public function count(){
		return count($this->items);
	}

	public function clear(){
		$this->items = array();
		$this->keys = array();
	}
	
	public function toArray(){
		return $this->items;
	}
	
	private function cmp($a,$b){
		$value1 = $a->getCompareValue($this->sortField);
		$value2 = $b->getCompareValue($this->sortField);
		if($this->order == "ASC"){
			return ($value1 < $value2) ? -1 : 1; 
		}else{
			return ($value1 > $value2) ? -1 : 1; 
		}
	}
	
	public function sortElements($field="id",$order="ASC"){
		$this->sortField = $field;
		$this->order = $order;
		usort($this->items,array($this,"cmp"));
		$this->keys = array_keys($this->items);
	}
	
	public function getId(){}
	public function getKeys(){
		if(count($this->keys)<= 0){
			$this->keys = array_keys($this->items);
		}
		return array_keys($this->items);
	}
	
	public function getElementAtIndex($i){
		if(count($this->keys)<= 0){
			$this->keys = array_keys($this->items);
		}
		return $this->items[$this->keys[$i]];
	}
	
	public function getElementForKey($k){
		return array_key_exists($k, $this->items)?$this->items[$k]:false;
	}
	
	public function removeElementForKey($k){
		unset($this->items[$k]);
	}
	
	public function getLastElement(){
		if($this->count()>0){
			return $this->getElementAtIndex($this->count()-1);
		}
		return false;
	}
	
	abstract function init();
}