<?php
class Campaign extends DatabaseTable{
	private $id;
	private $idCamp;
	private $idTemp;
	private $nome;
	private $type;
	private $width;
	private $height;
	private $bgBanner;
	private $headText;
	private $subHeadText;
	private $colorText;
	private $bgText;
	private $fileBanner;
	private $fileLogo;
	private $webSiteText;
	private $webSiteLink;
	private $ctaText;
	private $ctaLink;
	private $bannerList;
	private $listContenuti = null;
	private $thumbUrl;
	private $json;
	private $cssStyle;
	
	public function __construct(Database $db_conn, $id = NULL){
		parent::__construct($db_conn);
		$this->setStatus(DatabaseTable::NEW_RECORD);
		$this->setTable("pot_banner");
		
		if($id != NULL){
			$this->init($id);
		}
	}
	
	public function init($id){
		if(isset($id) && !empty($id)){
			$result = $this->select("SELECT * FROM pot_banner WHERE idBanner = '" . $id . "' AND idCamp='-1'", false);
			if(count($result)>0){
				$this->initByRecord($result[0]);
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
	public function initByRecord($record){
		$this->setStatus(DatabaseTable::LOADED_RECORD);
		$this->id = $record["idBanner"];
		$this->idCamp = $record["idCamp"];
		$this->idTemp = $record["idTemp"];
		$this->nome = $record["nome"];
		$this->type = $record["type"];
		$this->headText = $record["headText"];
		$this->subHeadText = $record["subHeadText"];
		$this->colorText = $record["colorText"];
		$this->bgText = $record["bgText"];
		$this->width = $record["width"];
		$this->height = $record["height"];
		$this->bgBanner = $record["bgBanner"];
		$this->fileBanner = $record["fileBanner"];
		$this->fileLogo = $record["fileLogo"];
		$this->coordMainAreaBanner = $record['coordMainAreaBanner'];
		$this->coordMainAreaLogo = $record['coordMainAreaLogo'];
		$this->webSiteText = $record["webSiteText"];
		$this->webSiteLink = $record["webSiteLink"];
		$this->ctaText = $record["ctaText"];
		$this->ctaLink = $record["ctaLink"];
		$this->thumbUrl = $record["thumbUrl"];
		$this->json = json_decode($record["json"], true);
		$this->cssStyle = $record["cssStyle"];
	}
	
	/*GETTERS/SETTERS*/
	public function getId(){
		return $this->id;
	}
	public function getIdTemp(){
		return $this->idTemp;
	}
	public function getIdCamp(){
		return $this->idCamp;
	}
	public function getNome(){
		return $this->nome;
	}
	public function getType(){
		return $this->type;
	}
	public function getHeadText(){
		return $this->headText;
	}
	public function getSubHeadText(){
		return $this->subHeadText;
	}
	public function getColorText(){
		return $this->colorText;
	}
	public function getBgText(){
		return $this->bgText;
	}
	public function getBgBanner(){
		return $this->bgBanner;
	}
	public function getWidth(){
		return $this->width;
	}
	public function getHeight(){
		return $this->height;
	}
	public function getFileBanner(){
		return $this->fileBanner;
	}
	public function getFileLogo(){
		return $this->fileLogo;
	}
	public function getWebSiteText(){
		return $this->webSiteText;
	}
	public function getWebSiteLink(){
		return $this->webSiteLink;
	}
	public function getCtaText(){
		return $this->ctaText;
	}
	public function getCtaLink(){
		return $this->ctaLink;
	}
	public function getCoordMainAreaBanner(){
		return $this->coordMainAreaBanner;
	}
	public function getCoordMainAreaLogo(){
		return $this->coordMainAreaLogo;
	}
	public function getJson(){
		return $this->json;
	}
	
	public function getStyle(){
		return $this->cssStyle;
	}
	public function getCountBanner(){
		$result = $this->select("SELECT count(idBanner) as totalBanner FROM ".$this->getTable()." WHERE idCamp = ".$this->id, false);
		return $result[0]['totalBanner'];
	}
	//todo: implementare un get che ritorni un array di oggetti Banner per ogni formato
	public function getAllFormats(){
		$result = $this->select("SELECT idBanner FROM ".$this->getTable()." WHERE idCamp = ".$this->id, false);
		$arrayFormats = [];
		foreach($result as $k=>$v){
			$banner = new Banner($this->get_db_connection());
			$banner->init($v['idBanner']);
			
			$arrayFormats[] = $banner;
		}
		return $arrayFormats;
	}
	public function getThumbUrl(){
		return $this->thumbUrl;
	}
	public function saveThumbUrl($idBanner=0){
		if($this->getId()){
			$idBanner = $this->getId();
		}else{
			$this->init($idBanner);
		}
	
		$image = Utility::get_data("https://restpack.io/api/screenshot/v2/capture?access_token=lVfmFjAXP2wSrlAkqB79jiLDuYVSZ8bo4PS9clPhhSVcsQ51&url=".ROOT_PATH.$idBanner."/A/700x320&width=700&height=320&delay=7000&fresh=true");
		file_put_contents(ROOT_DIR.'tmp/thumbBanner_'.$idBanner.'.png', $image);
	
		$this->thumbUrl = Utility::nullizza(ROOT_PATH.'tmp/thumbBanner_'.$idBanner.'.png');
	
		$sql = "UPDATE ".$this->getTable()." SET ";
		$sql.= "  thumbUrl = $this->thumbUrl";
		$sql.= ", data_mod = now()";
	
		$sql .= " WHERE idBanner = " . $idBanner;
	
		$resp = $this->query($sql);
	}
	public function delete(){
		$sql = "DELETE FROM ".$this->getTable()." WHERE idBanner = '".$this->id."' OR idCamp='".$this->id."'";
		$resp = $this->query($sql);
		return $resp;
	
	}
	public function deleteSingle($idBanner){
		$sql = "DELETE FROM ".$this->getTable()." WHERE idBanner = '".$idBanner."'";
		$resp = $this->query($sql);
		return $resp;
	
	}
	public function saveSingle($sqlData){
		
		foreach($sqlData as $k=>$v){
			${$k} = Utility::nullizza($v);
		}
		
		//update
		$sql = "UPDATE ".$this->getTable()." SET ";
		$sql.= " idTemp = $idTemp";
		$sql.= ", data_mod = now()";
	
		$sql .= " WHERE idBanner = " . $idBanner;
	
		$resp = $this->query($sql);
		
		$this->id = $sqlData['idBanner'];
	
		$this->saveThumbUrl();
			
	}
	/*save*/
	public function saveUpdate($sqlData){
		
		if(array_key_exists('fileBanner', $sqlData)){
			$sqlData['fileBanner'] = end(explode("/assets/",$sqlData['fileBanner']));
		} else {
			$fileBanner = "''";
		}
		if(array_key_exists('fileLogo', $sqlData)){
			$sqlData['fileLogo'] = end(explode("/assets/",$sqlData['fileLogo']));
		} else {
			$fileLogo = "''";
		}
		
		foreach($sqlData as $k=>$v){
			${$k} = Utility::nullizza($v);
		}
		$result = array();
		//echo "SELECT ID FROM ".$this->getTable()." WHERE ID = $idPreview ";
		
		if(!empty($sqlData['idBanner']) && $sqlData['idBanner'] >0)
			$result = $this->select("SELECT idBanner FROM ".$this->getTable()." WHERE idBanner = $idBanner ", false);
			
			if(count($result)>0){
				//update
				$sql = "UPDATE ".$this->getTable()." SET ";
				$sql.= "  idCamp = $idCamp";
				$sql.= ", idTemp = $idTemp";
				$sql.= ", type = $type";
				$sql.= ", nome = $nome";
				$sql.= ", width = $width";
				$sql.= ", height = $height";
				$sql.= ", bgBanner = $bgBanner";
				$sql.= ", fileBanner = $fileBanner";
				$sql.= ", fileLogo = $fileLogo";
				$sql.= (isset($coordMainAreaBanner))?", coordMainAreaBanner = $coordMainAreaBanner":"";
				$sql.= (isset($coordMainAreaLogo))?", coordMainAreaLogo = $coordMainAreaLogo":"";
				$sql.= ", headText = $headText";
				$sql.= ", subHeadText = $subHeadText";
				$sql.= ", colorText = $colorText";
				$sql.= ", bgText = $bgText";
				$sql.= ", webSiteText = $webSiteText";
				$sql.= ", webSiteLink = $webSiteLink";
				$sql.= ", ctaText = $ctaText";
				$sql.= ", ctaLink = $ctaLink";
				$sql.= ", data_mod = now()";
	
				$sql .= " WHERE idBanner = " . $idBanner;
	
				$resp = $this->query($sql);
				
				#$this->idTemp =  $sqlData['idTemp']? $sqlData['idTemp']:'A';
				$this->id = $sqlData['idBanner'];
				
				$this->saveThumbUrl();
				
				$result = $this->select("SELECT idBanner FROM ".$this->getTable()." WHERE idCamp = $idBanner ", false);
								
				if(count($result)>0){
					#$this->query("DELETE FROM ".$this->getTable()." WHERE idCamp = $idBanner");
					foreach($result as $row){
						$sql = "UPDATE ".$this->getTable()." SET ";
						$sql.= " nome = $nome";
						$sql.= ", bgBanner = $bgBanner";
						$sql.= ", fileBanner = $fileBanner";
						$sql.= ", fileLogo = $fileLogo";
						$sql.= (isset($coordMainAreaBanner))?", coordMainAreaBanner = $coordMainAreaBanner":"";
						$sql.= (isset($coordMainAreaLogo))?", coordMainAreaLogo = $coordMainAreaLogo":"";
						$sql.= ", headText = $headText";
						$sql.= ", subHeadText = $subHeadText";
						$sql.= ", colorText = $colorText";
						$sql.= ", bgText = $bgText";
						$sql.= ", webSiteText = $webSiteText";
						$sql.= ", webSiteLink = $webSiteLink";
						$sql.= ", ctaText = $ctaText";
						$sql.= ", ctaLink = $ctaLink";
						$sql.= ", data_mod = now()";
						
						$sql .= " WHERE idBanner = " . $row['idBanner'];
						
						$resp = $this->query($sql);
					}
				}
				
				return $sqlData['idBanner'];
			} else {
	
				$sql = "INSERT INTO ".$this->getTable()." (";
				$sql.= "idCamp,";
				$sql.= "idTemp,";
				$sql.= "type,";
				$sql.= "nome,";
				$sql.= "width,";
				$sql.= "height,";
				$sql.= "bgBanner,";
				$sql.= "fileBanner,";
				$sql.= "fileLogo,";
				$sql.= "coordMainAreaBanner,";
				$sql.= "coordMainAreaLogo,";
				$sql.= "headText,";
				$sql.= "subHeadText,";
				$sql.= "colorText,";
				$sql.= "bgText,";
				$sql.= "webSiteText,";
				$sql.= "webSiteLink,";
				$sql.= "ctaText,";
				$sql.= "ctaLink,";
				$sql.= "data_ins";

				$sql.= ")VALUES(";
				$sql.= "-1,";
				$sql.= "$idTemp,";
				$sql.= "$type,";
				$sql.= "$nome,";
				$sql.= "$width,";
				$sql.= "$height,";
				$sql.= "$bgBanner,";
				$sql.= "$fileBanner,";
				$sql.= "$fileLogo,";
				$sql.= "$coordMainAreaBanner,";
				$sql.= "$coordMainAreaLogo,";
				$sql.= "$headText,";
				$sql.= "$subHeadText,";
				$sql.= "$colorText,";
				$sql.= "$bgText,";
				$sql.= "$webSiteText,";
				$sql.= "$webSiteLink,";
				$sql.= "$ctaText,";
				$sql.= "$ctaLink,";
				$sql.= "now() ";
				$sql.= ")";
	
				$idBanner = $this->insert($sql);
				
				$this->idTemp =  $sqlData['idTemp']? $sqlData['idTemp']:'A';
				$this->id = $idBanner;
				
				$this->saveThumbUrl();
				
				if(isset($sqlData['formatList'])){
					foreach($sqlData['formatList'] as $formatData){
						$formatName = Utility::nullizza($formatData['name']);
						$width = Utility::nullizza($formatData['width']);
						$height = Utility::nullizza($formatData['height']);
						
						#$sql = str_replace("(-1,", "($idBanner,", $sql);
						
						$sql = "INSERT INTO ".$this->getTable()." (";
						$sql.= "idCamp,";
						$sql.= "idTemp,";
						$sql.= "type,";
						$sql.= "nome,";
						$sql.= "formatName,";
						$sql.= "width,";
						$sql.= "height,";
						$sql.= "bgBanner,";
						$sql.= "fileBanner,";
						$sql.= "fileLogo,";
						$sql.= "coordMainAreaBanner,";
						$sql.= "coordMainAreaLogo,";
						$sql.= "headText,";
						$sql.= "subHeadText,";
						$sql.= "colorText,";
						$sql.= "bgText,";
						$sql.= "webSiteText,";
						$sql.= "webSiteLink,";
						$sql.= "ctaText,";
						$sql.= "ctaLink,";
						$sql.= "data_ins";
						
						$sql.= ")VALUES(";
						$sql.= "$idBanner,";
						$sql.= "$idTemp,";
						$sql.= "$type,";
						$sql.= "$nome,";
						$sql.= "$formatName,";
						$sql.= "$width,";
						$sql.= "$height,";
						$sql.= "$bgBanner,";
						$sql.= "$fileBanner,";
						$sql.= "$fileLogo,";
						$sql.= "$coordMainAreaBanner,";
						$sql.= "$coordMainAreaLogo,";
						$sql.= "$headText,";
						$sql.= "$subHeadText,";
						$sql.= "$colorText,";
						$sql.= "$bgText,";
						$sql.= "$webSiteText,";
						$sql.= "$webSiteLink,";
						$sql.= "$ctaText,";
						$sql.= "$ctaLink,";
						$sql.= "now() ";
						$sql.= ")";
						
						$resp = $this->insert($sql);
					}
				}
				
				return $idBanner;
			}
	
			return $resp;
	}
	
}



class CampaignList extends DatabaseTable{
	private $items;
	private $filled;
	
	public function __construct($db_conn){
		if(!isset($table) || empty($table) || is_null($table)){
			$table = "pot_Campaign";
		}
		parent::__construct($db_conn);
		$this->items = array();
	}
	
	public function init(){
		$this->clear();
		$sql = "SELECT * FROM pot_banner WHERE idCamp = -1";
		$result = $this->select($sql,false);
		foreach ($result as $record){
			$tmpItem = new Campaign($this->get_db_connection());
			$tmpItem->initByRecord($record);
			$this->add($tmpItem);
		}
	}
	
	public function add(Campaign $a){

		foreach($this->items as $k => $v){ 
			if($v->getId() == $a->getId()){ 
				unset($this->items[$k]);
			} 
		}
		array_push($this->items, $a);

	}
	
	public function toArray($i = null){
		if($i === null){
			return $this->items;
		}else{
			return $this->items[$i];
		}
	}
	
	public function count(){
		return count($this->items);
	}

	public function clear(){
		$this->items = array();
	}
	
	public function getId(){
		$ids = array();
		foreach ($this->items as $itm){
			array_push($ids, $itm->getId());
		}
		return $ids;
	}
		

}


?>