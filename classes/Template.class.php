<?php

require_once ROOT_DIR."classes/Base.class.php";
require_once ROOT_DIR."classes/Block.class.php";
require_once ROOT_DIR."classes/VariableConfiguration.class.php";

class Template extends Base{
    const TABLE = "template";
	
	/*VARIABLES*/
	private $_name = "";
	private $_cache = 0;
		
	/*VARIABLES - FINE*/
	
	/*VARIABLES - GET/SET*/
	public function setId($v){
		$this->id = $v;
	}
	
	
	public function setName($v){
		$this->_name = $v;
		return $this;
	}
	
	public function getName(){
		return $this->_name;
	}
	
	public function setCache($v){
		$this->_cache = $v;
		return $this;
	}
	
	public function getCache(){
		return $this->_cache;
	}
	
	public function getJsonStructure(){
		$json = "";
		$this->_cache = "";
		if(strlen($this->_cache)<=0){
			$bList = new BlockList($this->get_db_connection());
		    $json = $bList->getStructureJson($this->getId());
			if(strlen($json)>2){
				$this->_cache = $json;
				$this->save();
				$json = $this->_cache;
			}else{
				$json = "";
			}
		}else{
			$json = $this->_cache;
		}
		
		return $json;
	}
	
	/*VARIABLES - GET/SET - FINE*/
	
	
	/*INIT*/
	public function __construct($db_conn = null, $ident = null){
		parent::__construct($db_conn, $ident);
		$this->columnList = array(
								  "id"=>"setId",
								  "name"=>"setName",
								  "cachedStructure"=>"setCache"
								  );
		
	}
	
	/*INIT - FINE*/
	
	/* FUNCTIONS */
	
	public function getVariableConfiguration(){
		$bList = new BlockList($this->get_db_connection());
		$obj = $bList->getDefaultVariables($this->getId());
		return $obj;
	}
	
	
    /*save*/
	public function save(){
		switch ($this->getStatus()){
			case DatabaseTable::LOADED_RECORD:
				
				//update
				$sql = "UPDATE " . $this->getTable() . " SET ";
				$sql.= " name = " . $this->format($this->_name) ;
				$sql.= ", cachedStructure = " . $this->format($this->_cache) ;
				$sql .= " WHERE id = '" . $this->id . "'";
				
				$resp = $this->query($sql);
                
				break;
			
			case DatabaseTable::NEW_RECORD:
				throw new Exception("generic ");
				$t = new DateTime();
				$sql = "INSERT INTO " . $this->getTable() . "(";
				$sql.= "targa";
				$sql.= ")VALUES(";
                $sql.= $this->cleanString($this->targa);
				$sql.= ")";
				
				$this->id = $this->insert($sql);
				
				if($this->id !== false){
					$this->setStatus(DatabaseTable::LOADED_RECORD);
					return $this->save();	
				}else{
					throw new Exception("generic ");
					die("Fatal error occours");
				}	
                
				break;
			default:
				throw new Exception("generic ");
				break;
		}
		return $resp;
	}
	
	
	/*CLASSI STATICHE*/
	/*CLASSI STATICHE FINE*/
}

class TemplateList extends CustomList{
	const TABLE = "template";
	public function __construct($db_conn = null, $ident = null){
		parent::__construct($db_conn, $ident);
	}
	public function init(){
		$this->clear();
		$sql = "SELECT * FROM ". $this->getTable();
		$result = $this->select($sql,false);
		foreach ($result as $record){
			$tmpItem = new Template($this->get_db_connection());
			$tmpItem->initByRecord($record);
			$this->add($tmpItem);
		}
	}
}