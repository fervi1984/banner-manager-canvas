<?php
class Banner extends DatabaseTable{
	private $id;
	private $idCamp;
	private $idTemp;
	private $nome;
	private $type;
	private $width;
	private $height;
	private $bgBanner;
	private $headText;
	private $subHeadText;
	private $colorText;
	private $bgText;
	private $fileBanner;
	private $fileLogo;
	private $webSiteText;
	private $webSiteLink;
	private $ctaText;
	private $ctaLink;
	private $coordMainAreaBanner;
	private $coordMainAreaLogo;
	private $listContenuti = null;
	private $json;
	private $jsonCanvas;
	private $cssStyle;
	private $thumbUrl;
	
	public function __construct(Database $db_conn, $id = NULL){
		parent::__construct($db_conn);
		$this->setStatus(DatabaseTable::NEW_RECORD);
		$this->setTable("pot_banner");
		
		if($id != NULL){
			$this->init($id);
		}
	}
	
	public function init($id){
		if(isset($id) && !empty($id)){
			$result = $this->select("SELECT * FROM pot_banner WHERE idBanner = '" . $id . "'", false);
			if(count($result)>0){
				$this->initByRecord($result[0]);
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
	public function initByRecord($record){
		$this->setStatus(DatabaseTable::LOADED_RECORD);
		$this->id = $record["idBanner"];
		$this->idCamp = $record["idCamp"];
		$this->idTemp = $record["idTemp"];
		$this->nome = $record["nome"];
		$this->type = $record["type"];
		$this->headText = $record["headText"];
		$this->subHeadText = $record["subHeadText"];
		$this->colorText = $record["colorText"];
		$this->bgText = $record["bgText"];
		$this->width = $record["width"];
		$this->height = $record["height"];
		$this->bgBanner = $record["bgBanner"];
		$this->fileBanner = $record["fileBanner"];
		$this->coordMainAreaBanner = $record['coordMainAreaBanner'];
		$this->coordMainAreaLogo = $record['coordMainAreaLogo'];
		$this->fileLogo = $record["fileLogo"];
		$this->webSiteText = $record["webSiteText"];
		$this->webSiteLink = $record["webSiteLink"];
		$this->ctaText = $record["ctaText"];
		$this->ctaLink = $record["ctaLink"];
		$this->thumbUrl = $record["thumbUrl"];
		$this->json = json_decode($record["json"], true);
		$this->jsonCanvas = json_decode($record["jsonCanvas"], true);
		$this->cssStyle = $record["cssStyle"];
	}
	
	/*GETTERS/SETTERS*/
	public function getId(){
		return $this->id;
	}
	public function getIdTemp(){
		return $this->idTemp;
	}
	public function getNome(){
		return $this->nome;
	}
	public function getType(){
		return $this->type;
	}
	public function getHeadText(){
		return $this->headText;
	}
	public function getSubHeadText(){
		return $this->subHeadText;
	}
	public function getColorText(){
		return $this->colorText;
	}
	public function getBgText(){
		return $this->bgText;
	}
	public function getBgBanner(){
		return $this->bgBanner;
	}
	public function getWidth(){
		return $this->width;
	}
	public function getHeight(){
		return $this->height;
	}
	public function getFileBanner(){
		return $this->fileBanner;
	}
	public function getFileLogo(){
		return $this->fileLogo;
	}
	public function getWebSiteText(){
		return $this->webSiteText;
	}
	public function getWebSiteLink(){
		return $this->webSiteLink;
	}
	public function getCtaText(){
		return $this->ctaText;
	}
	public function getCtaLink(){
		return $this->ctaLink;
	}
	public function getCoordMainAreaBanner(){
		return $this->coordMainAreaBanner;
	}
	public function getCoordMainAreaLogo(){
		return $this->coordMainAreaLogo;
	}
	public function getJson(){
		return $this->json;
	}
	
	public function getJsonCanvas(){
		return $this->jsonCanvas;
	}
	
	
	public function getStyle(){
		return $this->cssStyle;
	}
	public function getIdCamp(){
		return $this->idCamp;
	}
	public function getThumbUrl(){
		return $this->thumbUrl;
	}
	public function delete(){
		$sql = "DELETE FROM ".$this->getTable()." WHERE idBanner = '".$this->id."'";
		$resp = $this->query($sql);
		return $resp;
		
	}
	public function saveThumbUrl(){
		/*if($this->getId()){
			$idBanner = $this->getId();
		}else{
			$this->init($idBanner);
		}*/
	
		$idBanner = $this->id;
		$image = Utility::get_data("https://restpack.io/api/screenshot/v2/capture?access_token=lVfmFjAXP2wSrlAkqB79jiLDuYVSZ8bo4PS9clPhhSVcsQ51&url=".ROOT_PATH.$idBanner."/".$this->getIdTemp()."/700x320&width=700&height=320&delay=7000&fresh=true");
		file_put_contents(ROOT_DIR.'tmp/thumbBanner_'.$idBanner.'.png', $image);
	
		$this->thumbUrl = Utility::nullizza(ROOT_PATH.'tmp/thumbBanner_'.$idBanner.'.png');
	
		$sql = "UPDATE ".$this->getTable()." SET ";
		$sql.= "  thumbUrl = $this->thumbUrl";
		$sql.= ", data_mod = now()";
	
		$sql .= " WHERE idBanner = " . $idBanner;
	
		$resp = $this->query($sql);
	}
	/*save*/
	public function saveUpdate($sqlData){
		
		if(array_key_exists('fileBanner', $sqlData)){
			$sqlData['fileBanner'] = end(explode("/assets/",$sqlData['fileBanner']));
		}
		
		if(array_key_exists('fileLogo', $sqlData)){
			$sqlData['fileLogo'] = end(explode("/assets/",$sqlData['fileLogo']));
		}
		
		foreach($sqlData as $k=>$v){
			${$k} = Utility::nullizza($v);
		}
		$result = array();
		//echo "SELECT ID FROM ".$this->getTable()." WHERE ID = $idPreview ";
		
		if(!empty($sqlData['idBanner']) && $sqlData['idBanner'] >0)
			$result = $this->select("SELECT idBanner FROM ".$this->getTable()." WHERE idBanner = $idBanner ", false);
			
			if(count($result)>0){
				//update
				$sql = "UPDATE ".$this->getTable()." SET ";
				$sql.= (isset($idBanner))?"idBanner = $idBanner":"";
				$sql.= (isset($idCamp))?", idCamp = $idCamp":"";
				$sql.= (isset($idTemp))?", idTemp = $idTemp":"";
				$sql.= (isset($type))?", type = $type":"";
				$sql.= (isset($nome))?", nome = $nome":"";
				$sql.= (isset($width))?", width = $width":"";
				$sql.= (isset($height))?", height = $height":"";
				$sql.= (isset($bgBanner))?", bgBanner = $bgBanner":"";
				$sql.= (isset($fileBanner))?", fileBanner = $fileBanner":"";
				$sql.= (isset($fileLogo))?", fileLogo = $fileLogo":"";
				$sql.= (isset($coordMainAreaBanner))?", coordMainAreaBanner = $coordMainAreaBanner":"";
				$sql.= (isset($coordMainAreaLogo))?", coordMainAreaLogo = $coordMainAreaLogo":"";
				$sql.= (isset($fileLogo))?", fileLogo = $fileLogo":"";
				$sql.= (isset($headText))?", headText = $headText":"";
				$sql.= (isset($subHeadText))?", subHeadText = $subHeadText":"";
				$sql.= (isset($colorText))?", colorText = $colorText":"";
				$sql.= (isset($bgText))?", bgText = $bgText":"";
				$sql.= (isset($webSiteText))?", webSiteText = $webSiteText":"";
				$sql.= (isset($webSiteText))?", webSiteLink = $webSiteLink":"";
				$sql.= (isset($ctaText))?", ctaText = $ctaText":"";
				$sql.= (isset($ctaLink))?", ctaLink = $ctaLink":"";
				$sql.= (isset($json))?", json = $json":"";
				$sql.= (isset($cssStyle))?", cssStyle = $cssStyle":"";
				$sql.= ", data_mod = now()";
	
				$sql .= " WHERE idBanner = " . $idBanner;
	
				$resp = $this->query($sql);
				
				$this->idTemp =  $sqlData['idTemp']? $sqlData['idTemp']:'A';
				$this->id = $sqlData['idBanner'];
				
				$this->saveThumbUrl();
				
				return $sqlData['idBanner'];
			} else {
	
				$sql = "INSERT INTO ".$this->getTable()." (";
				$sql.= "idCamp,";
				$sql.= "idTemp,";
				$sql.= "type,";
				$sql.= "nome,";
				$sql.= "width,";
				$sql.= "height,";
				$sql.= "bgBanner,";
				$sql.= "fileBanner,";
				$sql.= "fileLogo,";
				$sql.= "coordMainAreaBanner,";
				$sql.= "coordMainAreaLogo,";
				$sql.= "headText,";
				$sql.= "subHeadText,";
				$sql.= "colorText,";
				$sql.= "bgText,";
				$sql.= "webSiteText,";
				$sql.= "webSiteLink,";
				$sql.= "ctaText,";
				$sql.= "ctaLink,";
				$sql.= "data_ins";

				$sql.= ")VALUES(";
				$sql.= "$idCamp,";
				$sql.= "$idTemp,";
				$sql.= "$type,";
				$sql.= "$nome,";
				$sql.= "$width,";
				$sql.= "$height,";
				$sql.= "$bgBanner,";
				$sql.= "$fileBanner,";
				$sql.= "$fileLogo,";
				$sql.= "$coordMainAreaBanner,";
				$sql.= "$coordMainAreaLogo,";
				$sql.= "$headText,";
				$sql.= "$subHeadText,";
				$sql.= "$colorText,";
				$sql.= "$bgText,";
				$sql.= "$webSiteText,";
				$sql.= "$webSiteLink,";
				$sql.= "$ctaText,";
				$sql.= "$ctaLink,";
				$sql.= "now() ";
				$sql.= ")";
				
				$idBanner = $this->insert($sql);
				
				$this->idTemp =  $sqlData['idTemp']? $sqlData['idTemp']:'A';
				$this->id = $idBanner;
				
				$this->saveThumbUrl();
				
				return $idBanner;
			}
			
			
	
			return $resp;
	}
	
}



class BannerList extends DatabaseTable{
	private $items;
	private $filled;
	
	public function __construct($db_conn){
		if(!isset($table) || empty($table) || is_null($table)){
			$table = "pot_banner";
		}
		parent::__construct($db_conn);
		$this->items = array();
	}
	
	public function init(){
		$this->clear();
		$sql = "SELECT * FROM pot_banner WHERE type='banner'";
		$result = $this->select($sql,false);
		foreach ($result as $record){
			$tmpItem = new Banner($this->get_db_connection());
			$tmpItem->initByRecord($record);
			$this->add($tmpItem);
		}
	}
	
	public function add(Banner $a){

		foreach($this->items as $k => $v){ 
			if($v->getId() == $a->getId()){ 
				unset($this->items[$k]);
			} 
		}
		array_push($this->items, $a);

	}
	
	public function toArray($i = null){
		if($i === null){
			return $this->items;
		}else{
			return $this->items[$i];
		}
	}
	
	public function count(){
		return count($this->items);
	}

	public function clear(){
		$this->items = array();
	}
	
	public function getId(){
		$ids = array();
		foreach ($this->items as $itm){
			array_push($ids, $itm->getId());
		}
		return $ids;
	}
		

}


?>