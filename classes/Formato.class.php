<?php
class Formato extends DatabaseTable{
	private $id;
	private $categoria;
	private $nome;
	private $width;
	private $height;
	
	public function __construct(Database $db_conn, $id = NULL){
		parent::__construct($db_conn);
		$this->setStatus(DatabaseTable::NEW_RECORD);
		$this->setTable("pot_formato");
		
		if($id != NULL){
			$this->init($id);
		}
	}
	
	public function init($id){
		if(isset($id) && !empty($id)){
			$result = $this->select("SELECT * FROM pot_formati WHERE idFormato = '" . $id . "'", false);
			if(count($result)>0){
				$this->initByRecord($result[0]);
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
	public function initByRecord($record){
		$this->setStatus(DatabaseTable::LOADED_RECORD);
		$this->id = $record["idFormato"];
		$this->categoria = $record["categoria"];
		$this->nome = $record["nome"];
		$this->width = $record["width"];
		$this->height = $record["height"];
	}
	
	/*GETTERS/SETTERS*/
	public function getId(){
		return $this->id;
	}
	
	public function getCategoria(){
		return $this->categoria;
	}
	public function getNome(){
		return $this->nome;
	}
	public function getWidth(){
		return $this->width;
	}
	public function getHeight(){
		return $this->height;
	}
	public function getScaledWidth(){
		return $this->width / 1500 * (COL_CONTENT_WIDTH - 20);
	}
	public function getScaledHeight(){
		return $this->height / 1500 * (COL_CONTENT_WIDTH - 20);
	}
	
}



class FormatoList extends DatabaseTable{
	private $items;
	private $filled;
	
	public $orderBy = ["ordine" => "asc"];
	public $where = "1=1";
	
	public function __construct($db_conn){
		if (!isset($table) || empty($table) || is_null($table)) {
			$table = "pot_formati";
		}
		parent::__construct($db_conn);
		$this->items = array();
	}
	
	public function init($where = "1=1"){
		$this->clear();
		$sql = "SELECT * FROM pot_formati WHERE $where";
		
		$orderBy = "";
		foreach($this->orderBy as $key => $order) {
			$orderBy .= ",$key $order";
		}
		
		if (strlen($orderBy) > 0) {
			$sql .= " ORDER BY " . ltrim($orderBy, ",");
		}
		
		$result = $this->select($sql,false);
		foreach ($result as $record){
			$tmpItem = new Formato($this->get_db_connection());
			$tmpItem->initByRecord($record);
			$this->add($tmpItem);
		}
	}
	
	public function initByDim($formatWidth, $formatHeight){
		$this->init("width = '" . $formatWidth . "' AND height='" . $formatHeight . "'");
	}
	
	public function initAllVisible ()
	{
		$where = "isVisible = 1";
		$this->init($where);
	}
	
	public function add(Formato $a){

		foreach($this->items as $k => $v){ 
			if($v->getId() == $a->getId()){ 
				unset($this->items[$k]);
			} 
		}
		array_push($this->items, $a);

	}
	
	public function toArray($i = null){
		if($i === null){
			return $this->items;
		}else{
			return $this->items[$i];
		}
	}
	
	public function count(){
		return count($this->items);
	}

	public function clear(){
		$this->items = array();
	}
	
	public function getId(){
		$ids = array();
		foreach ($this->items as $itm){
			array_push($ids, $itm->getId());
		}
		return $ids;
	}
		

}


?>