<?php

require_once ROOT_DIR."classes/Base.class.php";
require_once ROOT_DIR."classes/CustomList.class.php";

require_once ROOT_DIR."classes/Campagna.class.php";
require_once ROOT_DIR."classes/Base.class.php";
require_once ROOT_DIR."classes/Formato.class.php";

class Banner extends Base{
    const TABLE = "banner";
	
	/*VARIABLES*/
	private $_idAlfanumeric = "";
	private $_idCamp = 0;
	private $_objCamp = null;
	private $_idFormato = 5;
	private $_objFormato = null;
	private $_isCustom = 0;
	private $_imageUrl = null;
	private $_imageThumbUrl = null;
	private $_variableCaches = null;
	private $_name = "";
	private $_width = null;
	private $_height = null;
	
		
	/*VARIABLES - FINE*/
	
	/*VARIABLES - GET/SET*/
	public function setId($v){
		$this->id = $v;
		return $this;
	}
	public function setIdAlfanumeric(){
		$this->_idAlfanumeric = uniqid();
	}
	public function getIdAlfanumeric(){
		return $this->_idAlfanumeric;
	}
	public function setCustom($v){
		$this->_isCustom = intval($v) ? true : false;
		return $this;
	}
	
	public function setName($v){
		$this->_name = $v;
		return $this;
	}
	
	public function setIdCampagna($v){
		$this->_idCamp = $v;
		$this->_objCamp = null;
		return $this;
	}
	
	public function getObjCampagna(){
		if($this->_objCamp === null){
			$this->_objCamp = new Campagna($this->get_db_connection());
			if($this->_objCamp->init($this->_idCamp)){
				//tutto apposto
			}else{
				$this->_objCamp = false;
			}
		}
		return $this->_objCamp;
	}
	
	
	public function setIdFormato($v){
		$this->_idFormato = $v;
		$this->_objFormato = null;
		
		return $this;
	}
	public function getIdFormato(){	
		return $this->_idFormato;
	}
	public function getObjFormat(){
		if($this->_objFormato === null){
			$this->_objFormato = new Formato($this->get_db_connection());
			if($this->_objFormato->init($this->_idFormato)){
				//tutto apposto
			}else{
				$this->_objFormato = false;
			}
		}
		return $this->_objFormato;
	}
	
	public function setWidth($v){
		$this->_width = $v;
		return $this;
	}
	
	public function getWidth(){
		$width = $this->_width;
		if(!$this->_isCustom){
			$width = $this->getObjFormat()->getWidth();
		}
		return $width;
	}
	
	public function setHeight($v){
		$this->_height = $v;
		return $this;
	}
	
	public function getHeight(){
		$height = $this->_height;
		if(!$this->_isCustom){
			$height = $this->getObjFormat()->getHeight();
		}
		return $height;
		
	}
	public function getName(){
		return $this->_name;
	}	
	public function getImageThumbUrl(){
		return $this->_imageThumbUrl;
	}
	public function getImageUrl(){
		return $this->_imageUrl;
	}
	/*VARIABLES - GET/SET - FINE*/
	
	
	/*INIT*/
	public function __construct($db_conn = null, $ident = null){
		parent::__construct($db_conn, $ident);
		$this->columnList = array(
								  "id"=>"setId",
								  "idAlfanumeric"=>"setIdAlfanumeric",
								  "idCamp"=>"setIdCampagna",
								  "idFormato"=>"setIdFormato",
								  "isCustom"=>"setCustom",
								  "width"=>"setWidth",
								  "height" => "setHeight",
								  "name"=>"setName",
								  "userIns"=>"",
								  "userMod"=>"",
								  "imageUrl"=>"",
								  "imageThumbUrl"=>"",
								  "data_ins"=>"",
								  "data_mod" => "",
								  "variableCaches" => ""
								  );
		
	}
	
	public function initByAlpha($alphaId = ""){

		//`id`, `idAlfanumeric`, `idCamp`, `idFormato`, `isCustom`, `width`, `height`, `name`, `userIns`, `userMod`, `imageUrl`, `imageThumbUrl`, `data_ins`, `data_mod`, `variableCaches`
		if(isset($alphaId) && !empty($alphaId)){
			$result = $this->select("SELECT * FROM " . $this->getTable(). " WHERE idAlfanumeric LIKE \"" . $alphaId . "\"");
			if(count($result)>0){
				$this->initByRecord($result[0]);
				return $this;
			}else{
				return null;
			}
		}else{
			return null;
		}
	}
	
	/*INIT - FINE*/
	
	/* FUNCTIONS */
	
	public function getTemplateObj(){
		if($this->getObjCampagna()){
			return $this->getObjCampagna()->getTemplateObj();
		}
		return false;
	}
	
	
	
    /*save*/
	public function save(){
		$idAlfanumeric = Utility::nullizza($this->_idAlfanumeric);
		$idCamp = Utility::nullizza($this->_idCamp);
		$idFormato = Utility::nullizza($this->_idFormato);
		$isCustom = Utility::nullizza($this->_isCustom);
		$width = Utility::nullizza($this->_width);
		$height = Utility::nullizza($this->_height);
		$name = Utility::nullizza($this->_name);
		$imageUrl = Utility::nullizza($this->_imageUrl);
		$imageThumbUrl = Utility::nullizza($this->_imageThumbUrl);
		$variableCaches = Utility::nullizza($this->_variableCaches);
		
		if(!empty($this->id) && $this->id >0)
			$result = $this->select("SELECT id FROM ".$this->getTable()." WHERE id = $this->id ", false);
		
		if(isset($result) && count($result)>0){	
			$sql = "UPDATE ".$this->getTable()." SET ";
			$sql.= " idAlfanumeric = $idAlfanumeric";
			$sql.= ", idCamp = $idCamp";
			$sql.= ", idFormato = $idFormato";
			$sql.= ", isCustom = 0";
			$sql.= ", width = $width";
			$sql.= ", height = $height";
			$sql.= ", name = $name";
			$sql.= ", imageUrl = $imageUrl";
			$sql.= ", imageThumbUrl = $imageThumbUrl";
			$sql.= ", variableCaches = $variableCaches";
			
			$sql.= ", data_mod = now()";
			
			$sql.= " WHERE id = " . $this->id;
			
			$resp = $this->query($sql);
			
			return $this->id;
		} else {
			$sql = "INSERT INTO ".$this->getTable()." (";
			$sql.= "idAlfanumeric,";
			$sql.= "idCamp,";
			$sql.= "idFormato,";
			$sql.= "isCustom,";
			$sql.= "width,";
			$sql.= "height,";
			$sql.= "name,";
			$sql.= "imageUrl,";
			$sql.= "imageThumbUrl,";
			$sql.= "variableCaches,";
			$sql.= "data_ins";
			
			$sql.= ")VALUES(";
			$sql.= "'".uniqid()."',";
			$sql.= "$idCamp,";
			$sql.= "$idFormato,";
			$sql.= "0,";
			$sql.= "$width,";
			$sql.= "$height,";
			$sql.= "$name,";
			$sql.= "$imageUrl,";
			$sql.= "$imageThumbUrl,";
			$sql.= "$variableCaches,";
			$sql.= "now() ";
			$sql.= ")";
			
			$idBanner = $this->insert($sql);
			
			$this->id = $idBanner;
			
			return $this->id;
		}
        //throw new Exception("generic ");
	}
	
	
	/*CLASSI STATICHE*/
	/*CLASSI STATICHE FINE*/
}

class BannerList extends CustomList{
	const TABLE = "banner";
	public function __construct($db_conn = null, $ident = null){
		parent::__construct($db_conn, $ident);
	}
	public function init(){
		$this->clear();
		$sql = "SELECT * FROM ".$this->getTable();
		$result = $this->select($sql,false);
		foreach ($result as $record){
			$tmpItem = new Banner($this->get_db_connection());
			$tmpItem->initByRecord($record);
			$this->add($tmpItem);
		}
	}
	public function initbyIdCamp($v){
		$this->clear();
		$sql = "SELECT * FROM ".$this->getTable()." WHERE idCamp=".$v;
		$result = $this->select($sql,false);
		foreach ($result as $record){
			$tmpItem = new Banner($this->get_db_connection());
			$tmpItem->initByRecord($record);
			$this->add($tmpItem);
		}
	}
}

