<?php
class BannerPreview extends DatabaseTable{
	private $ID;
	private $idBanner;
	private $html = null;
	private $version;
	private $thumbUrl;
	
	public function __construct(Database $db_conn, $id = NULL){
		parent::__construct($db_conn);
		$this->setStatus(DatabaseTable::NEW_RECORD);
		$this->setTable("pot_preview");
		
		if($id != NULL){
			$this->init($id);
		}
	}
	
	public function init($id){
		if(isset($id) && !empty($id)){
			$result = $this->select("SELECT * FROM ".$this->getTable()." WHERE ID = '" . $id . "'", false);
			if(count($result)>0){
				$this->initByRecord($result[0]);
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
	public function initByRecord($record){
		$this->setStatus(DatabaseTable::LOADED_RECORD);
		$this->ID = $record["ID"];
		$this->idBanner = $record["idBanner"];
		$this->html = $record["html"];
		$this->url = $record["url"];
		$this->version = $record["version"];
		$this->thumbUrl = $record["thumbUrl"];
	}
	
	/*GETTERS/SETTERS*/
	public function getId(){
		return $this->ID;
	}	
	public function getIdBanner(){
		return $this->idBanner;
	}
	public function getHtml(){
		return $this->html;
	}
	public function getUrl(){
		return $this->url;
	}
	public function getVersion(){
		return $this->version;
	}	
	
	public function delete(){
		echo $sql = "DELETE FROM ".$this->getTable()." WHERE ID = '".$this->ID."'";
		$resp = $this->query($sql);
		return $resp;
	
	}
	
	public function saveThumbUrl($idPrev=0){
		if($this->getId()){
			$idPrev = $this->getId();
		}else{
			$this->init($idPrev);
		}
		
		$image = Utility::get_data("https://restpack.io/api/screenshot/v2/capture?access_token=lVfmFjAXP2wSrlAkqB79jiLDuYVSZ8bo4PS9clPhhSVcsQ51&url=".ROOT_PATH."preview/".$idPrev."&thumbnail_width=270&thumbnail_height=270");
		
		file_put_contents(ROOT_DIR.'assets/previewThumb/thumbPrev_'.$idPrev.'.png', $image);
		
		$this->thumbUrl = Utility::nullizza(ROOT_PATH.'assets/previewThumb/thumbPrev_'.$idPrev.'.png');
		
		$sql = "UPDATE ".$this->getTable()." SET ";
		$sql.= "  thumbUrl = $this->thumbUrl";
		$sql.= ", data_mod = now()";
		
		$sql .= " WHERE ID = " . $idPrev;
		
		$resp = $this->query($sql);
	}
	
	public function getThumbUrl(){
		return $this->thumbUrl;
	}
	/*save*/
	public function saveUpdate($sqlData){
		//var_dump($sqlData);
		foreach($sqlData as $k=>$v){
			${$k} = Utility::nullizza($v);
		}
		
		$result = array();
		//echo "SELECT ID FROM ".$this->getTable()." WHERE ID = $idPreview ";
		if(!empty($sqlData['idPreview']) && $sqlData['idPreview'] >0)
			$result = $this->select("SELECT ID FROM ".$this->getTable()." WHERE ID = $idPreview ", false);
		
		if(count($result)>0){	
			//update
			$sql = "UPDATE ".$this->getTable()." SET ";
			$sql.= "  idBanner = $idBanner";
			$sql.= ", html = $html";
			$sql.= ", url = $url";
			$sql.= ", data_mod = now()";

			$sql .= " WHERE ID = " . $idPreview;
			
			$resp = $this->query($sql);
			
			$this->saveThumbUrl();
			
			return $sqlData['idPreview'];
			
		} else {
			
			$sth = $this->get_db_connection()->getResource()->prepare("INSERT INTO  pot_preview (idBanner, html, url) VALUES(:idBanner,:html,:url);");
			
			$i = $sqlData['idBanner'];
			$i2 = $sqlData['html'];
			$i3 = $sqlData['url'];
			
			
			$sth->bindParam(":idBanner", $i, PDO::PARAM_INT);
			$sth->bindParam(":html", $i2, PDO::PARAM_STR);
			$sth->bindParam(":url", $i3, PDO::PARAM_STR);
			$sth->execute();

			/*$sql = "INSERT INTO ".$this->getTable()." (";
			$sql.= " idBanner,
					 html,
					 url,
		 			 data_ins";
			$sql.= ")VALUES(";
			$sql.= "$idBanner,";
			$sql.= "$html,";
			$sql.= "$url,";
			$sql.= "now() ";
			$sql.= ")";*/
			
			//$resp = $this->query($sql);
			
			$this->ID = $this->get_db_connection()->getResource()->lastInsertId();
			
			$this->saveThumbUrl();
			
			return $this->ID;
		}
		
		return $resp;
	}
	
}



class BannerPreviewList extends DatabaseTable{
	private $items;
	private $filled;
	
	public function __construct($db_conn){
		if(!isset($table) || empty($table) || is_null($table)){
			$table = "pot_banner";
		}
		parent::__construct($db_conn);
		$this->items = array();
	}
	
	public function init(){
		$this->clear();
		$sql = "SELECT * FROM pot_preview";
		$result = $this->select($sql,false);
		foreach ($result as $record){
			$tmpItem = new BannerPreview($this->get_db_connection());
			$tmpItem->initByRecord($record);
			$this->add($tmpItem);
		}
	}
	
	public function initByBannerId($id){
		$this->clear();
		$sql = "SELECT * FROM pot_preview WHERE idBanner=$id";
		$result = $this->select($sql,false);
		foreach ($result as $record){
			$tmpItem = new BannerPreview($this->get_db_connection());
			$tmpItem->initByRecord($record);
			$this->add($tmpItem);
		}
	}
	
	public function add(BannerPreview $a){

		foreach($this->items as $k => $v){ 
			if($v->getId() == $a->getId()){ 
				unset($this->items[$k]);
			} 
		}
		array_push($this->items, $a);

	}
	
	public function toArray($i = null){
		if($i === null){
			return $this->items;
		}else{
			return $this->items[$i];
		}
	}
	
	public function count(){
		return count($this->items);
	}

	public function clear(){
		$this->items = array();
	}
	
	public function getId(){
		$ids = array();
		foreach ($this->items as $itm){
			array_push($ids, $itm->getId());
		}
		return $ids;
	}
		

}


?>