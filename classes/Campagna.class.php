<?php
require_once ROOT_DIR . "classes/Base.class.php";
require_once ROOT_DIR . "classes/Template.class.php";
require_once ROOT_DIR . "classes/CustomList.class.php";
require_once ROOT_DIR . "classes/Banner.class.php";
class Campagna extends Base {
	const TABLE = "campaign";
	
	/* VARIABLES */
	private $_idAlfanumeric = "";
	private $_idTemplate = 0;
	private $_templateObj = null;
	private $_type = null;
	private $_name = "";
	private $_preview = null;
	private $_variableCache = null;
	private $_dataCreazione = null;
	private $_bannerList = null;
	private $_bgUrl = null;
	private $_logoUrl = null;
	private $_colorBg = null;
	private $_headText = null;
	private $_subHeadText = null;
	private $_webSiteText = null;
	private $_webSiteUrl = null;
	private $_ctaText = null;
	private $_ctaUrl = null;
	private $_idWrapperTesti = 0;
	private $_coordMainAreaBanner = null;
	private $_coordMainAreaLogo = null;
	
	/* VARIABLES - FINE */
	
	/* VARIABLES - GET/SET */
	public function setId($v) {
		$this->id = $v;
	}
	public function setIdentAlfanumeric($v) {
		$this->_idAlfanumeric = $v;
		return $this;
	}
	public function getIdentAlfanumeric() {
		return $this->_idAlfanumeric;
	}
	public function setTemplate($v) {
		$this->_idTemplate = $v;
		$this->_templateObj = null;
		return $this;
	}
	public function getTemplate() {
		return $this->_idTemplate;
	}
	public function setType($v) {
		$this->_type = $v;
	}
	public function getType() {
		return $this->_type;
	}
	public function setBlockParent($v) {
		$this->_idBlockParent = $v;
		return $this;
	}
	public function setName($v) {
		$this->_name = $v;
		return $this;
	}
	public function getName() {
		return $this->_name;
	}
	public function setBgUrl($v) {
		$this->_bgUrl = $v;
	}
	public function setLogoUrl($v) {
		$this->_logoUrl = $v;
	}
	public function getLogoUrl($v) {
		return $this->_logoUrl;
	}
	public function setHeadtext($v) {
		$this->_headText = $v;
	}
	public function setSubHeadtext($v) {
		$this->_subHeadText = $v;
	}
	public function setColorBg($v) {
		$this->_colorBg = $v;
	}
	public function setWebSiteText($v) {
		$this->_webSiteText = $v;
	}
	public function setWebSiteUrl($v) {
		$this->_webSiteUrl = $v;
	}
	public function setCtaText($v) {
		$this->_ctaText = $v;
	}
	public function setCtaUrl($v) {
		$this->_ctaUrl = $v;
	}
	public function setPreview($v) {
		$this->_preview = $v;
		return $this;
	}
	public function getPreview() {
		return $this->_preview;
	}
	public function setVariabileCache($v) {
		$this->_variableCache = $v;
		return $this;
	}
	public function setDataCreazione($v) {
		$this->_dataCreazione = $v;
		return $this;
	}
	public function setCoordMainAreaBanner($v){
		$this->_coordMainAreaBanner = $v;
	}
	public function setCoordMainAreaLogo($v){
		$this->_coordMainAreaLogo = $v;
	}
	public function saveCoordMainAreaBanner(){
		$arrayCoord = json_decode($this->_coordMainAreaBanner);
		$sql = "SELECT id FROM ". DB_PREF . "campaign_block WHERE idCampagna=".$this->id." AND idTipoBlocco=2 AND nomeVariabile='main' AND idValore=2 AND arrayIndex=0";
		$result = $this->select($sql);
		
		$idBlock = (!empty($result) && isset($result[0]['id']))? $result[0]['id'] :"";
		
		if(!empty($idBlock)){
		
			$sql = "UPDATE ". DB_PREF . "campaign_block SET";
			$sql .= " valore = ".$arrayCoord->x1;
			$sql .= " WHERE id = $idBlock";
		
			$this->query($sql);
		
			$sql = "SELECT id FROM ". DB_PREF . "campaign_block WHERE idCampagna=".$this->id." AND idTipoBlocco=2 AND nomeVariabile='main' AND idValore=3 AND arrayIndex=0";
			$result = $this->select($sql);
			
			$idBlock = (!empty($result) && isset($result[0]['id']))? $result[0]['id'] :"";
			
			$sql = "UPDATE ". DB_PREF . "campaign_block SET";
			$sql .= " valore = ".$arrayCoord->y1;
			$sql .= " WHERE id = $idBlock";
			
			$this->query($sql);
			
			$sql = "SELECT id FROM ". DB_PREF . "campaign_block WHERE idCampagna=".$this->id." AND idTipoBlocco=2 AND nomeVariabile='main' AND idValore=4 AND arrayIndex=0";
			$result = $this->select($sql);
				
			$idBlock = (!empty($result) && isset($result[0]['id']))? $result[0]['id'] :"";
				
			$sql = "UPDATE ". DB_PREF . "campaign_block SET";
			$sql .= " valore = ".$arrayCoord->x2;
			$sql .= " WHERE id = $idBlock";
				
			$this->query($sql);
			
			$sql = "SELECT id FROM ". DB_PREF . "campaign_block WHERE idCampagna=".$this->id." AND idTipoBlocco=2 AND nomeVariabile='main' AND idValore=5 AND arrayIndex=0";
			$result = $this->select($sql);
			
			$idBlock = (!empty($result) && isset($result[0]['id']))? $result[0]['id'] :"";
			
			$sql = "UPDATE ". DB_PREF . "campaign_block SET";
			$sql .= " valore = ".$arrayCoord->y2;
			$sql .= " WHERE id = $idBlock";
			
			$this->query($sql);
		
		} else{
				
			$sql = "INSERT INTO " . DB_PREF . "campaign_block (";
			$sql .= "idCampagna,";
			$sql .= "idTipoBlocco,";
			$sql .= "idBloccoPadre,";
			$sql .= "nomeVariabile,";
			$sql .= "idValore,";
			$sql .= "valore,";
			$sql .= "arrayIndex";
				
			$sql .= ")VALUES(";
			$sql .= Utility::nullizza ( $this->id ) . ",";
			$sql .= "2,";
			$sql .= "NULL,";
			$sql .= "'main',";
			$sql .= "2,";
			$sql .= Utility::nullizza ( $arrayCoord->x1 ) . ",";
			$sql .= "0";
			$sql .= ")";
				
			$this->insert ( $sql );
			
			$sql = "INSERT INTO " . DB_PREF . "campaign_block (";
			$sql .= "idCampagna,";
			$sql .= "idTipoBlocco,";
			$sql .= "idBloccoPadre,";
			$sql .= "nomeVariabile,";
			$sql .= "idValore,";
			$sql .= "valore,";
			$sql .= "arrayIndex";
			
			$sql .= ")VALUES(";
			$sql .= Utility::nullizza ( $this->id ) . ",";
			$sql .= "2,";
			$sql .= "NULL,";
			$sql .= "'main',";
			$sql .= "3,";
			$sql .= Utility::nullizza ( $arrayCoord->y1 ) . ",";
			$sql .= "0";
			$sql .= ")";
			
			$this->insert ( $sql );
			
			$sql = "INSERT INTO " . DB_PREF . "campaign_block (";
			$sql .= "idCampagna,";
			$sql .= "idTipoBlocco,";
			$sql .= "idBloccoPadre,";
			$sql .= "nomeVariabile,";
			$sql .= "idValore,";
			$sql .= "valore,";
			$sql .= "arrayIndex";
			
			$sql .= ")VALUES(";
			$sql .= Utility::nullizza ( $this->id ) . ",";
			$sql .= "2,";
			$sql .= "NULL,";
			$sql .= "'main',";
			$sql .= "4,";
			$sql .= Utility::nullizza ( $arrayCoord->x2 ) . ",";
			$sql .= "0";
			$sql .= ")";
			
			$this->insert ( $sql );
			
			$sql = "INSERT INTO " . DB_PREF . "campaign_block (";
			$sql .= "idCampagna,";
			$sql .= "idTipoBlocco,";
			$sql .= "idBloccoPadre,";
			$sql .= "nomeVariabile,";
			$sql .= "idValore,";
			$sql .= "valore,";
			$sql .= "arrayIndex";
			
			$sql .= ")VALUES(";
			$sql .= Utility::nullizza ( $this->id ) . ",";
			$sql .= "2,";
			$sql .= "NULL,";
			$sql .= "'main',";
			$sql .= "5,";
			$sql .= Utility::nullizza ( $arrayCoord->y2 ) . ",";
			$sql .= "0";
			$sql .= ")";
			
			$this->insert ( $sql );
		}
	}
	public function saveCoordMainAreaLogo(){
	
	}
	public function savePreview() {
		$idBanner = $this->id;
		$image = Utility::get_data ( "https://restpack.io/api/screenshot/v2/capture?access_token=lVfmFjAXP2wSrlAkqB79jiLDuYVSZ8bo4PS9clPhhSVcsQ51&url=" . ROOT_PATH . "/camp/" . $this->_idAlfanumeric . "/width/700/height/320&width=700&height=320&fresh=true" );
		file_put_contents ( ROOT_DIR . 'assets/projectThumb/thumbBanner_' . $idBanner . '.png', $image );
		
		$this->preview = Utility::nullizza ( ROOT_PATH . 'assets/projectThumb/thumbBanner_' . $idBanner . '.png' );
		
		$sql = "UPDATE " . $this->getTable () . " SET ";
		$sql .= "  preview = $this->preview";
		$sql .= " WHERE id = " . $idBanner;
		
		$resp = $this->query ( $sql );
	}
	public function saveBg() {
		$sql = "SELECT id FROM ". DB_PREF . "campaign_block WHERE idCampagna=".$this->id." AND idTipoBlocco=2 AND nomeVariabile='main' AND idValore=1 AND arrayIndex=0";
		$result = $this->select($sql);
		
		$idBlock = (!empty($result) && isset($result[0]['id']))? $result[0]['id'] :"";
		
		if(!empty($idBlock)){
			
			$sql = "UPDATE ". DB_PREF . "campaign_block SET";
			$sql .= " valore = ".Utility::nullizza ( $this->_bgUrl );
			$sql .= " WHERE id = $idBlock";
			
			$this->query($sql);
				
			return $idBlock;
			
		} else{
			
			$sql = "INSERT INTO " . DB_PREF . "campaign_block (";
			$sql .= "idCampagna,";
			$sql .= "idTipoBlocco,";
			$sql .= "idBloccoPadre,";
			$sql .= "nomeVariabile,";
			$sql .= "idValore,";
			$sql .= "valore,";
			$sql .= "arrayIndex";
				
			$sql .= ")VALUES(";
			$sql .= Utility::nullizza ( $this->id ) . ",";
			$sql .= "2,";
			$sql .= "NULL,";
			$sql .= "'main',";
			$sql .= "1,";
			$sql .= Utility::nullizza ( $this->_bgUrl ) . ",";
			$sql .= "0";
			$sql .= ")";
			
			return $this->insert ( $sql );
			
		}
	}
	public function saveLogo() {
		$sql = "SELECT id FROM ". DB_PREF . "campaign_block WHERE idCampagna=".$this->id." AND idTipoBlocco=3 AND nomeVariabile='logo' AND idValore=16 AND arrayIndex=0";
		$result = $this->select($sql);
		
		$idBlock = (!empty($result) && isset($result[0]['id']))? $result[0]['id'] :"";
		
		if(!empty($idBlock)){
			
			$sql = "UPDATE ". DB_PREF . "campaign_block SET";
			$sql .= " valore = ".Utility::nullizza ( $this->_logoUrl );
			$sql .= " WHERE id = $idBlock";
			
			$this->query($sql);
				
			return $idBlock;
			
		} else{
			$sql = "INSERT INTO " . DB_PREF . "campaign_block (";
			$sql .= "idCampagna,";
			$sql .= "idTipoBlocco,";
			$sql .= "idBloccoPadre,";
			$sql .= "nomeVariabile,";
			$sql .= "idValore,";
			$sql .= "valore,";
			$sql .= "arrayIndex";
			
			$sql .= ")VALUES(";
			$sql .= Utility::nullizza ( $this->id ) . ",";
			$sql .= "3,";
			$sql .= "NULL,";
			$sql .= "'logo',";
			$sql .= "16,";
			$sql .= Utility::nullizza ( $this->_logoUrl ) . ",";
			$sql .= "0";
			$sql .= ")";
			
			return $this->insert ( $sql );
		}
	}
	public function saveColorBg() {
		
		$sql = "SELECT id FROM ". DB_PREF . "campaign_block WHERE idCampagna=".$this->id." AND idTipoBlocco=1 AND nomeVariabile='main' AND idValore=10 AND arrayIndex=0";
		$result = $this->select($sql);
		
		$idBlock = (!empty($result) && isset($result[0]['id']))? $result[0]['id'] :"";
		
		if(!empty($idBlock)){
				
			$sql = "UPDATE ". DB_PREF . "campaign_block SET";
			$sql .= " valore = ".Utility::nullizza ( $this->_colorBg );
			$sql .= " WHERE id = $idBlock";
				
			$this->query($sql);
		
			return $idBlock;
				
		} else{
			
			$sql = "INSERT INTO " . DB_PREF . "campaign_block (";
			$sql .= "idCampagna,";
			$sql .= "idTipoBlocco,";
			$sql .= "idBloccoPadre,";
			$sql .= "nomeVariabile,";
			$sql .= "idValore,";
			$sql .= "valore,";
			$sql .= "arrayIndex";
			
			$sql .= ")VALUES(";
			$sql .= Utility::nullizza ( $this->id ) . ",";
			$sql .= "1,";
			$sql .= "NULL,";
			$sql .= "'main',";
			$sql .= "10,";
			$sql .= Utility::nullizza ( $this->_colorBg ) . ",";
			$sql .= "0";
			$sql .= ")";
			
			return $this->insert ( $sql );
		}
	}
	public function saveWrapperTesti() {
		
		$sql = "SELECT id FROM ". DB_PREF . "campaign_block WHERE idCampagna=".$this->id." AND idTipoBlocco=5 AND nomeVariabile='wrapperTestiComposti' AND idValore=18 AND arrayIndex=0";
		$result = $this->select($sql);
		
		$idBlock = (!empty($result) && isset($result[0]['id']))? $result[0]['id'] :"";
		
		if(!empty($idBlock)){
				
			#$sql = "UPDATE ". DB_PREF . "campaign_block SET";
			#$sql .= " valore = ".Utility::nullizza ( $this->_bgUrl );
			#$sql .= " WHERE id = $idBlock";
				
			#$this->query($sql);
		
			return $idBlock;
				
		} else{
			$sql = "INSERT INTO " . DB_PREF . "campaign_block (";
			$sql .= "idCampagna,";
			$sql .= "idTipoBlocco,";
			$sql .= "idBloccoPadre,";
			$sql .= "nomeVariabile,";
			$sql .= "idValore,";
			$sql .= "valore,";
			$sql .= "arrayIndex";
			
			$sql .= ")VALUES(";
			$sql .= Utility::nullizza ( $this->id ) . ",";
			$sql .= "5,";
			$sql .= "NULL,";
			$sql .= "'wrapperTestiComposti',";
			$sql .= "18,";
			$sql .= "'',";
			$sql .= "0";
			$sql .= ")";
			
			return $this->insert ( $sql );
		}
	}
	public function saveHeadText() {
		if ($this->_idWrapperTesti == 0)
			$this->_idWrapperTesti = $this->saveWrapperTesti ();
		
		$sql = "SELECT id FROM ". DB_PREF . "campaign_block WHERE idCampagna=".$this->id." AND idTipoBlocco=5 AND idBloccoPadre=".$this->_idWrapperTesti." AND nomeVariabile='wrapperTestiComposti' AND idValore=9 AND arrayIndex=0";
		$result = $this->select($sql);
		
		$idBlock = (!empty($result) && isset($result[0]['id']))? $result[0]['id'] :"";
		
		if(!empty($idBlock)){
		
			$sql = "UPDATE ". DB_PREF . "campaign_block SET";
			$sql .= " valore = ".Utility::nullizza ( $this->_headText );
			$sql .= " WHERE id = $idBlock";
		
			$this->query($sql);
		
			return $idBlock;
		
		} else{
			$sql = "INSERT INTO " . DB_PREF . "campaign_block (";
			$sql .= "idCampagna,";
			$sql .= "idTipoBlocco,";
			$sql .= "idBloccoPadre,";
			$sql .= "nomeVariabile,";
			$sql .= "idValore,";
			$sql .= "valore,";
			$sql .= "arrayIndex";
			
			$sql .= ")VALUES(";
			$sql .= Utility::nullizza ( $this->id ) . ",";
			$sql .= "5,";
			$sql .= Utility::nullizza ( $this->_idWrapperTesti ) . ",";
			$sql .= "'wrapperTestiComposti',";
			$sql .= "9,";
			$sql .= Utility::nullizza ( $this->_headText ) . ",";
			$sql .= "0";
			$sql .= ")";
			
			return $this->insert ( $sql );
		}
	}
	public function saveSubHeadText() {
		if ($this->_idWrapperTesti == 0)
			$this->_idWrapperTesti = $this->saveWrapperTesti ();
		
		$sql = "SELECT id FROM ". DB_PREF . "campaign_block WHERE idCampagna=".$this->id." AND idTipoBlocco=5 AND idBloccoPadre=".$this->_idWrapperTesti." AND nomeVariabile='wrapperTestiComposti' AND idValore=9 AND arrayIndex=1";
		$result = $this->select($sql);
	
		$idBlock = (!empty($result) && isset($result[0]['id']))? $result[0]['id'] :"";
	
		if(!empty($idBlock)){
	
			$sql = "UPDATE ". DB_PREF . "campaign_block SET";
			$sql .= " valore = ".Utility::nullizza ( $this->_subHeadText );
			$sql .= " WHERE id = $idBlock";
	
			$this->query($sql);
	
			return $idBlock;
	
		} else {
			$sql = "INSERT INTO " . DB_PREF . "campaign_block (";
			$sql .= "idCampagna,";
			$sql .= "idTipoBlocco,";
			$sql .= "idBloccoPadre,";
			$sql .= "nomeVariabile,";
			$sql .= "idValore,";
			$sql .= "valore,";
			$sql .= "arrayIndex";
			
			$sql .= ")VALUES(";
			$sql .= Utility::nullizza ( $this->id ) . ",";
			$sql .= "5,";
			$sql .= Utility::nullizza ( $this->_idWrapperTesti ) . ",";
			$sql .= "'wrapperTestiComposti',";
			$sql .= "9,";
			$sql .= Utility::nullizza ( $this->_subHeadText ) . ",";
			$sql .= "1";
			$sql .= ")";
			
			return $this->insert ( $sql );
		}
	}
	public function saveWebSiteText() {
		$sql = "SELECT id FROM ". DB_PREF . "campaign_block WHERE idCampagna=".$this->id." AND idTipoBlocco=1 AND nomeVariabile='web' AND idValore=9 AND arrayIndex=0";
		$result = $this->select($sql);
		
		$idBlock = (!empty($result) && isset($result[0]['id']))? $result[0]['id'] :"";
		
		if(!empty($idBlock)){
		
			$sql = "UPDATE ". DB_PREF . "campaign_block SET";
			$sql .= " valore = ".Utility::nullizza ( $this->_webSiteText );
			$sql .= " WHERE id = $idBlock";
		
			$this->query($sql);
		
			return $idBlock;
		
		} else {			
			$sql = "INSERT INTO " . DB_PREF . "campaign_block (";
			$sql .= "idCampagna,";
			$sql .= "idTipoBlocco,";
			$sql .= "idBloccoPadre,";
			$sql .= "nomeVariabile,";
			$sql .= "idValore,";
			$sql .= "valore,";
			$sql .= "arrayIndex";
			
			$sql .= ")VALUES(";
			$sql .= Utility::nullizza ( $this->id ) . ",";
			$sql .= "1,";
			$sql .= "NULL,";
			$sql .= "'web',";
			$sql .= "9,";
			$sql .= Utility::nullizza ( $this->_webSiteText ) . ",";
			$sql .= "0";
			$sql .= ")";
			
			return $this->insert ( $sql );
		}
	}
	public function saveWebSiteUrl() {
		$sql = "SELECT id FROM ". DB_PREF . "campaign_block WHERE idCampagna=".$this->id." AND idTipoBlocco=1 AND nomeVariabile='web' AND idValore=15 AND arrayIndex=0";
		$result = $this->select($sql);
		
		$idBlock = (!empty($result) && isset($result[0]['id']))? $result[0]['id'] :"";
		
		if(!empty($idBlock)){
		
			$sql = "UPDATE ". DB_PREF . "campaign_block SET";
			$sql .= " valore = ".Utility::nullizza ( $this->_webSiteUrl );
			$sql .= " WHERE id = $idBlock";
		
			$this->query($sql);
		
			return $idBlock;
		
		} else {
			$sql = "INSERT INTO " . DB_PREF . "campaign_block (";
			$sql .= "idCampagna,";
			$sql .= "idTipoBlocco,";
			$sql .= "idBloccoPadre,";
			$sql .= "nomeVariabile,";
			$sql .= "idValore,";
			$sql .= "valore,";
			$sql .= "arrayIndex";
			
			$sql .= ")VALUES(";
			$sql .= Utility::nullizza ( $this->id ) . ",";
			$sql .= "1,";
			$sql .= "NULL,";
			$sql .= "'web',";
			$sql .= "15,";
			$sql .= Utility::nullizza ( $this->_webSiteUrl ) . ",";
			$sql .= "0";
			$sql .= ")";
			
			return $this->insert ( $sql );
		}
	}
	public function saveCtaText() {
		$sql = "SELECT id FROM ". DB_PREF . "campaign_block WHERE idCampagna=".$this->id." AND idTipoBlocco=1 AND nomeVariabile='cta' AND idValore=9 AND arrayIndex=0";
		$result = $this->select($sql);
		
		$idBlock = (!empty($result) && isset($result[0]['id']))? $result[0]['id'] :"";
		
		if(!empty($idBlock)){
		
			$sql = "UPDATE ". DB_PREF . "campaign_block SET";
			$sql .= " valore = ".Utility::nullizza ( $this->_ctaText );
			$sql .= " WHERE id = $idBlock";
		
			$this->query($sql);
		
			return $idBlock;
		
		} else {
			$sql = "INSERT INTO " . DB_PREF . "campaign_block (";
			$sql .= "idCampagna,";
			$sql .= "idTipoBlocco,";
			$sql .= "idBloccoPadre,";
			$sql .= "nomeVariabile,";
			$sql .= "idValore,";
			$sql .= "valore,";
			$sql .= "arrayIndex";
			
			$sql .= ")VALUES(";
			$sql .= Utility::nullizza ( $this->id ) . ",";
			$sql .= "1,";
			$sql .= "NULL,";
			$sql .= "'cta',";
			$sql .= "9,";
			$sql .= Utility::nullizza ( $this->_ctaText ) . ",";
			$sql .= "0";
			$sql .= ")";
			
			return $this->insert ( $sql );
		}
	}
	public function saveCtaUrl() {
		$sql = "SELECT id FROM ". DB_PREF . "campaign_block WHERE idCampagna=".$this->id." AND idTipoBlocco=1 AND nomeVariabile='cta' AND idValore=15 AND arrayIndex=0";
		$result = $this->select($sql);
		
		$idBlock = (!empty($result) && isset($result[0]['id']))? $result[0]['id'] :"";
		
		if(!empty($idBlock)){
		
			$sql = "UPDATE ". DB_PREF . "campaign_block SET";
			$sql .= " valore = ".Utility::nullizza ( $this->_ctaUrl );
			$sql .= " WHERE id = $idBlock";
		
			$this->query($sql);
		
			return $idBlock;
		
		} else {
			$sql = "INSERT INTO " . DB_PREF . "campaign_block (";
			$sql .= "idCampagna,";
			$sql .= "idTipoBlocco,";
			$sql .= "idBloccoPadre,";
			$sql .= "nomeVariabile,";
			$sql .= "idValore,";
			$sql .= "valore,";
			$sql .= "arrayIndex";
			
			$sql .= ")VALUES(";
			$sql .= Utility::nullizza ( $this->id ) . ",";
			$sql .= "1,";
			$sql .= "NULL,";
			$sql .= "'cta',";
			$sql .= "15,";
			$sql .= Utility::nullizza ( $this->_ctaUrl ) . ",";
			$sql .= "0";
			$sql .= ")";
			
			return $this->insert ( $sql );
		}
	}
	/* save */
	public function save() {
		$idAlfanumeric = Utility::nullizza ( $this->_idAlfanumeric );
		$idTemplate = !empty($this->_idTemplate) && $this->_idTemplate > 0? $this->_idTemplate: 1; // Utility::nullizza($this->_idTemplate);
		
		$type = Utility::nullizza ( $this->_type );
		$nome = Utility::nullizza ( $this->_name );
		$preview = Utility::nullizza ( $this->_preview );
		$variableCache = Utility::nullizza ( $this->_variableCache );
		
		if (! empty ( $this->id )) {
			$sql = "UPDATE " . $this->getTable () . " SET";
			$sql .= " idAlfanumeric = $idAlfanumeric";
			$sql .= ",idTemplate = $idTemplate";
			$sql .= ",type = $type";
			$sql .= ",nome = $nome";
			$sql .= ",preview = $preview";
			$sql .= ",variableCache = $variableCache";
			$sql .= " WHERE id=" . $this->id;
			
			$this->query ( $sql );
		} else {
			$sql = "INSERT INTO " . $this->getTable () . " (";
			$sql .= "idAlfanumeric,";
			$sql .= "idTemplate,";
			$sql .= "type,";
			$sql .= "nome,";
			$sql .= "preview,";
			$sql .= "variableCache,";
			$sql .= "dataCreazione";
			$sql .= ") VALUES (";
			$sql .= "'" . uniqid () . "',";
			$sql .= "$idTemplate,";
			$sql .= "$type,";
			$sql .= "$nome,";
			$sql .= "$preview,";
			$sql .= "$variableCache,";
			$sql .= "now()";
			$sql .= ")";
			
			$idCampagna = $this->insert ( $sql );
			$this->id = $idCampagna;
		}
		
		if (! empty ( $this->_bgUrl ))
			$this->saveBg ();
		if (! empty ( $this->_bgUrl ))
			$this->saveLogo ();
		if (! empty ( $this->_colorBg ))
			$this->saveColorBg ();
		if (! empty ( $this->_headText ))
			$this->saveHeadText ();
		if (! empty ( $this->_subHeadText ))
			$this->saveSubHeadText ();
		if (! empty ( $this->_ctaText ))
			$this->saveCtaText ();
		if (! empty ( $this->_ctaUrl ))
			$this->saveCtaUrl ();
		if (! empty ( $this->_webSiteText ))
			$this->saveWebSiteText ();
		if (! empty ( $this->_webSiteUrl ))
			$this->saveWebSiteUrl ();
		if (! empty($this->_coordMainAreaBanner))
			$this->saveCoordMainAreaBanner();
		
		$this->savePreview ();
		
		return $this->id;
	}
	public function delete() {
		$sql = "DELETE FROM " . DB_PREF . "campaign_block WHERE idCampagna='" . $this->id . "'";
		$this->query ( $sql );
		
		$sql = "DELETE FROM " . DB_PREF . "banner WHERE idCamp='" . $this->id . "'";
		$this->query ( $sql );
		
		$sql = "DELETE FROM " . $this->getTable () . " WHERE id='" . $this->id . "'";
		$this->query ( $sql );
	}
	/* VARIABLES - GET/SET - FINE */
	
	/* INIT */
	public function __construct($db_conn = null, $ident = null) {
		parent::__construct ( $db_conn, $ident );
		$this->columnList = array (
				"id" => "setId",
				"idAlfanumeric" => "setIdentAlfanumeric",
				"idTemplate" => "setTemplate",
				"type" => "setType",
				"nome" => "setName",
				"preview" => "setPreview",
				"variableCache" => "setVariabileCache",
				"dataCreazione" => "setDataCreazione" 
		);
	}
	public function init($id = "") {
		
		// `id`, `idAlfanumeric`, `idCamp`, `idFormato`, `isCustom`, `width`, `height`, `name`, `userIns`, `userMod`, `imageUrl`, `imageThumbUrl`, `data_ins`, `data_mod`, `variableCaches`
		if (isset ( $id ) && ! empty ( $id )) {
			$result = $this->select ( "SELECT * FROM " . $this->getTable () . " WHERE id = \"" . $id . "\"" );
			if (count ( $result ) > 0) {
				$this->initByRecord ( $result [0] );
				$this->_bannerList = new BannerList ();
				$this->_bannerList->initbyIdCamp ( $result [0] ['id'] );
				return $this;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	public function initByAlpha($alphaId = "") {
		
		// `id`, `idAlfanumeric`, `idCamp`, `idFormato`, `isCustom`, `width`, `height`, `name`, `userIns`, `userMod`, `imageUrl`, `imageThumbUrl`, `data_ins`, `data_mod`, `variableCaches`
		if (isset ( $alphaId ) && ! empty ( $alphaId )) {
			$result = $this->select ( "SELECT * FROM " . $this->getTable () . " WHERE idAlfanumeric LIKE \"" . $alphaId . "\"" );
			if (count ( $result ) > 0) {
				$this->initByRecord ( $result [0] );
				$this->_bannerList = new BannerList ();
				$this->_bannerList->initbyIdCamp ( $result [0] ['id'] );
				return $this;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	public function initByRecord($record, $aliasTable = null) {
		$this->setStatus ( DatabaseTable::LOADED_RECORD );
		
		$this->_bannerList = new BannerList ();
		$this->_bannerList->initbyIdCamp ( $record ['id'] );
		
		$prefixTable = "";
		if (! is_null ( $aliasTable )) {
			$prefixTable = $aliasTable . ".";
		}
		
		// per ogni colonna dichiarata
		$columns = $this->getColumsList ();
		for($i = 0; $i < count ( $columns ); $i ++) {
			$name = $prefixTable . $columns [$i];
			if (array_key_exists ( $name, $record ) && strlen ( $this->columnList [$columns [$i]] ) > 0) {
				// se ho una funzione di settaggio definita la uso
				$this->{$this->columnList [$columns [$i]]} ( $record [$name] );
			} else {
				// se NON ho una funzione di settaggio definita creo una variabile semplice
				$this->{$columns [$i]} = $record [$name];
			}
			// tolgo i record già settati
			unset ( $record [$name] );
		}
		
		// setto come semplici variabili gli altri record rimasti
		foreach ( $record as $cname => $cvalue ) {
			$this->{$cname} = $cvalue;
		}
	}
	
	/* INIT - FINE */
	public function getArrayBanner() {
		return $this->_bannerList->toArray ();
	}
	/* FUNCTIONS */
	public function getTemplateObj() {
		if ($this->_templateObj === null) {
			$this->_templateObj = new Template ( $this->get_db_connection () );
			if (! $this->_templateObj->init ( $this->_idTemplate )) {
				$this->_templateObj = null;
			}
		}
		return $this->_templateObj;
	}
	public function getVariables($format = "json", $cached = true) {
		$structureVariable = $this->getTemplateObj ()->getVariableConfiguration ();
		$customVariable = $this->select ( "
		SELECT `id`, `idCampagna`, `idTipoBlocco`, `idBloccoPadre`, `nomeVariabile`, `idValore`, `valore`, `arrayIndex` FROM `" . DB_PREF . "campaign_block`
		WHERE idCampagna = " . $this->id . "
		ORDER BY idTipoBlocco ASC,nomeVariabile ASC,arrayIndex ASC" );
		
		for($i = 0; $i < count ( $customVariable ); $i ++) {
			$name = $customVariable [$i] ["nomeVariabile"];
			$idVariabile = $customVariable [$i] ["idValore"];
			$valore = $customVariable [$i] ["valore"];
			$ordine = $customVariable [$i] ["arrayIndex"];
			$recordPadre = isset ( $customVariable [$i] ["idBloccoPadre"] ) && ! is_null ( $customVariable [$i] ["idBloccoPadre"] ) ? $this->findRecordPadre ( $customVariable, $customVariable [$i] ["idBloccoPadre"] ) : false;
			if (array_key_exists ( $name, $structureVariable )) {
				// esiste andiamo a vedere i parametri
				$structureVariable [$name]->setVariableValue ( $idVariabile, $valore, $ordine, $recordPadre ["idValore"] );
			} else {
				// non esiste nella struttura quindi la ignoro
			}
		}
		$return = array ();
		foreach ( $structureVariable as $name => $obj ) {
			$return [$name] = $obj->getStructure ();
		}
		return $return;
	}
	private function findRecordPadre($recordSet, $idRecord) {
		foreach ( $recordSet as $record ) {
			if ($record ["id"] == $idRecord) {
				return $record;
			}
		}
		return false;
	}
	
	/* CLASSI STATICHE */
	/* CLASSI STATICHE FINE */
}
class CampagnaList extends CustomList {
	const TABLE = "campaign";
	public function __construct($db_conn = null, $ident = null) {
		parent::__construct ( $db_conn, $ident );
	}
	public function init() {
		$this->clear ();
		$sql = "SELECT * FROM " . $this->getTable ();
		$result = $this->select ( $sql, false );
		foreach ( $result as $record ) {
			$tmpItem = new Campagna ( $this->get_db_connection () );
			$tmpItem->initByRecord ( $record );
			$this->add ( $tmpItem );
		}
	}
}
