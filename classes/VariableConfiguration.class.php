<?php

require_once ROOT_DIR."classes/Base.class.php";
require_once ROOT_DIR."classes/CustomList.class.php";
require_once ROOT_DIR."classes/Fonts.class.php";

class VariableConfigurationSingleParam extends Base{
    const TABLE = "blockType_value";
	
	/*VARIABLES*/
	private $_idBlockType = 0;
	private $_type = "";
	private $_nomeVariabile = 0;
	private $_mandatory = 0;
	private $_defaultValue = "";
	private $_actualValue = "";
	private $_isSet = false;
	
	/*VARIABLES - FINE*/
	
	/*VARIABLES - GET/SET*/
	public function setId($v){
		$this->id = $v;
		return $this;
	}
	
	public function setIdTipoBlocco($v){
		$this->_idTipoBlocco = $v;
		return $this;
	}
	
	public function setType($v){
		$this->_type = $v;
		return $this;
	}
	public function getType(){
		return $this->_type;
	}
	
	public function setNomeVariabile($v){
		$this->_nomeVariabile = $v;
		return $this;
	}
	
	public function getNomeVariabile(){
		return $this->_nomeVariabile;
	}
	
	public function setMandatory($v){
		$this->_mandatory = $v;
		return $this;
	}
	
	public function setDefaultValue($v){
		$this->_defaultValue = $v;
		return $this;
	}
	
	public function setValue($value,$type = null){
		$this->_isSet = true;
		$this->_actualValue = $this->getFormattedVariable($value,$type);
	}
	
	public function addBlock(){
		if($this->_idTipoBlocco == BlocksType::MULTIPLETEXT){
			$t = new VariableConfiguration($this->get_db_connection());
			if($t->initByIdTipoBlocco(BlocksType::TEXT) && $t->count()>=1){
				array_push($this->_actualValue,$t);
			}
		}
	}
	
	private function getFormattedVariable($value,$type){
		$type = isset($type) ? $type : $this->_type;
		$returnValue = $value;
		list($type,$subtype,$subtypesubtype) = array_pad(explode(" ",$type), 3, '');
		switch($type){
			case "int":
				$returnValue = intval($value);
				break;
			case "array":
				//tolgo il primo e l'ultimo carattere definito da [ ]
				$returnValue = array();
				if($subtype == "multipletext"){
					$t = new VariableConfiguration($this->get_db_connection());
					if($t->initByIdTipoBlocco(BlocksType::TEXT) && $t->count()>=1){
						array_push($returnValue,$t);
					}
					
				}else{
					
					if(!is_array($value)){
						//se non mi è stato passato un array allora è una stringa di tipo json
						$value = json_decode($value);
					}
					foreach($value as $newValue){
						array_push($returnValue,$this->getFormattedVariable($newValue,"$subtype $subtypesubtype"));
					}
				}
				
				break;
			case "character":
				$value = strtolower(trim($value));
				if(strlen($value)>1){
					$returnValue = substr ( $value , 0 , 1 );
				}
				break;
			case "font":
				
				$value = intval($value);
				$font = new Fonts($this->get_db_connection());
				if($font->init($value)){
					$returnValue = $font->getUrl();
				}
				
				break;
			case "range":
				//tolgo il primo e l'ultimo carattere definito da [ ]
				$returnValue = array();
				if(!is_array($value)){
					//se non mi è stato passato un array allora è una stringa di tipo json
					$value = json_decode($value);
				}
				for($i = 0; $i<2; $i++){
					array_push($returnValue,$this->getFormattedVariable($value[$i],"$subtype $subtypesubtype"));
				}
				break;
		}
		return $returnValue;
	}
	
	public function getValue(){
		if($this->_type == "imagePath"){
			$imageObj = array();
			$imageObj["mime"] = false;
			$imageObj["http"] = false;
			$imageObj["url"] = false;
			$mime = false;
			$ext = substr($this->_actualValue,strrpos($this->_actualValue,".")+1);
			if($ext == "jpg" || $ext == "jpeg"){
				$mime = "jpeg";
			}elseif($ext == "png"){
				$mime = "png";
			}elseif($ext == "svg"){
				$mime = "svg+xml";
			}
			
			if($mime !== false && file_exists(ASSET_DIR.$this->_actualValue)){
				$imageObj["mime"] = $mime;
				$imageObj["url"] = base64_encode(file_get_contents(ASSET_DIR.$this->_actualValue));
				$imageObj["http"] = ASSET_PATH . $this->_actualValue;
			}
			
			return $imageObj;
		}else{
			return $this->_actualValue;
		}
		
	}
	
	/*VARIABLES - GET/SET - FINE*/
	
	/*INIT*/
	public function __construct($db_conn = null, $ident = null){
		parent::__construct($db_conn, $ident);
		$this->columnList = array(
								  "id"=>"setId",
								  "idBlockType"=>"setIdTipoBlocco",
								  "type"=>"setType",
								  "name"=>"setNomeVariabile",
								  "mandatory"=>"setMandatory",
								  "defaultValue"=>"setDefaultValue"
								  );
		
	}
	
	public function initByRecord($record,$aliasTable = null){
		parent::initByRecord($record,$aliasTable);
		$this->setValue($this->_defaultValue);
		
	}
	
	/*INIT - FINE*/
	
	/* FUNCTIONS */
	
	
    /*save*/
	public function save(){
        throw new Exception("generic ");
	}
	
	
	/*CLASSI STATICHE*/
	/*CLASSI STATICHE FINE*/
}

class VariableConfiguration extends CustomList{
	
	private $_idTipoBlocco = 0;
	
	/*VARIABLES*/
	/*VARIABLES FINE*/
	
	/*INIT*/
	public function init(){
		$this->clear();
	}
	
	public function initByIdTipoBlocco($idTipoBlocco){
		$this->_idTipoBlocco = $idTipoBlocco;
		
		$sql = "SELECT " . VariableConfigurationSingleParam::getSelectFieldAll("v") . "
		FROM " . DB_PREF . VariableConfigurationSingleParam::TABLE  . " AS v
		WHERE idBlockType = " . $this->format($this->_idTipoBlocco);
		
		$result = $this->select($sql,false);
		foreach ($result as $record){
			$tmpItem = new VariableConfigurationSingleParam($this->get_db_connection());
			$tmpItem->initByRecord($record,"v");
			$this->add($tmpItem);
		}
		return $this;
	}
	
	/*INIT FINE*/
	
	public function setVariableValue($name,$value,$arrIndex = 0, $nomeCampoPadre = false){
		if($this->_idTipoBlocco == BlocksType::MULTIPLETEXT && $nomeCampoPadre){
			//tipo di blocco multipletext
			
			$tm = $this->getElementForKey($nomeCampoPadre);
			if($tm){
				//ho abbastanza elementi nel mio caso?
				//aggiungo stringhe se me ne mancano
				for($i = count($tm->getValue()); $i <= $arrIndex; $i++){
					$tm->addBlock();
				}
				$arr = $tm->getValue();
				$arr[$arrIndex] -> setVariableValue($name,$value);
				
			}else{
				
			}
			
		}else{
			$tm = $this->getElementForKey($name);
			if($tm && $tm->getType() !== "array multipletext"){
				$tm->setValue($value);
			}else{
				
			}
		}
		
	}
	
	public function getStructure(){
		$jsonObj = array();
		if($this->count()>0){
			foreach($this->toArray() as $name => $obj){
				$value = $obj->getValue();
				if(is_array($value)){
					$jsonObj[$obj->getNomeVariabile()] = array();
					foreach($value as $index => $subvalue){
						if($subvalue instanceof VariableConfiguration){
							$jsonObj[$obj->getNomeVariabile()][$index] = $subvalue->getStructure();
						}else{
							$jsonObj[$obj->getNomeVariabile()][$index] = $subvalue;
						}
					}
				}else{
					$jsonObj[$obj->getNomeVariabile()] = $value;
				}
				
			}
		}
		return $jsonObj;
	}
	
	public function getJsonStructure(){
		
		return json_encode($this->getStructure());
	}
}
