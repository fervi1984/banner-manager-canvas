<?php

require_once ROOT_DIR."classes/Base.class.php";
require_once ROOT_DIR."classes/CustomList.class.php";

class Fonts extends Base{
    const TABLE = "fonts";
	
	/*VARIABLES*/
	private $_fontFamily = 0;
	private $_type = "";
	private $_min = 0;
	private $_max = 0;
	private $_path = "";
	
	/*VARIABLES - FINE*/
	
	/*VARIABLES - GET/SET*/
	public function setId($v){
		$this->id = $v;
		return $this;
	}
	
	public function setFontFamily($v){
		$this->_fontFamily = $v;
		return $this;
	}
	
	public function setType($v){
		$this->_type = $v;
		return $this;
	}
	
	public function setMin($v){
		$this->_min = $v;
		return $this;
	}
	
	public function setMax($v){
		$this->_max = $v;
		return $this;
	}
	
	public function setPath($v){
		$this->_path = $v;
		return $this;
	}
	
	public function getPath(){
		return ROOT_DIR . "fonts/". $this->_path;
	}
	
	
	public function getUrl(){
		return ROOT_PATH. "fonts/". $this->_path;
	}
	/*VARIABLES - GET/SET - FINE*/
	
	/*INIT*/
	public function __construct($db_conn = null, $ident = null){
		parent::__construct($db_conn, $ident);
		$this->columnList = array(
								  "id"=>"setId",
								  "fontFamily"=>"setFontFamily",
								  "type"=>"setType",
								  "min"=>"setMin",
								  "max"=>"setMax",
								  "path"=>"setPath"
								  );
	}
	
	
	/*INIT - FINE*/
	
	/* FUNCTIONS */
	
	
    /*save*/
	public function save(){
        throw new Exception("generic ");
	}
	
	
	/*CLASSI STATICHE*/
	/*CLASSI STATICHE FINE*/
}
