<?php

require_once ROOT_DIR."classes/Base.class.php";
require_once ROOT_DIR."classes/CustomList.class.php";

class Block extends Base{
    const TABLE = "block";
	
	/*VARIABLES*/
	private $_idBlockType = 0;
	private $_idTemplate = 0;
	private $_idBlockParent = 0;
	private $_name = "";
	private $_orderValue = 0;
	private $_priority = 0;
	private $_origin = 1;
	private $_vSize = 0;
	private $_vWeight = 1;
	private $_hSize = 0;
	private $_hWeight = 1;
	private $_background = null;
	
		
	/*VARIABLES - FINE*/
	
	/*VARIABLES - GET/SET*/
	public function setId($v){
		$this->id = $v;
	}
	
	public function setBlockType($v){
		$this->_idBlockType = $v;
		return $this;
	}
	
	public function getBlockType(){
		return $this->_idBlockType;
	}
	
	/////////////// TODO //////////////////
	/////////////// TEMPORANEO //////////////////
	public function getTypeString(){
		$value = "image";
		switch($this->_idBlockType){
			case 1:
				$value = "txt";
				break;
			case 2:
				$value = "image";
				break;
			case 3:
				$value = "imageLogo";
				break;
			case 5:
				$value = "multipleTxt";
				break;
			case 6:
				$value = "multipleImage";
				break;
		}
		
		return $value;
	}
	/////////////// TEMPORANEO FINE //////////////////
	
	public function setTemplate($v){
		$this->_idTemplate = $v;
		return $this;
	}
	
	public function getTemplate(){
		return $this->_idTemplate;
	}
	
	public function setBlockParent($v){
		$this->_idBlockParent = $v;
		return $this;
	}
	
	public function getBlockParent(){
		return $this->_idBlockParent;
	}
	
	public function setName($v){
		$this->_name = $v;
		return $this;
	}
	
	public function getName(){
		return $this->_name;
	}
	
	public function setOrderValue($v){
		$this->_orderValue = $v;
		return $this;
	}
	
	public function getOrderValue(){
		return $this->_orderValue;
	}
	
	public function setPriority($v){
		$this->_priority = $v;
		return $this;
	}
	
	public function getPriority(){
		return $this->_priority;
	}
	
	public function setOrigin($v){
		$this->_origin = $v;
		return $this;
	}
	
	public function getOrigin(){
		return $this->_origin;
	}
	
	public function setVSize($v){
		$this->_vSize = $v;
		return $this;
	}
	
	public function getVSize(){
		return $this->_vSize;
	}
	
	public function setVWeight($v){
		$this->_vWeigh = $v;
		return $this;
	}
	
	public function getVWeight(){
		return $this->_vWeigh;
	}
	
	public function setHSize($v){
		$this->_hSize = $v;
		return $this;
	}
	
	public function getHSize(){
		return $this->_hSize;
	}
	
	public function setHWeight($v){
		$this->_hWeight = $v;
		return $this;
	}
	
	public function getHWeight(){
		return $this->_hWeight;
	}
	
	public function setBackground($v){
		$this->_background = $v;
		return $this;
	}
	
	public function getBackground(){
		return $this->_background;
	}
	
	/*VARIABLES - GET/SET - FINE*/
	
	
	/*INIT*/
	public function __construct($db_conn = null, $ident = null){
		parent::__construct($db_conn, $ident);
		$this->columnList = array(
								  "id"=>"setId",
								  "idBlockType"=>"setBlockType",
								  "idTemplate"=>"setTemplate",
								  "idBlockParent"=>"setBlockParent",
								  "name"=>"setName",
								  "orderValue"=>"setOrderValue",
								  "priority"=>"setPriority",
								  "origin"=>"setOrigin",
								  "vSize"=>"setVSize",
								  "vWeight"=>"setVWeight",
								  "hSize"=>"setHSize",
								  "hWeight"=>"setHWeight",
								  "bk" => "setBackground"
								  );
		
	}
	
	/*INIT - FINE*/
	
	/* FUNCTIONS */
	
	
    /*save*/
	public function save(){
        throw new Exception("generic ");
	}
	
	
	/*CLASSI STATICHE*/
	/*CLASSI STATICHE FINE*/
}

class BlockList extends CustomList{
	const NUM_OF_RECORD = 9999;
	
	private $_idTemplate = 0;
	private $_jsonMaster = array();
	private $_blockInsert = array();
	
	/*VARIABLES*/
	/*VARIABLES FINE*/
	
	/*INIT*/
	public function init(){
		$this->clear();
	}
	
	public function initByIdTemplate($idTemplate){
		if($idTemplate <> $this->_idTemplate){
			
			$this->clear();
			$this->_idTemplate = $idTemplate;
			$sql = "SELECT " . Block::getSelectFieldAll("b") . "
			FROM " . DB_PREF . Block::TABLE  . " AS b
			WHERE idTemplate = " . $this->format($this->_idTemplate) . "
			ORDER BY orderValue asc,id asc";
			
			$result = $this->select($sql,false);
			
			foreach ($result as $record){
				$tmpItem = new Block($this->get_db_connection());
				$tmpItem->initByRecord($record,"b");
				$this->add($tmpItem);
			}
			
		}
		
		return $this;
	}
	
	/*INIT FINE*/
	
	public function getStructureJson($idTemplate){
		
		$this->initByIdTemplate($idTemplate);
		
		if($this->count()>0){
			$this->addToJsonStructureWithParent(0,$this->_jsonMaster);
		}
		
		return json_encode($this->_jsonMaster, JSON_NUMERIC_CHECK);
	}
	
	public function getDefaultVariables($idTemplate){
		$elements = array();
		$this->initByIdTemplate($idTemplate);
		foreach($this->toArray() as $block){
			//il generic non contiene inforazioni
			if($block->getBlockType() != 4){
				$conf = new VariableConfiguration($this->get_db_connection());
				$conf -> initByIdTipoBlocco($block->getBlockType());
				$elements[$block->getName()] = $conf;
			}
		}
		return $elements;
	}
	
	private function addToJsonStructureWithParent($idParent,&$value){
		foreach($this->toArray() as $data){
			if($data -> getBlockParent() == $idParent){
				if($idParent == 0){
					$newValue = &$value;
				}else{
					if(!array_key_exists("blocks",$value)){
						$value["blocks"] = array();
					}
					$newValue = &$value["blocks"][count($value["blocks"])];
				}
				
				if($data->getBlockType() != 4){
					$newValue["type"] = $data->getTypeString();
				}
				
				if($data->getBackground() !== null){
					$newValue["bk"] = $data->getBackground();
				}
				
				$newValue["id"] = $data->getId();
				$newValue["name"] = $data->getName();
				$newValue["origin"] = $data->getOrigin();
				$newValue["priority"] = $data->getPriority();
				$newValue["v-size"] = $data->getVSize();
				$newValue["v-weight"] = $data->getVWeight();
				$newValue["h-size"] = $data->getHSize();
				$newValue["h-weight"] = $data->getHWeight();
				
				$this->addToJsonStructureWithParent($data->getId(),$newValue);
			}
		}
	}
	
}

class BlocksType {
	const TEXT = 1;
	const IMAGE = 2;
	const LOGO = 3;
	const GENERAL = 4;
	const MULTIPLETEXT = 5;
}
