<?php

require_once ROOT_DIR."classes/Base.class.php";
require_once ROOT_DIR."classes/CustomList.class.php";

class Variable extends Base{
    const TABLE = "campaign_block";
	
	/*VARIABLES*/
	private $_idCampagna = 0;
	private $_idTipoBlocco = 0;
	private $_nomeVariabile = "";
	private $_idValore = 0;
	private $_valore = "";
	private $_ordine = 0;
	
	/*VARIABLES - FINE*/
	
	/*VARIABLES - GET/SET*/
	private function setId($v){
		$this->id = $v;
		return $this;
	}
	
	public function setIdCampagna($v){
		$this->_idCampagna = $v;
		return $this;
	}
	
	public function setIdTipoBlocco($v){
		$this->_idTipoBlocco = $v;
		return $this;
	}
	
	public function setNomeVariabile($v){
		$this->_nomeVariabile = $v;
		return $this;
	}
	
	public function setIdValore($v){
		$this->_idValore = $v;
		return $this;
	}
	
	public function setValore($v){
		$this->_valore = $v;
		return $this;
	}
	
	public function setOrdine($v){
		$this->_ordine = $v;
		return $this;
	}
	
	/*VARIABLES - GET/SET - FINE*/
	
	/*INIT*/
	public function __construct($db_conn = null, $ident = null){
		parent::__construct($db_conn, $ident);
		$this->columnList = array(
								  "id"=>"setId",
								  "idCampagna"=>"setIdCampagna",
								  "idTipoBlocco"=>"setIdTipoBlocco",
								  "nomeVariabile"=>"setNomeVariabile",
								  "idValore"=>"setIdValore",
								  "valore"=>"setValore",
								  "ordine"=>"setOrdine"
								  );
		
	}
	
	/*INIT - FINE*/
	
	/* FUNCTIONS */
	
	
    /*save*/
	public function save(){
        throw new Exception("generic ");
	}
	
	
	/*CLASSI STATICHE*/
	/*CLASSI STATICHE FINE*/
}
