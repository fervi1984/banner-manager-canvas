<?php
/*
* @author: Armando Iori
* @e-mail: armando.iori@sintetik.it
* @description: Database table abstract class
*/

abstract class DatabaseTable{
	private $db_connection;
	private $table;
	
	private $ins_date;	//Nullable
	private $mod_date;	//Nullable
	private $ins_user;	//Oggetto utente (User) Nullable
	private $mod_user;	//Oggetto utente (User) Nullable
	
	private $status;
	private $debugMode;
	
	private $isAutoincrementKey;
	
	const NEW_RECORD = "NEW";
	const LOADED_RECORD = "LOADED";
	
	public function __construct(Database $db_conn){
		$this->debugMode = false;
		if($this->_checkVar($db_conn)){
			$this->set_db_connection($db_conn);
		}
		$this->isAutoincrementKey = true;
		
	}

	public function setStatus($status){
		$this->status = $status;
	}
	public function isLoaded(){
		return $this->getStatus() == DatabaseTable::LOADED_RECORD;
	}
	public function isNew(){
		return $this->getStatus() == DatabaseTable::NEW_RECORD;
	}
	public function getStatus(){
		return $this->status;
	}
	
	public function setAutoincrementKey($autoincrement){
		$this->isAutoincrementKey = $autoincrement;
	}
	
	public function isAutoincrementKey(){
		return $this->isAutoincrementKey == true;
	}
	
	public function setTable($table){
		$this->table = $table;
	}
	
	public function getTable(){
		return $this->table;
	}
	
	public function select($sql, $debug = false){
		return $this->db_connection->select($sql, $debug);
	}
	
	public function query($sql, $debug = false){
		return $this->db_connection->query($sql, $debug);
	}
	
	public function insert($sql, $debug = false){
		return $this->db_connection->insert($sql, $debug);
	}
	
	public function cleanString($str){
		return $this->db_connection->cleanString($str);
	}
	
	public function format($str){
		return $this->db_connection->format($str);
	}
	
	public function set_db_connection(Database $db_conn){
		if($this->_checkVar($db_conn)){
			$this->db_connection = $db_conn;
		}
	}
	
	public function get_db_connection(){
		return $this->db_connection;
	}
	
	private function _checkVar($var){
		$check = false;
		if(isset($var) && !empty($var)){
			$check = true;
		}
		return $check;
	}
	
	public function setDebugMode($flag){
		$this->debugMode = $flag;
	}
	
	public function getDebugMode(){
		return $this->debugMode;
	}
	
	public function setInsDate($insDate){
		$this->ins_date = $insDate;
	}
	public function setModDate($modDate){
		$this->mod_date = $modDate;
	}
	public function getInsDate(){
		return $this->ins_date;
	}
	public function getModDate(){
		return $this->mod_date;
	}
	//Recuperare da database?
	public function setInsUser(User $u){
		$this->ins_user = $u;
	}
	//Recuperare da database?
	public function setModUser(User $u){
		$this->mod_user = $u;
	}
	public function getInsUser(){
		return $this->ins_user;
	}
	public function getModUser(){
		return $this->mod_user;
	}
	public function getNextAI($fieldName){
		$sql = "SELECT MAX(" . $fieldName . ") FROM " . $this->getTable();
		$result = $this->select($sql);
		return ($result[0][0] + 1);
	}
	
	public function delete(){
		$result = false;
		if($this->getStatus() == DatabaseTable::LOADED_RECORD){
			$sql = "DELETE FROM " . $this->getTable() . " WHERE ID = " . $this->getId();
			$result = $this->query($sql);
		}
		return $result;
	}
	
	abstract function getId();
	
}

/*
 * Encapsulated in DatabaseTable extensions
 * Not used
 */

class DatabaseTableField{
	private $name;
	private $type;
	
	public function __construct($name = "", $type = ""){
		$this->name = $name;
		$this->type = $type;
	}
}

?>