<?php
// Content type
header('Content-type: image/jpeg');

$cache = true;
if(isset($_GET['cache']) && $_GET['cache']=='-1')
	$cache=false;

$cachedir = "cache/";
$log = false;

if($cache and !file_exists("cache")) {
	mkdir($cachedir, 0777);
}

$src=$_GET['src'];
$typ=$_GET['typ'];
$sizstring = $_GET['siz'];
$siz=explode(",",$sizstring);
$parsstring="";
if(isset($_GET['pars'])) {
	$parsstring = $_GET['pars'];
	$pars=explode(",",$parsstring);
}

if(isset($_GET['qua'])) $qua=$_GET['qua'];
else $qua=90;

$cachenamefile = md5(implode(",",$_GET)).".jpg";
#$cachenamefile = md5( $src.$typ.$sizstring.$parsstring).".jpg";

if($cache && file_exists($cachedir.$cachenamefile)) {
	readfile($cachedir.$cachenamefile);
}
else {
	include("imageresample_core.php");
}
?>
