<?php
function getHost($Address) { 
   $parseUrl = parse_url(trim($Address)); 
   return @trim($parseUrl['host'] ? $parseUrl['host'] : array_shift(explode('/', $parseUrl['path'], 2))); 
} 

/*function get_data($url) {
	$ch = curl_init();
	$timeout = 5;
	$useragent = 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:10.0.2) Gecko/20100101 Firefox/10.0.2';

	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_AUTOREFERER, true);
	curl_setopt($ch, CURLOPT_REFERER, 'http://www.google.com');
	curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_FAILONERROR, true);
	curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);

	$data = curl_exec($ch);
	#$data = @iconv(mb_detect_encoding($data, mb_detect_order(), true), "UTF-8", $data);
	curl_close($ch);
	return $data;
}*/

?>
<div class="container fancy-content-default">
	<div id="bannerListSaved">
		<ul class="row">
		<?php
			$i=0;
			foreach($bannerList as $k=>$v){
				$i++;
				#$image = get_data("https://restpack.io/api/screenshot/v2/capture?access_token=lVfmFjAXP2wSrlAkqB79jiLDuYVSZ8bo4PS9clPhhSVcsQ51&url=http://lab.clxeurope.com/bannermanager/preview/".$v->getId()."&thumbnail_width=270&thumbnail_height=270");
				#file_put_contents(ROOT_DIR.'tmp/'.$v->getId().'.png', $image);
				
				echo '<li class="col col-3" data-key="'.$k.'" data-id="'.$v->getId().'">
						<div class="inner-wrapper" style=\'background: url("'.$v->getThumbUrl().'");\'>
							<button type="button" class="close delete-item" aria-label="Close">
							  <span aria-hidden="true">&times;</span>
							</button>
							<div class="tassello"><a href="javascript:void(0)">'.getHost($v->getUrl()).'</a></div>
						</div>
					 </li>';
				
				if($i%4==0)
					echo '</ul><ul class="row">';
			}
		?>
		</ul>
	</div>
</div>
<script style="display: none" type="text/javascript">	
	$("#bannerListSaved li").off().on('click',function(){
		idPreview = $(this).attr('data-id');
		$("#previewId").val($(this).attr('data-id'));
		var urlAjax =  ROOT_PATH+'ajax/actionMenuPrev.php?type=get-saved-preview';
		$.ajax({url: urlAjax, type:"POST", data: {idPreview: idPreview },
			dataType: "json",
			success: function(contentData){
				$("#preview iframe").attr("src",'about:blank');
				iframe = $("#preview iframe")[0];
				iframe = iframe.contentWindow || ( iframe.contentDocument.document || iframe.contentDocument);

				iframe.document.open();
				iframe.document.write(contentData['html']);
				iframe.document.close();
				$("#urlPage").val(contentData['url']);
				$(".fancybox-close-small").click();
			}
		});
		
	});	

	$("#bannerListSaved li .delete-item").off().on('click', function(e){
		e.preventDefault();
		if (window.confirm("Do you really want to delete this item?")) { 
			colItem = $(this).closest(".col");
			urlAjax = ROOT_PATH+'ajax/actionMenuPrev.php';
			dataType = colItem.attr('data-type');
			dataId = colItem.attr('data-id');
			dataForm = { 
					idPreview: dataId,
					type: 'delete-preview',
			}
			
			$.ajax({url: urlAjax, type:"POST", data: dataForm,
				success: function(){
					colItem.remove();
				}
			});
		}
		
		return false;
		
	});
</script>