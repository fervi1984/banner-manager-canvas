<?php
$initWidth = 720;
$initHeight = 300;
define("COL_CONTENT_WIDTH",250);

?>

<script>
	var COL_CONTENT_WIDTH = <?php echo COL_CONTENT_WIDTH; ?>;
	bannerLink = "<?php echo ROOT_PATH . $objBanner->getId() . "/"; ?>";
	$(function(){
		updateTemplateSize(<?php echo $initWidth; ?>,<?php echo $initHeight; ?>);
		$(".editor-single .formato:first").click();
	});
</script>
<style>
	/*#editor .editor-single{
		height: calc( 100% - 100px);
	}*/
</style>
<div id="edit-campaign-page">
	<input type="hidden" name="bannerId" id="bannerId" value="<?php echo $objBanner->getId(); ?>" />
	<input type="hidden" name="bannerWidth" id="bannerWidth" value="<?php echo $initWidth; ?>" />
	<input type="hidden" name="bannerHeight" id="bannerHeight" value="<?php echo $initHeight; ?>" />
	<div id="preview" class="rightPos">
		<iframe id="iframe" src="<?php echo ROOT_PATH ."camp/".$objCampagna->getIdentAlfanumeric(); ?>"	width="<?php echo $initWidth; ?>" height="<?php echo $initHeight; ?>"></iframe>
		<div class="bottom-action-container">
			<div id="info-campaign" class="btn btn-secondary">INFO CAMPAIGN</div>
			<div id="edit-banner" class="btn btn-secondary">EDIT BANNER</div>
			<div id="save-banner" class="btn btn-success">SAVE BANNER</div>
			<div id="delete-banner" class="btn btn-secondary">DELETE BANNER</div>
		</div>
	</div>
	<div id="formats" class="leftPos">
		<div class="editor-single active" data-panel="resize">
			<div class="content">
				<?php
				//$lstFormati = new FormatoList($connection);
				//$lstFormati->init();
				foreach($arrayBanner as $objBanner){
					$objFormato = $objBanner->getObjFormat();
					if(!empty($objFormato)){ ?>
						<div class="formato" data-id="<?php echo $objFormato->getId(); ?>" data-width="<?php echo $objFormato->getWidth(); ?>" data-height="<?php echo $objFormato->getHeight(); ?>">
							<div class="demo" style="width:<?php echo ($objFormato->getWidth() / 1500 * (COL_CONTENT_WIDTH - 20)); ?>px; height:<?php echo ($objFormato->getHeight() / 1500 * (COL_CONTENT_WIDTH - 20)); ?>px"></div>
							<?php echo $objFormato->getNome(); ?><br>
							<small><?php echo $objFormato->getWidth(); ?>x<?php echo $objFormato->getHeight(); ?></small>
							<i class="fa fa-pencil sr-only" aria-hidden="true"></i>
						</div>
					<?php
					} else {
					?>
						<div class="formato" data-width="<?php echo $objBanner->getWidth(); ?>" data-height="<?php echo $objBanner->getHeight(); ?>">
							<div class="demo" style="width:<?php echo ($objBanner->getWidth() / 1500 * (COL_CONTENT_WIDTH - 20)); ?>px; height:<?php echo ($objBanner->getHeight() / 1500 * (COL_CONTENT_WIDTH - 20)); ?>px"></div>
							<?php echo $objBanner->getName(); ?><br>
							<small><?php echo $objBanner->getWidth(); ?>x<?php echo $objBanner->getHeight(); ?></small>
							<i class="fa fa-pencil sr-only" aria-hidden="true"></i>
						</div>
					<?php	
					}
				}
				?>
			</div>
		</div>
	</div>
	<div id="editor">
		
	  	<div class="editor-single" data-panel="template">
			<div class="content">
				<h2>Templates</h2>
				<div>
					<div class="template" data-id="A">
						<iframe src="<?php echo ROOT_PATH . $objBanner->getId() . "/A/" . $initWidth . "x" . $initHeight; ?>" width="<?php echo COL_CONTENT_WIDTH; ?>px" height="<?php echo COL_CONTENT_WIDTH * $initHeight / $initWidth; ?>px"></iframe>
						<div class="clickme"></div>
					</div>
				</div>
				<div>
					<div class="template" data-id="B">
						<iframe src="<?php echo ROOT_PATH . $objBanner->getId() . "/B/" . $initWidth . "x" . $initHeight; ?>" width="<?php echo COL_CONTENT_WIDTH; ?>px" height="<?php echo COL_CONTENT_WIDTH * $initHeight / $initWidth; ?>px"></iframe>
						<div class="clickme"></div>
					</div>
				</div>
				<div>
					<div class="template" data-id="C">
						<iframe src="<?php echo ROOT_PATH . $objBanner->getId() . "/C/" . $initWidth . "x" . $initHeight; ?>" width="<?php echo COL_CONTENT_WIDTH; ?>px" height="<?php echo COL_CONTENT_WIDTH * $initHeight / $initWidth; ?>px"></iframe>
						<div class="clickme"></div>
					</div>
				</div>
				<div>
					<div class="template" data-id="D">
						<iframe src="<?php echo ROOT_PATH . $objBanner->getId() . "/D/" . $initWidth . "x" . $initHeight; ?>" width="<?php echo COL_CONTENT_WIDTH; ?>px" height="<?php echo COL_CONTENT_WIDTH * $initHeight / $initWidth; ?>px"></iframe>
						<div class="clickme"></div>
					</div>
				</div>
				<div>
					<div class="template" data-id="E">
						<iframe src="<?php echo ROOT_PATH . $objBanner->getId() . "/E/" . $initWidth . "x" . $initHeight; ?>" width="<?php echo COL_CONTENT_WIDTH; ?>px" height="<?php echo COL_CONTENT_WIDTH * $initHeight / $initWidth; ?>px"></iframe>
						<div class="clickme"></div>
					</div>
				</div>
				<div>
					<div class="template" data-id="F">
						<iframe src="<?php echo ROOT_PATH . $objBanner->getId() . "/F/" . $initWidth . "x" . $initHeight; ?>" width="<?php echo COL_CONTENT_WIDTH; ?>px" height="<?php echo COL_CONTENT_WIDTH * $initHeight / $initWidth; ?>px"></iframe>
						<div class="clickme"></div>
					</div>
				</div>
				<div>
					<div class="template" data-id="G">
						<iframe src="<?php echo ROOT_PATH . $objBanner->getId() . "/G/" . $initWidth . "x" . $initHeight; ?>" width="<?php echo COL_CONTENT_WIDTH; ?>px" height="<?php echo COL_CONTENT_WIDTH * $initHeight / $initWidth; ?>px"></iframe>
						<div class="clickme"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="bottom-action-container sr-only">
			<div class="row"><div class="col-md-6"><div id="add-new-format" class="btn btn-secondary">NEW FORMAT</div></div><div class="col-md-6"><div id="reset-all" class="btn btn-secondary">RESET ALL</div></div></div>
			<a data-fancybox data-type="ajax" data-src="<?php echo ROOT_PATH ?>ajax/export-recap.php?bannerId=<?php echo $objBanner->getId(); ?>" id="export-banner-set" class="btn btn-success col-md-12" href="javascript:void(0)">EXPORT</a>
		</div>
	</div>
</div>