<?php # echo var_dump($campagnaVar); ?>
<style>
	#content{
		height: auto;
	}
</style>
<div class="modal fade" id="modalareaselect" data-type='banner' tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Select main area</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<form class="container insert-new-container pb-5 full-height" data-toggle="validator">
		<input type="hidden" id="idCampagna" value="<?php echo ((!empty($objCampagna) && is_object($objCampagna))? $objCampagna->getId():'') ?>" />
		<input type="hidden" id="idBanner" value="<?php echo ((!empty($objBanner) && is_object($objBanner))? $objBanner->getId():'') ?>" />
		<input type="hidden" id="projectType" value="banner" />
		<input type="hidden" id="imageUrl" value='<?php echo ((!empty($campagnaVar) && isset($campagnaVar['main']['url']['http']))? $campagnaVar['main']['url']['http']:'') ?>' />
		<input type="hidden" id="imageThumbUrl" value='<?php echo ((!empty($campagnaVar) && isset($campagnaVar['logo']['urlV']['url']))? $campagnaVar['logo']['urlV']['http']:'') ?>' />
		<input type="hidden" id="coordMainAreaBanner" value='<?php echo ((!empty($campagnaVar) && isset($campagnaVar['main']['x1']))?'{"x1":'.$campagnaVar['main']['x1'].',"y1":'.$campagnaVar['main']['y1'].',"x2":'.$campagnaVar['main']['x2'].',"y2":'.$campagnaVar['main']['y2'].'}':'{}') ?>' />
		<input type="hidden" id="coordMainAreaLogo" value='{}' />
		
		<div class="pt-5">			
			<div class="form-group">
				<label for="campaignName">Banner Name *</label>
			    <div class="input-group">
				    <div class="input-group-addon">&nbsp;</div>
				    <input type="text" class="form-control" id="campaignName" value="<?php echo ((!empty($objCampagna) && is_object($objCampagna))?$objCampagna->getName():'') ?>" required>
				 </div>
			</div>
		</div>		
		<div class="row mt-3 mb-3">
			<div class="col-6">
				<div class="form-group">
						<label>Banner Image *</label><br/>
						<span class="btn btn-success fileinput-button">
					        <i class="glyphicon glyphicon-plus"></i>
					        <span>Add files...</span>
					        <!-- The file input field used as target for the file upload widget -->
					        <input class="fileupload" id="bannerUp" type="file" value="<?php echo ((!empty($campagnaVar) && isset($campagnaVar['main']['url']['http']))? $campagnaVar['main']['url']['http']:'') ?>">
					    </span>
					    <br>
					    <br>
					    <!-- The global progress bar -->
					    <div id="progress-bannerUp" class="progress">
					        <div class="progress-bar progress-bar-success" style="<?php echo (!empty($campagnaVar) && isset($campagnaVar['main']['url']['http'])?'width:100%':'') ?>"></div>
					    </div>
					    <!-- The container for the uploaded files -->
					    <div id="files-bannerUp" class="files">
					    <?php if(!empty($campagnaVar) && isset($campagnaVar['main']['url']['http'])){ ?>
					    	<div>
					    		<a target="_blank" href="<?php echo $campagnaVar['main']['url']['http'] ?>">
					    			<p><img width="100" src="<?php  echo $campagnaVar['main']['url']['http']; ?>" /><br><span><?php echo $campagnaVar['main']['url']['http'] ?></span><br></p>
					    		</a>
					    	</div>
					    <?php } ?>
					    </div>
			    </div>
			</div>
			<div class="col-6">
				<div class="form-group">
				<label>Logo Image *</label><br/>
					<span class="btn btn-success fileinput-button">
				        <i class="glyphicon glyphicon-plus"></i>
				        <span>Add files...</span>
				        <!-- The file input field used as target for the file upload widget -->
				        <input class="fileupload"  id="logoUp" type="file" value="<?php echo ((!empty($campagnaVar) && isset($campagnaVar['logo']['urlV']['http']))? $campagnaVar['logo']['urlV']['http']:'') ?>">
				    </span>
				    <br>
				    <br>
				    <!-- The global progress bar -->
				    <div id="progress-logoUp" class="progress">
				        <div class="progress-bar progress-bar-success" style="<?php echo (!empty($campagnaVar) && isset($campagnaVar['logo']['urlV']['http'])?'width:100%':'') ?>"></div>
				    </div>
				    <!-- The container for the uploaded files -->
				    <div id="files-logoUp" class="files">
			    	 <?php if(!empty($campagnaVar) && isset($campagnaVar['logo']['urlV']['http'])){ ?>
					    	<div>
					    		<a target="_blank" href="<?php echo $campagnaVar['logo']['urlV']['http'] ?>">
					    			<p><img width="100" src="<?php  echo $campagnaVar['logo']['urlV']['http']; ?>" /><br><span><?php echo $campagnaVar['logo']['urlV']['http'] ?></span><br></p>
					    		</a>
					    	</div>
					    <?php } ?>
				    </div>
			    </div>
			</div>
		</div>
		<label for="bgBanner">Background Banner color</label>
		<div id="bgBannerPicker" class="input-group colorpicker-component">
		    <input id="bgBanner" type="text" value="<?php #echo ((!empty($objBanner) && is_object($objBanner))?$objBanner->getBgBanner():'') ?>" class="form-control" />
		    <span class="input-group-addon"><i></i></span>
		</div>
		<div class="form-group">
			<label for="headBanner">Head banner *</label>
		    <div class="input-group">
			    <div class="input-group-addon">&nbsp;</div>
			    <input type="text" class="form-control" id="headText" value="<?php echo ((!empty($campagnaVar) && isset($campagnaVar['wrapperTestiComposti']['texts'][0]))? $campagnaVar['wrapperTestiComposti']['texts'][0]['text'][0]:'') ?>" placeholder="" required />
			 </div>
		</div>
		<div class="form-group">
			<label for="headBanner">Sub Head banner</label>
		    <div class="input-group">
			    <div class="input-group-addon">&nbsp;</div>
			    <input type="text" class="form-control" id="subHeadText" value="<?php echo ((!empty($campagnaVar) && isset($campagnaVar['wrapperTestiComposti']['texts'][1]))? $campagnaVar['wrapperTestiComposti']['texts'][1]['text'][0]:'') ?>" placeholder="">
			 </div>
		</div>
		<div class="row">
			<div class="col-6">
				<div class="form-group">
					<label for="webSiteText">Web Site Text</label>
				    <div class="input-group">
					    <div class="input-group-addon">&nbsp;</div>
					    <input type="text" class="form-control" id="webSiteText" value="<?php echo ((!empty($campagnaVar) && isset($campagnaVar['web']['text'][0]))? $campagnaVar['web']['text'][0]:'') ?>" placeholder="">
					 </div>
				</div>	
			</div>
			<div class="col-6">
				<div class="form-group">
					<label for="webSiteLink">Web Site Link</label>
				    <input type="text" class="form-control" id="webSiteLink" value="<?php echo ((!empty($campagnaVar) && isset($campagnaVar['web']['link']))? $campagnaVar['web']['link']:'') ?>" placeholder="">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-6">
				<div class="form-group">
					<label for="ctaText">Call To Action Text</label>
				    <div class="input-group">
					    <div class="input-group-addon">&nbsp;</div>
					    <input type="text" class="form-control" id="ctaText" value="<?php echo ((!empty($campagnaVar) && isset($campagnaVar['cta']['text'][0]))? $campagnaVar['cta']['text'][0]:'') ?>" placeholder="">
					 </div>
				</div>	
			</div>
			<div class="col-6">
				<div class="form-group">
					<label for="ctaLink">Call to Action Link</label>
				    <input type="text" class="form-control" id="ctaLink" value="<?php echo ((!empty($campagnaVar) && isset($campagnaVar['cta']['link']))? $campagnaVar['cta']['link']:'') ?>" placeholder="">
				</div>
			</div>
		</div>

		<div class="form-group">
			<label>Add Element</label>
			<div class="row">
				<div class="col-9">
			   		<select class="form-control col-12" id="newElementList" placeholder=""></select>
			    </div>
			    <div class="col-3">
			    	<div id="addNew" class=" col-12 btn btn-secondary">ADD NEW</div>
			    </div>
		    </div>
		</div>

		<div class="row">
			<div class="col-3 offset-6">
				<a href="<?php echo ROOT_PATH ?>admin/new" id="undo" data-fancybox-close class="col-12 btn btn-secondary">CANCEL</a>
			</div>
			<div class="col-3">
				<div id="save" class="col-12 btn btn-success">SAVE</div>
			</div>
		</div>
</form>