<?php
$initWidth = 600;
$initHeight = 300;
define("COL_CONTENT_WIDTH",250);

?>

<script>
	var COL_CONTENT_WIDTH = <?php echo COL_CONTENT_WIDTH; ?>;
	

	bannerLink = "<?php echo ROOT_PATH . $objBanner->getId() . "/"; ?>";

	$.needToConfirm = true;
	function OnBeforeUnLoad () 
	{
	    return "Are you sure?";
	}
	
	window.onbeforeunload= function(){
		if($.needToConfirm) {
			return OnBeforeUnLoad()
		}

		return "Are you sure?";
	}

	//window.setInterval(function () {window.stop()},10);
	
	
	$(function(){
		updateTemplateSize(<?php echo $initWidth; ?>,<?php echo $initHeight; ?>);		
	});
	
</script>
<style>
#preview{
	height: calc(100% - 36px); /* urlPageContainer height */
	position: relative;
}
</style>

<input type="hidden" name="bannerId" id="bannerId" value="<?php echo $objCampagna->getId(); ?>" />
<input type="hidden" name="idAlfanumeric" id="idAlfanumeric" value="<?php echo $objCampagna->getIdentAlfanumeric(); ?>" />
<!-- da compilare nel caso di apertura di un salvataggio -->
<input type="hidden" name="previewId" id="previewId" value="" />

<div class="urlPageContainer">
	<div class="lBtns">
		<div class="barBtn" style="background-image: url('../../assets/icons/prev.svg');"></div>
		<div class="barBtn" style="background-image: url('../../assets/icons/next.svg');"></div>
	</div>
	<div class="bar">
		<div class="http">http://</div>
		<input id="urlPage" name="urlPage" type="text" class="form-control" value="www.mediaworld.it" />
	</div>
	<div class="rBtns">
		<div class="barBtn" style="background-image: url('../../assets/icons/reload.svg');"></div>
		<div class="barBtn" style="background-image: url('../../assets/icons/gallery.svg');"></div>
	</div>
</div>
<div id="preview">
	<iframe src="<?php echo ROOT_PATH ?>templates/banner-site-sim.php" width="100%" height="100%"></iframe>
	<div class="share">
		<button class="share-toggle-button">
			<i class="share-icon fa fa-share "></i>
		</button>
		<ul class="share-items">
			<li class="share-item">
				<a data-fancybox data-type="ajax" data-src="<?php echo ROOT_PATH ?>ajax/actionMenuPrev.php?type=open-preview&bannerId=<?php echo $objCampagna->getId() ?>" href="javascript:void(0)" class="share-button">
					<i class="share-icon fa fa-th"></i>
					<label>Open preview</label>
				</a>
			</li>
			<li data-action="copy-url" class="share-item">
				<a href="#" class="share-button">
					<i class="share-icon fa fa-link"></i>
					<label>Copy url</label>
				</a>
			</li>
			<li class="share-item">
				<a href="#" class="share-button">
					<i class="share-icon fa fa-envelope-o"></i>
					<label>Send mail</label>
				</a>
			</li>
			<li data-action="save-preview" class="share-item">
				<a href="#" class="share-button">
					<i class="share-icon fa fa-floppy-o"></i>
					<label>Save preview</label>
				</a>
			</li>
		</ul>
	</div>
</div>
<div id="editor">
	<div class="editor-single active" data-panel="template">
		<div class="content">
			<h2>Templates</h2>
			<div>
			<?php foreach($arrayTemplate as $objTemp){ ?>
				<div class="template" data-id="<?php echo $objTemp->getId() ?>">
					<iframe src="<?php echo ROOT_PATH . "camp/". $objCampagna->getIdentAlfanumeric() . "/template/".$objTemp->getId() ?>" width="<?php echo COL_CONTENT_WIDTH; ?>px" height="<?php echo COL_CONTENT_WIDTH * $initHeight / $initWidth; ?>px"></iframe>
					<div class="clickme"></div>
				</div>
			<?php } ?>
			</div>
		</div>
	</div>
</div>
