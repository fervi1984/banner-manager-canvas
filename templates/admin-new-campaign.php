<?php #echo var_dump($campagnaVar); ?>
<style>
#content {
	height: auto;
}
</style>
<!-- Modal -->
<div class="modal fade" id="modalareaselect" data-type='campaign'
	tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
	aria-hidden="true">
	<div class="modal-dialog  modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Select main area</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">...</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
</div>
<div id="insert-new-slider" class="carousel slide" data-interval="false">
	<ol class="carousel-indicators">
		<li data-target="#insert-new-slider" data-slide-to="0" class="active"></li>
		<li data-target="#insert-new-slider" data-slide-to="1"></li>
	</ol>
	<form class="carousel-inner" data-toggle="validator" role="listbox">
		<div class="carousel-item active">
			<div class="container insert-new-container pb-5 mb-5 full-height">
				<input type="hidden" id="idCampagna" value="<?php echo ((!empty($objCampagna) && is_object($objCampagna))? $objCampagna->getId():'') ?>" />
				<input type="hidden" id="projectType" value="campaign" />
				<input type="hidden" id="imageUrl"	value='<?php echo ((!empty($campagnaVar) && isset($campagnaVar['main']['url']['http']))? $campagnaVar['main']['url']['http']:'') ?>' />
				<input type="hidden" id="imageThumbUrl"	value='<?php echo ((!empty($campagnaVar) && isset($campagnaVar['logo']['urlV']['url']))? $campagnaVar['logo']['urlV']['http']:'') ?>' />
				<input type="hidden" id="coordMainAreaBanner" value='<?php echo ((!empty($campagnaVar) && isset($campagnaVar['main']['x1']))?'{"x1":'.$campagnaVar['main']['x1'].',"y1":'.$campagnaVar['main']['y1'].',"x2":'.$campagnaVar['main']['x2'].',"y2":'.$campagnaVar['main']['y2'].'}':'{}') ?>' />
				<input type="hidden" id="coordMainAreaLogo" value='{}' />
				
				<div class="pt-5">
					<div class="form-group">
						<label for="campaignName">Campaign Name *</label>
						<div class="input-group">
							<div class="input-group-addon">&nbsp;</div>
							<input type="text" class="form-control" id="campaignName"
								value="<?php echo ((!empty($objCampagna) && is_object($objCampagna))?$objCampagna->getName():'') ?>"
								required>
						</div>
					</div>
				</div>
				<div class="row mt-3 mb-3">
					<div class="col-6">
						<div class="form-group">
							<label>Banner Image *</label><br /> <span
								class="btn btn-success fileinput-button"> <i
								class="glyphicon glyphicon-plus"></i> <span>Add files...</span>
								<!-- The file input field used as target for the file upload widget -->
								<input class="fileupload" id="bannerUp" type="file"
								value="<?php echo ((!empty($campagnaVar) && isset($campagnaVar['main']['url']['http']))? $campagnaVar['main']['url']['http']:'') ?>">
							</span> <br> <br>
							<!-- The global progress bar -->
							<div id="progress-bannerUp" class="progress">
								<div class="progress-bar progress-bar-success" style="<?php echo (!empty($campagnaVar) && isset($campagnaVar['main']['url']['http'])?'width:100%':'') ?>"></div>
							</div>
							<!-- The container for the uploaded files -->
							<div id="files-bannerUp" class="files">
							    <?php if(!empty($campagnaVar) && isset($campagnaVar['main']['url']['http'])){ ?>
							    	<div>
									<a target="_blank" href="<?php echo $campagnaVar['main']['url']['http'] ?>">
										<p>
											<img width="100"
												src="<?php  echo $campagnaVar['main']['url']['http']; ?>" /><br>
											<span><?php echo $campagnaVar['main']['url']['http'] ?></span><br>
										</p>
									</a>
								</div>
							    <?php } ?>
							    </div>
						</div>
					</div>
					<div class="col-6">
						<div class="form-group">
							<label>Logo Image *</label><br />
							<span
								class="btn btn-success fileinput-button"> <i
								class="glyphicon glyphicon-plus"></i> <span>Add files...</span>
								<!-- The file input field used as target for the file upload widget -->
								<input class="fileupload" id="logoUp" type="file"
								value="<?php echo ((!empty($campagnaVar) && isset($campagnaVar['logo']['urlV']['http']))? $campagnaVar['logo']['urlV']['http']:'') ?>">
							</span> <br> <br>
							<!-- The global progress bar -->
							<div id="progress-logoUp" class="progress">
								<div class="progress-bar progress-bar-success" style="<?php echo (!empty($campagnaVar) && isset($campagnaVar['logo']['urlV']['http'])?'width:100%':'') ?>"></div>
							</div>
							<!-- The container for the uploaded files -->
							<div id="files-logoUp" class="files">
					    	 <?php if(!empty($campagnaVar) && isset($campagnaVar['logo']['urlV']['http'])){ ?>
							    	<div>
									<a target="_blank" href="<?php echo $campagnaVar['logo']['urlV']['http'] ?>">
										<p>
											<img width="100"
												src="<?php  echo $campagnaVar['logo']['urlV']['http']; ?>" /><br>
											<span><?php echo $campagnaVar['logo']['urlV']['http'] ?></span><br>
										</p>
									</a>
								</div>
							    <?php } ?>
						    </div>
						</div>
					</div>
				</div>
				<label for="bgBanner">Background Banner color</label>
				<div id="bgBannerPicker" class="input-group colorpicker-component">
					<input id="bgBanner" type="text" value="<?php #echo ((!empty($objBanner) && is_object($objBanner))?$objBanner->getBgBanner():'') ?>" class="form-control" /> <span class="input-group-addon"><i></i></span>
				</div>
				<!--  <label for="colorText">Text color *</label>
				<div id="colorTextPicker" class="input-group colorpicker-component">
				    <input id="colorText" type="text" value="<?php echo ((!empty($objBanner) && is_object($objBanner))?$objBanner->getColorText():'') ?>" class="form-control" required />
				    <span class="input-group-addon"><i></i></span>
				</div> 
				<label for="bgText">Background Text color</label>
				<div id="bgTextPicker" class="input-group colorpicker-component">
				    <input id="bgText" type="text" value="<?php echo ((!empty($objBanner) && is_object($objBanner))?$objBanner->getBgText():'') ?>" class="form-control" />
				    <span class="input-group-addon"><i></i></span>
				</div> -->
				<div class="form-group">
					<label for="headBanner">Head banner *</label>
					<div class="input-group">
						<div class="input-group-addon">&nbsp;</div>
						<input type="text" class="form-control" id="headText"
							value="<?php echo ((!empty($campagnaVar) && isset($campagnaVar['wrapperTestiComposti']['texts'][0]))? $campagnaVar['wrapperTestiComposti']['texts'][0]['text'][0]:'') ?>"
							placeholder="" required />
					</div>
				</div>
				<div class="form-group">
					<label for="headBanner">Sub Head banner</label>
					<div class="input-group">
						<div class="input-group-addon">&nbsp;</div>
						<input type="text" class="form-control" id="subHeadText"
							value="<?php echo ((!empty($campagnaVar) && isset($campagnaVar['wrapperTestiComposti']['texts'][1]))? $campagnaVar['wrapperTestiComposti']['texts'][1]['text'][0]:'') ?>"
							placeholder="">
					</div>
				</div>
				<div class="row">
					<div class="col-6">
						<div class="form-group">
							<label for="webSiteText">Web Site Text</label>
							<div class="input-group">
								<div class="input-group-addon">&nbsp;</div>
								<input type="text" class="form-control" id="webSiteText"
									value="<?php echo ((!empty($campagnaVar) && isset($campagnaVar['web']['text'][0]))? $campagnaVar['web']['text'][0]:'') ?>"
									placeholder="">
							</div>
						</div>
					</div>
					<div class="col-6">
						<div class="form-group">
							<label for="webSiteLink">Web Site Link</label> <input type="text"
								class="form-control" id="webSiteLink"
								value="<?php echo ((!empty($campagnaVar) && isset($campagnaVar['web']['link']))? $campagnaVar['web']['link']:'') ?>"
								placeholder="">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-6">
						<div class="form-group">
							<label for="ctaText">Call To Action Text</label>
							<div class="input-group">
								<div class="input-group-addon">&nbsp;</div>
								<input type="text" class="form-control" id="ctaText"
									value="<?php echo ((!empty($campagnaVar) && isset($campagnaVar['cta']['text'][0]))? $campagnaVar['cta']['text'][0]:'') ?>"
									placeholder="">
							</div>
						</div>
					</div>
					<div class="col-6">
						<div class="form-group">
							<label for="ctaLink">Call to Action Link</label> <input
								type="text" class="form-control" id="ctaLink"
								value="<?php echo ((!empty($campagnaVar) && isset($campagnaVar['cta']['link']))? $campagnaVar['cta']['link']:'') ?>"
								placeholder="">
						</div>
					</div>
				</div>

				<div class="form-group">
					<label>Add Element</label>
					<div class="row">
						<div class="col-9">
							<select class="form-control col-12" id="newElementList" placeholder=""></select>
						</div>
						<div class="col-3">
							<div id="addNew" class=" col-12 btn btn-secondary">ADD NEW</div>
						</div>
					</div>
				</div>


				<div class="row">
					<div class="col-3 offset-6">
						<a href="<?php echo ROOT_PATH ?>admin/new" id="undo"
							data-fancybox-close class="col-12 btn btn-secondary">CANCEL</a>
					</div>
					<?php if (empty($objBanner)){ ?>
					<div class="col-3">
						<a class="carousel-control-next btn btn-success col-12 d-inline"
							href="#insert-new-slider" data-slide="next">NEXT</a>
					</div>
					<?php } else { ?>
					<div class="col-3">
						<a id="save" class="btn btn-success col-12"
							href="javascript:void(0)">SAVE CAMPAIGN</a>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
		<div class="carousel-item">
			<div class="container insert-new-container pb-5 mb-5 full-height">
				<div class="row mt-2">
					<span class="btn btn-secondary border-0 d-inline">ADD FORMATS</span>
					<span class="btn btn-secondary fileinput-button d-inline"> <i
						class="glyphicon glyphicon-plus"></i> <span>Upload Excel</span> <!-- The file input field used as target for the file upload widget -->
						<input class="fileupload" id="formatUp" type="file">
					</span>
				</div>
				<div id="formatTabForm" class="mt-3">
					<?php 
					if (!empty($objCampagna)){
						foreach($objCampagna->getArrayBanner() as $objBanner){ ?>
							<div class="row mt-2">
								<input type="hidden" class="formatId" value="<?php echo $objBanner->getId() ?>">
								<div class="col-3">
									<input type="number" class="formatWidth form-control" value="<?php echo $objBanner->getWidth() ?>" placeholder="width">
								</div>
								<div class="col-3">
									<input type="number" class="formatHeight form-control" value="<?php echo $objBanner->getHeight() ?>" placeholder="height">
								</div>
								<div class="col-3">
									<input type="text" class="formatName form-control" value="<?php echo $objBanner->getName() ?>" placeholder="name">
								</div>
								<div class="col-3">
									<input type="checkbox" class="formatSelected" checked value="">
								</div>
							</div>
					<?php 
						}
					} ?>
					<div class="row mt-2">
						<div class="col-3">
							<input type="number" class="formatWidth form-control" value="" placeholder="width">
						</div>
						<div class="col-3">
							<input type="number" class="formatHeight form-control" value="" placeholder="height">
						</div>
						<div class="col-3">
							<input type="text" class="formatName form-control" value="" placeholder="name">
						</div>
						<div class="col-3">
							<input type="checkbox" class="formatSelected" value="">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12 mt-3">
						<div id="addNewFormat" class="btn btn-secondary col-12 ">ADD NEW</div>
					</div>
				</div>
				<div class="row mt-5">
					<div class="col-3 offset-6">
						<a class="carousel-control-prev btn btn-secondary col-12"
							href="#insert-new-slider" data-slide="prev">PREVIOUS</a>
					</div>
					<div class="col-3">
						<a id="save" class="btn btn-success col-12"
							href="javascript:void(0)">SAVE CAMPAIGN</a>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>