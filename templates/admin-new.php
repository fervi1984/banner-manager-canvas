<div class="container huge-icon-container full-height">
	<div class="d-flex full-height">
		<div class="row col-12 align-self-center">
			<div class="col col-3 offset-2">
				<div class="inner-wrapper">
					<div class="tassello">
						<a href="<?php echo ROOT_PATH ?>admin/new-static">NEW STATIC BANNER</a>
					</div>
				</div>
			</div>
			<div class="col col-3">
				<div class="inner-wrapper">
					<div class="tassello">
						<a href="<?php echo ROOT_PATH ?>admin/new-banner">NEW DYNAMIC BANNER</a>
					</div>
				</div>
			</div>
			<div class="col col-3">
				<div class="inner-wrapper">
					<div class="tassello">
						<a href="<?php echo ROOT_PATH ?>admin/new-campaign">NEW DYNAMIC CAMPAIGN</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>