<?php
$initWidth = ($objBanner->getWidth()) ? $objBanner->getWidth() : 720;
$initHeight = ($objBanner->getHeight()) ? $objBanner->getHeight() : 300;
define ( "COL_CONTENT_WIDTH", 250 );
?>

<script>
	var COL_CONTENT_WIDTH = <?php echo COL_CONTENT_WIDTH; ?>;
	bannerLink = "<?php echo ROOT_PATH . $objBanner->getId() . "/"; ?>";
	$(function(){
		//updateTemplateSize(<?php echo $initWidth; ?>,<?php echo $initHeight; ?>);
	});
</script>
<div id="edit-banner-page">
	<div id="preview">
		<input type="hidden" name="bannerId" id="bannerId" value="<?php echo $objBanner->getId(); ?>" /> 
		<input type="hidden" name="bannerIdTemp" id="bannerIdTemp" value="A" />
		<input type="hidden" name="bannerWidth" id="bannerWidth" value="<?php echo $initWidth; ?>" /> 
		<input type="hidden" name="bannerHeight" id="bannerHeight" value="<?php echo $initHeight; ?>" />
		<input type="hidden" name="idFormato" id="idFormato" value="<?php echo $objBanner->getIdFormato(); ?>" />
		
		<iframe id="iframe" src="<?php echo ROOT_PATH ."camp/".$objCampagna->getIdentAlfanumeric(); ?>" width="<?php echo $initWidth; ?>" height="<?php echo $initHeight; ?>"></iframe>
		<div class="bottom-action-container">
			<div id="info-banner" class="btn btn-secondary">INFO BANNER</div>
			<div id="save-single-banner" class="btn btn-success">SAVE BANNER</div>
		</div>
	</div>
	<div id="editor">
		<div class="editor-single active" data-panel="resize">
			<div class="content">
				<h2>Custom</h2>
				<div class="custom">
					<input type="text" name="customWidth"
						value="<?php echo $initWidth; ?>"><input type="text"
						name="customHeight" value="<?php echo $initHeight; ?>"> <input
						type="button" value="Save" id="salvaCustom">
				</div>
				<div class="spacer"></div>
				<h2>Standard Presets</h2>
			<?php
			$lstFormati = new FormatoList ( $connection );
			$lstFormati->initAllVisible();
			foreach ( $lstFormati->toArray () as $objFormato ) {
				?><div class="formato"
					data-id="<?php echo $objFormato->getId(); ?>"
					data-width="<?php echo $objFormato->getWidth(); ?>"
					data-height="<?php echo $objFormato->getHeight(); ?>">
					<div class="demo" style="width:<?php echo $objFormato->getScaledWidth(); ?>px; height:<?php echo $objFormato->getScaledHeight(); ?>px"></div>
					<?php echo $objFormato->getNome(); ?><br> <small><?php echo $objFormato->getWidth(); ?>x<?php echo $objFormato->getHeight(); ?></small>
				</div><?php
			}
			?>
		</div>
		</div>
		<div class="editor-single" data-panel="template">
			<div class="content">
				<h2>Templates</h2>
				<div>
					<div class="template" data-id="A">
						<!--<iframe
							src="<?php echo ROOT_PATH . $objBanner->getId() . "/A/" . $initWidth . "x" . $initHeight; ?>"
							width="<?php echo COL_CONTENT_WIDTH; ?>px"
							height="<?php echo COL_CONTENT_WIDTH * $initHeight / $initWidth; ?>px"></iframe>-->
							Template A
						<div class="clickme"></div>
					</div>
				</div>
				<div>
					<div class="template" data-id="B">
						<!--<iframe
							src="<?php echo ROOT_PATH . $objBanner->getId() . "/B/" . $initWidth . "x" . $initHeight; ?>"
							width="<?php echo COL_CONTENT_WIDTH; ?>px"
							height="<?php echo COL_CONTENT_WIDTH * $initHeight / $initWidth; ?>px"></iframe>-->
							Template B
						<div class="clickme"></div>
					</div>
				</div>
				<div>
					<div class="template" data-id="C">
						<!--<iframe
							src="<?php echo ROOT_PATH . $objBanner->getId() . "/C/" . $initWidth . "x" . $initHeight; ?>"
							width="<?php echo COL_CONTENT_WIDTH; ?>px"
							height="<?php echo COL_CONTENT_WIDTH * $initHeight / $initWidth; ?>px"></iframe>-->
							Template C
						<div class="clickme"></div>
					</div>
				</div>
				<div>
					<div class="template" data-id="D">
						<!--<iframe
							src="<?php echo ROOT_PATH . $objBanner->getId() . "/D/" . $initWidth . "x" . $initHeight; ?>"
							width="<?php echo COL_CONTENT_WIDTH; ?>px"
							height="<?php echo COL_CONTENT_WIDTH * $initHeight / $initWidth; ?>px"></iframe>-->
							Template D
						<div class="clickme"></div>
					</div>
				</div>
				<div>
					<div class="template" data-id="E">
						<!--<iframe
							src="<?php echo ROOT_PATH . $objBanner->getId() . "/E/" . $initWidth . "x" . $initHeight; ?>"
							width="<?php echo COL_CONTENT_WIDTH; ?>px"
							height="<?php echo COL_CONTENT_WIDTH * $initHeight / $initWidth; ?>px"></iframe>-->
							Template E
						<div class="clickme"></div>
					</div>
				</div>
				<div>
					<div class="template" data-id="F">
						<!--<iframe
							src="<?php echo ROOT_PATH . $objBanner->getId() . "/F/" . $initWidth . "x" . $initHeight; ?>"
							width="<?php echo COL_CONTENT_WIDTH; ?>px"
							height="<?php echo COL_CONTENT_WIDTH * $initHeight / $initWidth; ?>px"></iframe>-->
							Template F
						<div class="clickme"></div>
					</div>
				</div>
				<div>
					<div class="template" data-id="G">
						<!--<iframe
							src="<?php echo ROOT_PATH . $objBanner->getId() . "/G/" . $initWidth . "x" . $initHeight; ?>"
							width="<?php echo COL_CONTENT_WIDTH; ?>px"
							height="<?php echo COL_CONTENT_WIDTH * $initHeight / $initWidth; ?>px"></iframe>-->
							Template G
						<div class="clickme"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
