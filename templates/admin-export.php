<?php
$initWidth = 720;
$initHeight = 300;
define("COL_CONTENT_WIDTH",250);

?>

<script>
	var COL_CONTENT_WIDTH = <?php echo COL_CONTENT_WIDTH; ?>;
	bannerLink = "<?php echo ROOT_PATH . $objBanner->getId() . "/"; ?>";
	$(function(){
		updateTemplateSize(<?php echo $initWidth; ?>,<?php echo $initHeight; ?>);
	});
</script>
<style>
	#editor .editor-single{
		height: calc( 100% - 100px);
	}
</style>
<input type="hidden" name="bannerId" id="bannerId" value="<?php echo $objBanner->getId(); ?>" />
<div id="preview" class="rightPos">
	<iframe id="iframe" src="<?php echo ROOT_PATH . $objBanner->getId() . "/D/0x0"; ?>" width="<?php echo $initWidth; ?>" height="<?php echo $initHeight; ?>"></iframe>
	<div class="bottom-action-container">
		<div id="save-banner" class="btn btn-success">SAVE BANNER</div>
	</div>
</div>
<div id="editor" class="leftPos">
	<div class="editor-single active" data-panel="resize">
		<div class="content">
			<?php
			$lstFormati = new FormatoList($connection);
			$lstFormati->init();
			foreach($lstFormati->toArray() as $objFormato){
				?><div class="formato" data-width="<?php echo $objFormato->getWidth(); ?>" data-height="<?php echo $objFormato->getHeight(); ?>">
					<div class="demo" style="width:<?php echo $objFormato->getScaledWidth(); ?>px; height:<?php echo $objFormato->getScaledHeight(); ?>px"></div>
					<?php echo $objFormato->getNome(); ?><br>
					<small><?php echo $objFormato->getWidth(); ?>x<?php echo $objFormato->getHeight(); ?></small>
				</div><?php
			}
			?>
		</div>
	</div>
	<div class="bottom-action-container">
		<div class="row"><div class="col-md-6"><div id="add-new-format" class="btn btn-secondary">NUOVO FORMATO</div></div><div class="col-md-6"><div id="reset-all" class="btn btn-secondary">RESET ALL</div></div></div>
		<a data-fancybox data-type="ajax" data-src="<?php echo ROOT_PATH ?>ajax/export-recap.php?bannerId=<?php echo $objBanner->getId(); ?>" id="export-banner-set" class="btn btn-success col-md-12" href="javascript:void(0)">EXPORT</a>
	</div>
</div>