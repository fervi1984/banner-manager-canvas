<div class="container huge-icon-container full-height">
	<div class="d-flex full-height">
		<div class="row col-12 align-self-center">
			<div class="col col-3 offset-3">
				<div class="inner-wrapper">
					<div class="tassello">
						<a href="<?php echo ROOT_PATH ?>admin/new">NEW PROJECT</a>
					</div>
				</div>
			</div>
			<div class="col col-3">
				<div class="inner-wrapper">
					<div class="tassello">
						<a href="<?php echo ROOT_PATH ?>admin/projects">OPEN PROJECT</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
