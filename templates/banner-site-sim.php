<?php 
header('Content-type: text/html; charset=UTF-8');
include("../init.php");
include_once('../classes/simple_html_dom.php');

function get_data($url) {
	
	$html = file_get_html($url);
	$html = iconv(mb_detect_encoding($html, mb_detect_order(), true), "UTF-8//IGNORE", $html);
	
	return $html;	
}
/* gets the data from a URL */
/*function get_data($url) {
	$ch = curl_init();
	$timeout = 5;
	$useragent = 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:10.0.2) Gecko/20100101 Firefox/10.0.2';

	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_AUTOREFERER, true);
	curl_setopt($ch, CURLOPT_REFERER, 'http://www.google.com');
	curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_FAILONERROR, true);
	curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
	#curl_setopt( $ch, CURLOPT_COOKIESESSION, true );
	#curl_setopt( $ch, CURLOPT_COOKIEJAR,  $_COOKIE['cookiefile'] );
	#curl_setopt( $ch, CURLOPT_COOKIEFILE, $_COOKIE['cookiefile'] );
	$data = curl_exec($ch);
	$data = @iconv(mb_detect_encoding($data, mb_detect_order(), true), "UTF-8", $data);
	curl_close($ch);
	return $data;
}*/
function addhttp($url) {
	if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
		$url = "http://" . $url;
	}
	return $url;
}
/*function sendRequest($url)
{
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_AUTOREFERER, true);
	curl_setopt($ch, CURLOPT_REFERER, 'http://www.google.com');
	curl_setopt($ch, CURLOPT_ENCODING, '');
	
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		 'User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.46 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
		 'Upgrade-Insecure-Request: 1',
		 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*//*;q=0.8',
		 'Accept-Language: it-IT,it;q=0.8,en-us;q=0.6,en;q=0.4,de,de-DE;q=0.94',
		 'Connection: close'
	));

	$contents = curl_exec($ch);
	$contents = iconv(mb_detect_encoding($contents, mb_detect_order(), true), "UTF-8", $contents);
	
	curl_close($ch);

	return $contents;
}

function getUrlContents($url, $maximumRedirections = null, $currentRedirection = 0)
{
	$result = false;

	$contents = sendRequest($url);

	// Check if we need to go somewhere else

	if (isset($contents) && is_string($contents))
	{
		preg_match_all('/<[\s]*meta[\s]*http-equiv="?REFRESH"?' . '[\s]*content="?[0-9]*;[\s]*URL[\s]*=[\s]*([^>"]*)"?' . '[\s]*[\/]?[\s]*>/si', $contents, $match);

		if (isset($match) && is_array($match) && count($match) == 2 && count($match[1]) == 1)
		{
			if (!isset($maximumRedirections) || $currentRedirection < $maximumRedirections)
			{
				return getUrlContents($match[1][0], $maximumRedirections, ++$currentRedirection);
			}

			$result = false;
		}
		else
		{
			$result = $contents;
		}
	}

	return $contents;
}*/

#$file = "tmp.html";
$mainUrl = (array_key_exists('mainUrl', $_REQUEST) && $_REQUEST['mainUrl']!="")?$_REQUEST['mainUrl']:"http://www.mediaworld.it";
$mainUrl = addhttp($mainUrl);
#https://restpack.io/api/screenshot/v2/capture?access_token=kwtxXSJ5CS1gZXjIfy5CiIvcWuiLteqWJ8LG7Uq1GwsyHttX&url=$mainUrl&format=html
$input =  get_data($mainUrl); //getUrlContents($mainUrl);

$input = preg_replace('/<meta charset[^>]+>/', "<meta charset='UTF-8'>", $input);
$input = preg_replace('/<meta http-equiv[^>]+>/', "", $input);

$pattern = '/([^\\:]|^)(\/\/)([a-zA-Z0-9._-]+)(.)([a-zA-Z0-9_-]+)(\/)([^"]*)/';
$replacement = '"http://${3}${4}${5}${6}${7}';
$input = preg_replace($pattern, $replacement, $input);

$input = str_replace('="/', "=\"$mainUrl/", $input);
$input = str_replace("='/", "=\'$mainUrl/", $input);
$input =  preg_replace('/<a(.*)href="([^"]*)"(.*)>/', '<a${1}href="javascript:void(0)"${3}>', $input);

$css = "iframe{pointer-events: none}
.banner-sel{
	position: relative;
}
.headerEdit{
	top: 0;
	z-index: 2;
}
.editTemplate{
	position: absolute;	
}
.editContainer,
.footerEdit,
.headerEdit{
	position: absolute;
	width: 100%;
	left: 0;
}
.editContainer{
	height: 100%;
	top: 0;
}
.footerEdit{
	bottom:0;
	z-index: 2;
}
.editContainer{
	border: 5px solid #f7ff00;
	color: #000;
	position: relative;
}
.headerEdit{
	height: 25px;
	background: #f7ff00;
	padding: 0 5px;
}
.headerEdit .btn-close{
	float: right;
	display: inline;
	cursor: pointer;
}
.headerEdit .btn-close:after{
	content: '✖';
}
.headerEdit .btn-edit{
	float: left;
	display: inline;
	height: 18px;
	width: 18px;
	background: url('".ROOT_PATH."assets/pencil-icon.png');
	cursor: pointer;
}
.footerEdit .btn-action{
	width: 50%;
	float: left;
	display: inline;
	text-align: center;
	background: #f7ff00;
	font-size: 12px;
	color: #000;
	cursor: pointer;
}
.footerEdit .btn-save{
	background: #449d44;
}
.addBanner{
	position: absolute;
	left: 0;
	top: 0;
	width: 100%;
	height: 100%;
	z-index: 1;
	background: #ccc;
	opacity: 0.3;
}
.addBanner span{
	position: absolute;
	left: 50%;
	top: 50%;
	transform: translation(-50%,-50%);
}
#loading-indicator{
	min-width: 100vw;
	min-height: 100vh;
	height: 100%;
	position: fixed;
	z-index: 100000;
	background: #fff;
	top: 0;
	left: 0;
}
";

$js="
<script src='".ROOT_PATH."js/jquery-1.12.4.min.js'></script>
<script>
	const ROOT_PATH = '".ROOT_PATH."';	
	jQuery( window ).on('load', function(){
		jQuery(\"link[href^='/']\").prop( 'href',
		  function( _idx, oldHref ) {
		    oldHref = oldHref.replace( /^\//, '".$mainUrl."' );
		    oldHref = oldHref.replace( /^http:\/\/".$_SERVER['SERVER_NAME']."/, '".$mainUrl."' );
			return oldHref;
		  }
		);
		jQuery(\"img[src^='/']\").prop( 'src',
		  function( _idx, oldHref ) {
		    oldHref = oldHref.replace( /^\//, '".$mainUrl."' );
		    oldHref = oldHref.replace( /^http:\/\/".$_SERVER['SERVER_NAME']."/, '".$mainUrl."' );
			return oldHref;
		  }
		);
	});		
</script>
<script src='".ROOT_PATH."js/simple-js-dom-inspector.js'></script>";

echo "<div id='preview-js-injection'><style>$css</style>".$js."</div><div id='loading-indicator'></div>".$input;

?>