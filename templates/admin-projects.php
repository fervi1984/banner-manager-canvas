<div class="spacedContainer">
	<div class="pContainer">
		<div class="pRow"><?php
		foreach($arrayProject as $objProject){
			?><div class="pCol-md-3" data-id="<?php echo $objProject->getId(); ?>" data-type="<?php echo $objProject->getType(); ?>">
				<div class="pCard">
					<div class="proportional proportion-pCard-image">
						<div class="proportional-push"></div>
						<div class="proportional-content">
							<div class="pCard-image" style="background-image: url('<?php echo $objProject->getPreview(); ?>'); background-position: contain;">
								<button type="button" class="pClear-input-button pCard-close delete-item"></button>
								<?php if($objProject->getType() == 'campaign'){ ?>
									<div class="countBanner"><?php echo count($objProject->getArrayBanner()) ?></div>
									<a href="<?php echo ROOT_PATH ?>admin/new-campaign/<?php echo $objProject->getId() ?>" type="button" class="pCard-edit fa fa-pencil" aria-hidden="true"></a>
								<?php } else { ?>
									<a href="<?php echo ROOT_PATH ?>admin/new-banner/<?php echo $objProject->getId() ?>" type="button" class="pCard-edit fa fa-pencil" aria-hidden="true"></a>
								<?php } ?>						
							</div>
						</div>
					</div>
					<div class="pCard-body">
						<div class="pCard-buttons">
							<a class="pCard-button pCard-button-secondary" href="<?php echo ROOT_PATH."admin/".$objProject->getId(); ?>"><?php echo $objProject->getName(); ?></a>
						</div>
					</div>
				</div>
			</div><?php
		}
		?></div>
	</div>
</div>
