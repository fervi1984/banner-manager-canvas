<!doctype html>
<html>
	<head>
		<meta charset="UTF-8">
		
		<!-- JQuery -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery.ui.widget.js"></script>
		<!-- Bootstrap -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">

		<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>

		<!-- Switches -->
		<link rel="stylesheet" href="<?php echo ROOT_PATH; ?>plugins/switches/switches.css">

		<!-- Share -->
		<link rel="stylesheet" href="<?php echo ROOT_PATH; ?>plugins/share/share.css">
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>plugins/share/TweenMax.min.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>plugins/share/share.js"></script>

		<!-- fancybox -->
		<script src="<?php echo ROOT_PATH; ?>plugins/fancybox-3.0/dist/jquery.fancybox.min.js"></script>
		<link rel="stylesheet" href="<?php echo ROOT_PATH; ?>plugins/fancybox-3.0/dist/jquery.fancybox.min.css">

		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>plugins/jquery-imgareaselect/css/imgareaselect-default.css" />
		 <script type="text/javascript" src="<?php echo ROOT_PATH; ?>plugins/jquery-imgareaselect/scripts/jquery.imgareaselect.js"></script>

		<!-- jquery fileupload -->
		<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
		<script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
		<!-- The Canvas to Blob plugin is included for image resizing functionality -->
		<script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
		<script src="<?php echo ROOT_PATH; ?>plugins/jquery-fileupload/jquery.fileupload.js"></script>
		<script src="<?php echo ROOT_PATH; ?>plugins/jquery-fileupload/jquery.fileupload-process.js"></script>
		<script src="<?php echo ROOT_PATH; ?>plugins/jquery-fileupload/jquery.fileupload-image.js"></script>
		<script src="<?php echo ROOT_PATH; ?>plugins/jquery-fileupload/main.js"></script>
		<link rel="stylesheet" href="<?php echo ROOT_PATH; ?>plugins/jquery-fileupload/jquery.fileupload.css">

		<link rel="stylesheet" href="<?php echo ROOT_PATH; ?>plugins/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css" />
		<script src="<?php echo ROOT_PATH; ?>plugins/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js"></script>
		<script src="<?php echo ROOT_PATH; ?>plugins/bootstrap-validator/dist/validator.min.js"></script>

		<script src="https://cdnjs.cloudflare.com/ajax/libs/spin.js/2.3.2/spin.min.js"></script>

		<!-- Stili e Script -->
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/functions.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/admin-onload.js"></script>

		<link rel="stylesheet" href="<?php echo ROOT_PATH; ?>plugins/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo ROOT_PATH; ?>css/percircle.css">
		<link rel="stylesheet" href="<?php echo ROOT_PATH; ?>css/admin.css">
		<?php
		include(ROOT_DIR."js/variabili.php");
		?>

		<link href="https://fonts.googleapis.com/css?family=Fira+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">

	</head>
	<body>
		<nav class="navbar navbar-toggleable-md">
			<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#panelMenu" aria-controls="panelMenu" aria-expanded="false" aria-label="Toggle navigation">
			  <span class="navbar-toggler-icon"></span>
			</button>
			<a class="navbar-brand" href="<?php echo ADMIN_PATH; ?>">Banner Manager</a>

			<?php if(!empty($par2) && is_numeric($par2)){ ?>
			<div class="collapse navbar-collapse" id="panelMenu">
				<ul class="navbar-nav">
					<!--
					<li class="nav-item active">
						<a class="nav-link <?php if($par3 == "preview"){ echo "disabled"; } ?>" href="#resize" data-panel="resize">Resize</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#template" data-panel="template">Template</a>
					</li>
					<li class="nav-item">
						<a class="nav-link disabled" href="#palette" data-panel="palette">Palette</a>
					</li>
					<li class="nav-item">
						<a class="nav-link disabled" href="#background" data-panel="background">Background</a>
					</li>
					<li class="nav-item">
						<a class="nav-link disabled" href="#text" data-panel="text">Text</a>
					</li>
			-->
					<li class="nav-item">
						<a class="nav-link btn btn-secondary" href="javascript:void(0)" data-fancybox data-type="ajax" data-src="<?php echo ROOT_PATH ?>ajax/export-recap.php?bannerId=<?php echo $objCampagna->getIdentAlfanumeric(); ?>&type=<?php # echo $objBanner->getType(); ?>" id="export-banner-set" data-panel="export">Export <?php if(isset($objBannerCampaign)){ ?><span class="countBanner"><?php echo $objBannerCampaign->getCountBanner() ?></span><?php } ?></a>
					</li>
				</ul>
				<label class="switch">
				    <input type="checkbox" class="switch-input" <?php if($par3 == "preview"){ echo "checked"; } ?>>
				    <span class="switch-trough switchurl" data-on="Prev" data-off="Edit" data-url-on="<?php echo ADMIN_PATH.($par2?$par2:"1")."/preview" ?>" data-url-off="<?php echo ADMIN_PATH.($par2?$par2:"1")."/" ?>"></span>
				    <span class="switch-handle"></span>
				</label>
				<!--<ul class="navbar-nav">
					<li class="nav-item">
						<a class="nav-link" href="">Edit</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?php echo ADMIN_PATH.($par2?$par2:"1")."/preview" ?>">Preview</a>
					</li>
				</ul>-->
			</div>
			<?php } ?>
		</nav>


		<div id="content"><?php
			if(isset($subpage)){
				include(ROOT_DIR."templates/".$page."-".$subpage.".php");
			}else{
				include(ROOT_DIR."templates/".$page.".php");
			}

		?></div>

	</body>
</html>
