function deleteProjectItem(colItem){
	dataType = colItem.attr('data-type');
	dataId = colItem.attr('data-id');
	dataForm = { 
			idBanner: dataId,
			type: dataType,
			action: 'delete'
	}
	
	urlAjax = ROOT_PATH+'ajax/banner.php';
	
	$.ajax({url: urlAjax, type:"POST", data: dataForm,
		success: function(){
			colItem.remove();
		}
	});
}
function insertNewProject(dataForm){
	urlAjax = ROOT_PATH+'ajax/banner.php';
	$.ajax({url: urlAjax, type:"POST", data: dataForm,
		success: function(idBanner){
			//$("#idBanner").val(idBanner);
			window.location.href = ROOT_PATH+"admin/"+idBanner;
			//console.log(idBanner);
		}
	});
}
function saveSingleBanner(idBanner,idFormato,w,h){
	if(idFormato>0){
		dataForm = { 
				idBanner: idBanner,
				idFormato: idFormato,
				isCustom: '0',
				type: 'banner',
				action: 'save-single'
		}
	}
	else { 
		dataForm = { 
				idBanner: idBanner,
				isCustom: '1',
				width: w,
				height: h,
				type: 'banner',
				action: 'save-single'
		}
	}
	$.ajax({url: urlAjax, type:"POST", data: dataForm,
		success: function(idBanner){
			$("#idBanner").val(idBanner);
		}
	});
}
function saveBanner(idBanner,idTemp){
	urlAjax = ROOT_PATH+'ajax/banner.php';
	
	dataForm = { 
			idBanner: idBanner,
			idTemp: idTemp,
			type: 'campaign',
			action: 'save-single'
	}
	
	$.ajax({url: urlAjax, type:"POST", data: dataForm,
		success: function(idBanner){

		}
	});
}
function deleteBanner(selectedItem){
	urlAjax = ROOT_PATH+'ajax/banner.php';
	
	dataId = selectedItem.attr('data-id');
	
	dataForm = { 
			idBanner: dataId,
			type: 'campaign',
			action: 'delete-single'
	}
	
	$.ajax({url: urlAjax, type:"POST", data: dataForm,
		success: function(){
			selectedItem.remove();
		}
	});
}
function savePreview(idBanner, idPreview, urlPreview, data){
	dataForm = {idBanner:  idBanner, idPreview: idPreview, url: urlPreview, html : data };
	
	$.ajax({url: urlAjax, type:"POST", data: dataForm,
		success: function(previewId){
			$("#previewId").val(previewId);
		}
	});
}
function addNewFormatRow(){
	newRow = $("#formatTabForm .row").first().clone();
	newRow.find("input").each(function(){
		$(this).removeAttr('checked');
		$(this).val("");
	})
	newRow.appendTo('#formatTabForm');
	newRow.find('input:first').focus();
}
function updatePerCircle(){
	var rotationMultiplier = 3.6;
	// For each div that its id ends with "circle", do the following.
	$( "div[id$='circle']" ).each(function() {
		// Save all of its classes in an array.
		var classList = $( this ).attr('class').split(/\s+/);
		// Iterate over the array
		for (var i = 0; i < classList.length; i++) {
		   /* If there's about a percentage class, take the actual percentage and apply the
				css transformations in all occurences of the specified percentage class,
				even for the divs without an id ending with "circle" */
		   if (classList[i].match("^p")) {
			var rotationPercentage = classList[i].substring(1, classList[i].length);
			var rotationDegrees = rotationMultiplier*rotationPercentage;
			$('.c100.p'+rotationPercentage+ ' .bar').css({
			  '-webkit-transform' : 'rotate(' + rotationDegrees + 'deg)',
			  '-moz-transform'    : 'rotate(' + rotationDegrees + 'deg)',
			  '-ms-transform'     : 'rotate(' + rotationDegrees + 'deg)',
			  '-o-transform'      : 'rotate(' + rotationDegrees + 'deg)',
			  'transform'         : 'rotate(' + rotationDegrees + 'deg)'
			});
		   }
		}
	});
}
function updateQueryStringParameter(uri, key, value) {
	  var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	  var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	  if (uri.match(re)) {
	    return uri.replace(re, '$1' + key + "=" + value + '$2');
	  }
	  else {
	    return uri + separator + key + "=" + value;
	  }
}
function copyTextToClipboard(text) {
	  var textArea = document.createElement("textarea");

	  //
	  // *** This styling is an extra step which is likely not required. ***
	  //
	  // Why is it here? To ensure:
	  // 1. the element is able to have focus and selection.
	  // 2. if element was to flash render it has minimal visual impact.
	  // 3. less flakyness with selection and copying which **might** occur if
	  //    the textarea element is not visible.
	  //
	  // The likelihood is the element won't even render, not even a flash,
	  // so some of these are just precautions. However in IE the element
	  // is visible whilst the popup box asking the user for permission for
	  // the web page to copy to the clipboard.
	  //

	  // Place in top-left corner of screen regardless of scroll position.
	  textArea.style.position = 'fixed';
	  textArea.style.top = 0;
	  textArea.style.left = 0;

	  // Ensure it has a small width and height. Setting to 1px / 1em
	  // doesn't work as this gives a negative w/h on some browsers.
	  textArea.style.width = '2em';
	  textArea.style.height = '2em';

	  // We don't need padding, reducing the size if it does flash render.
	  textArea.style.padding = 0;

	  // Clean up any borders.
	  textArea.style.border = 'none';
	  textArea.style.outline = 'none';
	  textArea.style.boxShadow = 'none';

	  // Avoid flash of white box if rendered for any reason.
	  textArea.style.background = 'transparent';


	  textArea.value = text;

	  document.body.appendChild(textArea);

	  textArea.select();

	  try {
	    var successful = document.execCommand('copy');
	    var msg = successful ? 'successful' : 'unsuccessful';
	    console.log('Copying text command was ' + msg);
	  } catch (err) {
	    console.log('Oops, unable to copy');
	  }

	  document.body.removeChild(textArea);
}

function updateTemplateSize(newW,newH){
	var newSrc = $("#iframe").attr("src");
	//newSrc = newSrc.substr(0, newSrc.lastIndexOf("/")+1) + newW + "x" + newH;
	//$("#iframe").attr("src", null);
	$("#iframe").attr("width", newW + "px");
	$("#iframe").attr("height", newH + "px");
	//$("#iframe").attr("src", newSrc);
	$("#preview #bannerWidth").val(newW);
	$("#preview #bannerHeight").val(newH);
}
