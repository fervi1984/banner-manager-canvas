<?php
require_once (str_replace("js", "", dirname(__FILE__)) . "init.php");
require_once ( ROOT_DIR. "plugins/minify/vendor/autoload.php" );
require_once ( ROOT_DIR. "classes/Template.class.php");
require_once ( ROOT_DIR. "classes/Campagna.class.php");
use MatthiasMullie\Minify;

$version = "1.2.0";
if(isset($_GET["v"])){
    $version = $_GET["v"];
    unset($_GET["v"]);
}

$debug = false;
if(isset($_GET["debug"]) && $_GET["debug"] == true){
    $debug = true;
    unset($_GET["debug"]);
}
$debug = true;
if(!file_exists(__DIR__ . "/canvasBanner/$version")){
    echo "versione inesistente --> $version";
    exit(0);
}

$Directory = new RecursiveDirectoryIterator(__DIR__ . "/canvasBanner/$version");
$Iterator = new RecursiveIteratorIterator($Directory);
$Regex = new RegexIterator($Iterator, '/^.+\.js$/i', RecursiveRegexIterator::GET_MATCH);

$minifier = new Minify\JS();
$contet = "";
$files = iterator_to_array($Regex);
ksort($files);
foreach($files as $path => $object){
    $contet.= file_get_contents($path);
    $minifier->add($path);    
}

$classiAggiuntive = array();
foreach($classiAggiuntive as $name){
    //$contet.= file_get_contents($name);
    $minifier->add($name);
}


//$template = new Template($connection);
$json = "{}";

$camp = false;
$template = false;
$bannerWidth = "false";
$bannerHeight = "false";

$bannerAlphaId = isset($_GET["banner"]) && strlen($_GET["banner"])>0 ? $_GET["banner"] : false;
$campagnaId = isset($_GET["camp"]) && strlen($_GET["camp"])>0 ? $_GET["camp"] : false;
if($bannerAlphaId !== false){
	$banner = new Banner($connection);
	if($banner->initByAlpha($bannerAlphaId)){
		$camp = $banner->getObjCampagna();
		$template = $banner->getTemplateObj();
		if(!$camp){
			print_r("console.log('banner non associato a una campagna');");
			exit(0);
		}
		
		if(!$template){
			print_r("console.log('Campagna senza template');");
			exit(0);
		}
		
		$bannerWidth = $banner->getWidth();
		$bannerHeight = $banner->getHeight();
		
	}else{
		print_r("console.log('banner non trovato');");
		exit(0);
	}
}elseif($campagnaId !== false){
	$camp = new Campagna($connection);
	if($camp->initByAlpha($campagnaId)){
		if(isset($_GET["template"])){
			$template = new Template($connection);
			if($template->init($_GET["template"])){
				
			}else{
				print_r("console.log('Template inesistente');");
				exit(0);
			}
		}else{
			$template = $camp->getTemplateObj();
		}
		
		if(isset($_GET["w"])){
			$bannerWidth = intval($_GET["w"]);
		}
		
		if(isset($_GET["h"])){
			$bannerHeight = intval($_GET["h"]);
		}
	}else{
		print_r("console.log('Campagna inesistente');");
		exit(0);
	}
	
	
}else{
	print_r("console.log('configurazione inesitente');");
	exit(0);
}

$data = $camp->getVariables("json");

//if(isset($_GET["template"]) && $template->init($_GET["template"])){
//}

$json = $template->getJsonStructure();
if(strlen($json)<=2){
	print_r("console.log('no template found');");
	exit(0);
}

if($debug){
	$toPrint = $contet;
}else{
	$toPrint = $minifier->minify();
}

$isTest = !empty($_GET["test"]) && $_GET["test"] == "1" ? "true" : "false";

$toPrint = str_replace("PHPmainTemplate",$json, $toPrint);
$toPrint = str_replace("PHPVariable",json_encode($data), $toPrint);
$toPrint = str_replace("PHPWidth",$bannerWidth, $toPrint);
$toPrint = str_replace("PHPHeight",$bannerHeight, $toPrint);
$toPrint = str_replace("PHPIstest",$isTest, $toPrint);

echo $toPrint;