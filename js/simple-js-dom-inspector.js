/**
 * Simple JavaScript DOM Inspector v0.1.2
 *
 * Highlights hovered elements with a 2px red outline, and then provides the hovered element
 * on click to the callback function, which can do anything with it.
 *
 * By default, the inspector just console.logs a jQuery-style CSS selector path for the element.
 * 
 * The CSS path-building code works up the given element's parent nodes to create an optimised
 * selector string, stopping at the first parent with an #id, but can also create an
 * ultra-specific full CSS path, right down to 'html'.
 * 
 * Optionally, it can check to see if any part of the CSS path matches multiple elements, or if
 * any element in the CSS path has no #id or .class, and add specific ":nth-child()"
 * pseudo-selector to match the element, eg. "html body #content p img:nth-child(1)"
 * 
 * Hit escape key to cancel the inspector.
 * 
 * NB: XPath code removed as it didn't really work very well, need to write from scratch.
 * 
 * Started putting in IE support, but won't work in IE just yet, check back next week for that
 * (so far, tested in FF4, Chrome, Safari, Opera 11.)
 * 
 * No warranty; probably won't break the internet. Improvements and linkbacks welcome!
 * 
 * - Joss
 */

(function(document) {
	
	var last;
	var offsetWindowTop = 32;
	var offsetWindowLeft = 7;
	var offsetWindowBottom = 25;
	var editTemplate = 
		"<div class='editTemplate'>" +
			"<div class='editContainer'>" +
				"<div class='headerEdit'>" +
					"<div class='glyphicon glyphicon-pencil btn-edit'></div><div class='btn-close'></div>" +
				"</div>" +
				"<div class='footerEdit'>" +
					"<div class='btn-action btn-undo'>UNDO</div><div class='btn-action btn-save'>SAVE</div>" +
				"</div>" +
			"</div>" +
		"</div>";
	
	var activeEditTemplate = "<div class='addBanner'><span>+</span></div>";
	
	function replaceBanner(element){
		var template = jQuery('#editor .template.selected', window.parent.document).attr('data-id');
		template = (template!="" && (typeof template!="undefined"))?template:'1';
		
		var bannerId = jQuery('#idAlfanumeric', window.parent.document).val();
		
		var placehold = '<iframe class="banner-rep" width="'+element.outerWidth()+'" height="'+element.outerHeight()+'" src="'+ROOT_PATH+'camp/'+bannerId+'/template/'+template+'"></iframe>'
		if(element.is("div,span"))
			element.html(placehold);
		else element.replaceWith(placehold);
	}
	
	function attachActiveEditEvent(){
		jQuery(".editTemplate .editContainer .addBanner").off().click(function(){
			replaceBanner(jQuery(".banner-sel").find('iframe.banner-rep'));
			jQuery('.addBanner').remove();
		})
	}
	
	function attachEditModeEvent(){
		jQuery(".btn-close,.btn-undo").off().on('click', function(){		
			jQuery(".editTemplate").remove();
			jQuery('.banner-sel').removeClass('banner-sel');
		});
		
		jQuery(".btn-edit").off().on('click', function(){
			if(jQuery('.addBanner').length == 0){
				jQuery(".editTemplate .editContainer").append(activeEditTemplate);
				attachActiveEditEvent();
			}
		});
	}

	/**
	 * MouseOver action for all elements on the page:
	 */
	function inspectorMouseOver(e) {
		// NB: this doesn't work in IE (needs fix):
		var element = jQuery(e.target);
		
		// Set outline:
		if(jQuery(".editTemplate")[0]){
			if(jQuery(".editTemplate").find(element).length == 0){
				e.target.style.outline = '2px solid #f00';
			}
		}
		else {
			e.target.style.outline = '2px solid #f00';
		}
		
		if(element.attr('href'))
			element.removeAttr('href');
		
		
		// Set last selected element so it can be 'deselected' on cancel.
		last = element;
	}
	
	
	/**
	 * MouseOut event action for all elements
	 */
	function inspectorMouseOut(e) {
		// Remove outline from element:
		var element = jQuery(e.target);
		e.target.style.outline = '';
		
		if(!!element){
			element.css('outline', 'none');
		}
	}
	
	
	/**
	 * Click action for hovered element
	 */
	function inspectorOnClick(e) {
		e.preventDefault();
		e.stopPropagation();
		
		var element = jQuery(e.target);

		if(jQuery(".editTemplate").length){
			if(jQuery(".editTemplate").find(element).length > 0){
				return true;
			}
		}
		
		if(!element.children("iframe").hasClass('banner-rep')){
			replaceBanner(element);
		} else {
			if(!element.hasClass('banner-sel') && element.children().hasClass('banner-rep')){
				iframeEl = element.children();
				offsetHeightWindow = offsetWindowTop + offsetWindowBottom;
				jQuery('.banner-sel').removeClass('banner-sel');
				element.addClass('banner-sel');
				jQuery(".editTemplate").remove();				
				element.append(editTemplate);
				jQuery(".editTemplate").css({
					 top: (iframeEl.position().top - offsetWindowTop) + "px",
					 left: (iframeEl.position().left - offsetWindowLeft) + "px",
					 width: iframeEl.width() + (offsetWindowLeft*2),
					 height: (iframeEl.height()+offsetHeightWindow)
				});
				attachEditModeEvent();
			}
			/*else if(!element.parent().hasClass('banner-sel') && element.hasClass('banner-rep')){
				$('.banner-sel').removeClass('banner-sel');
				element.parent().addClass('banner-sel');
				element.parent().append(editTemplate);
			}*/
		}
		
		return false;
	}


	/**
	 * Function to cancel inspector:
	 */
	function inspectorCancel(e) {
		// Unbind inspector mouse and click events:
		if (e === null && event.keyCode === 27) { // IE (won't work yet):
			document.detachEvent("mouseover", inspectorMouseOver);
			document.detachEvent("mouseout", inspectorMouseOut);
			document.detachEvent("click", inspectorOnClick);
			document.detachEvent("keydown", inspectorCancel);
			last.style.outlineStyle = 'none';
		} else if(e.which === 27) { // Better browsers:
			document.removeEventListener("mouseover", inspectorMouseOver, true);
			document.removeEventListener("mouseout", inspectorMouseOut, true);
			document.removeEventListener("click", inspectorOnClick, true);
			document.removeEventListener("keydown", inspectorCancel, true);
			
			// Remove outline on last-selected element:
			last.style.outline = 'none';
		}
	}
	

	
	if ( document.addEventListener ) {
		document.addEventListener("mouseover", inspectorMouseOver, true);
		document.addEventListener("mouseout", inspectorMouseOut, true);
		document.addEventListener("click", inspectorOnClick, true);
		document.addEventListener("keydown", inspectorCancel, true);
	} else if ( document.attachEvent ) {
		document.attachEvent("mouseover", inspectorMouseOver);
		document.attachEvent("mouseout", inspectorMouseOut);
		document.attachEvent("click", inspectorOnClick);
		document.attachEvent("keydown", inspectorCancel);
	}
	
})(document);