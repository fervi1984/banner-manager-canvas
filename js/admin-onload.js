var bannerLink;
var opts = {
		  lines: 17 // The number of lines to draw
		, length: 9 // The length of each line
		, width: 2 // The line thickness
		, radius: 23 // The radius of the inner circle
		, scale: 1 // Scales overall size of the spinner
		, corners: 1 // Corner roundness (0..1)
		, color: '#fff' // #rgb or #rrggbb or array of colors
		, opacity: 0.25 // Opacity of the lines
		, rotate: 0 // The rotation offset
		, direction: 1 // 1: clockwise, -1: counterclockwise
		, speed: 1 // Rounds per second
		, trail: 60 // Afterglow percentage
		, fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
		, zIndex: 2e9 // The z-index (defaults to 2000000000)
		, className: 'spinner' // The CSS class to assign to the spinner
		, top: '50%' // Top position relative to parent
		, left: '50%' // Left position relative to parent
		, shadow: false // Whether to render a shadow
		, hwaccel: false // Whether to use hardware acceleration
		, position: 'absolute' // Element positioning
		}

$(function(){
	//needToConfirm = true;
	//spinner
	
	/*$(window).on('load', function(){
		setTimeout(function(){
			$('#loading-indicator').fadeOut().remove();
		},1000)
		
	});*/
	
	$(document).ajaxSend(function(event, request, settings) {
		$('body').append("<div id='loading-indicator'></div>");
		
		
		var target = $("#loading-indicator")[0];
		var spinner = new Spinner(opts).spin(target);
	    
		$('#loading-indicator').show();
	});

	$(document).ajaxComplete(function(event, request, settings) {
	    $('#loading-indicator').fadeOut().remove();
	});

	$(".switchurl").parent().click(function(){
		//console.log("click");
		var $this = $(this);
		setTimeout(function(){
			if($this.find("input").prop('checked')){
				console.log("on");
				window.location.href = $this.find(".switchurl").attr("data-url-on");
			}else{
				console.log("off");
				window.location.href = $this.find(".switchurl").attr("data-url-off");
			}
		},500);
	});


	$("#panelMenu a").click(function(e){

		if(!$(this).hasClass("disabled")) {
			if($(this).attr('href').indexOf('#')=="-1"){
				return true;
			}else{
				//e.preventDefault();
				var panel = $(this).attr("data-panel");

				$("#editor .editor-single").removeClass("active");
				setTimeout(function(){
					$("#editor .editor-single[data-panel=\"" + panel + "\"]").addClass("active");
				},100);
			}
		}

	});
	
	$("#preview iframe").on('load',function(){
		
		var iframe = $("#preview iframe").contents();
		
	    $(iframe).scroll(function (e) { 
	    	e.preventDefault();
			return false;
	    });
	    
	    $(iframe).find('a').each(function(){
	    	$(this).attr("href", "javascript:void(0)");
	    });
	    
	    setTimeout(function(){
	    	$(iframe).find('#loading-indicator').remove();
	    }, 150);
	    	
	});

	//quando premo invio sulla barra dell'url nella pagina preview aggiorno l'url dell iframe per caricare il sito
	$("#urlPage").off().on('keypress', function (e) {
		 var key = e.which;
		 if(key == 13)  // the enter key code
		  {
			 $.needToConfirm = false; // sblocco refresh unload
			 
			 iframeUrl = $("#preview iframe").attr('src');
			 
			 if(iframeUrl=='about:blank')
				 iframeUrl=ROOT_PATH+"templates/banner-site-sim.php"; //aggiorno l'url del frame se provengo da uno stato di openn-preview
			 
			 updatedUrl = updateQueryStringParameter(iframeUrl,"mainUrl",$(this).val());
			 $("#preview iframe").attr('src', updatedUrl);
			 $.needToConfirm = true; // blocco refresh unload
		  }
		 $("#previewId").val(""); //non essendo per certo una preview salvata svuoto il campo preview id
		 
	});

	$("#salvaCustom").click(function(){
		updateTemplateSize($("[name=\"customWidth\"]").val(), $("[name=\"customHeight\"]").val());
		$("#idFormato").val("");
	});

	$(".template").click(function(){
		$(".template").removeClass('selected');
		$(this).addClass('selected');
		$("#iframe").attr("src", bannerLink + $(this).attr("data-id") + "/0x0");
		$("#preview #bannerIdTemp").val($(this).attr("data-id"));
	});
	
	$(".export").click(function(){
		$.ajax({
            url: ROOT_PATH+'ajax/export.php',
            success: function(data){
                window.location.href = ROOT_PATH+"image.png";
            }
        });
	});
	
	$(".share .share-item").off().on('click',function(){
		var actionMenu = $(this).attr("data-action");
		var idBanner = $("#bannerId").val();
		var idPreview = $("#previewId").val();
		
		switch(actionMenu){
			case 'save-preview':
				data = $("#preview iframe").contents().find("html").html();
				//data = $(data).not('#preview-js-injection');
				urlAjax =  ROOT_PATH+'ajax/actionMenuPrev.php?type='+actionMenu;
				urlPreview = $("#urlPage").val();
				
				savePreview(idBanner,idPreview,urlPreview, data);
				
				break;
			case 'copy-url':
				copyTextToClipboard(ROOT_PATH+"preview/"+idPreview);
				break;
		}
	});
	
	$(".huge-icon-container .inner-wrapper").off().click(function(e){
		e.preventDefault();
		window.location.href = $(this).find('a').attr('href');
	});
	
	$('#bgBannerPicker').colorpicker();
	$('#colorTextPicker').colorpicker();
	$('#bgTextPicker').colorpicker();
	
	
	$("form .carousel-control-next").on('click',function(){
		$(this).closest("form[data-toggle=validator]").validator('validate');
		if($(this).closest("form[data-toggle=validator]").find('.has-error').length > 0){
			return false;
		} else {
			return true;
		}		
	});

	$(".insert-new-container #save").off().click(function(){
		
		$(this).closest("form[data-toggle=validator]").validator('validate');
		
		idCampagna = $("#idCampagna").val();
		idBanner = $("#idBanner").val();
		//idCamp = ($("#idCamp").length)?$("#idCamp").val():'';
		idTemp = ($("#idTemp").length)?$("#idTemp").val():'';
		projectType =$('#projectType').val();
		campaignName = $('#campaignName').val();
		width = '';
		height = '';
		bgBanner= $('#bgBanner').val();
		headText = $('#headText').val();
		subHeadText = $('#subHeadText').val();
		colorText = $('#colorText').val();
		bgText = $('#bgText').val();
		webSiteText = $('#webSiteText').val();
		webSiteUrl = $('#webSiteLink').val();
		ctaText = $('#ctaText').val();
		ctaUrl = $('#ctaLink').val();
		fileBanner = (($("#files-bannerUp").find('a').length>0)? $("#files-bannerUp").find('a').attr('href'): $('#bannerUp').attr('value'));
		fileLogo = (($("#files-logoUp").find('a').length>0)? $("#files-logoUp").find('a').attr('href') : $('#logoUp').attr('value'));
		coordMainAreaBanner = $("#coordMainAreaBanner").val();
		coordMainAreaLogo = $("#coordMainAreaLogo").val();		
		
		formatList = [];
		//cssStyle = '';
		//json = '';
		$("#formatTabForm .row").each(function(){
			formatCheck = $(this).find('.formatSelected').is(':checked')
			if(formatCheck){
				formatId = ($(this).find('.formatId').length)?$(this).find('.formatId').val():'';
				formatWidth = $(this).find('.formatWidth').val();
				formatHeight = $(this).find('.formatHeight').val();
				formatName = $(this).find('.formatName').val();
				
				formatList.push({ idBanner: formatId, width: formatWidth, height: formatHeight, name: formatName });
			}
		});
		
		
		dataForm = { 
				idCampagna: idCampagna,
				idBanner: idBanner,
				idTemp: idTemp,
				type: projectType,
				campaignName: campaignName,
				width: width,
				height: height,
				bgBanner: bgBanner,
				headText: headText,
				subHeadText: subHeadText,
				colorText: colorText,
				bgText: bgText,
				webSiteText: webSiteText,
				webSiteUrl: webSiteUrl,
				ctaText: ctaText,
				ctaUrl: ctaUrl,
				fileBanner: fileBanner,
				fileLogo: fileLogo,
				coordMainAreaBanner: coordMainAreaBanner,
				coordMainAreaLogo: coordMainAreaLogo,
				formatList: formatList,
				action: 'save'
				//json: json,
				//cssStyle: cssStyle
		}
		
		
		if($(this).closest("form[data-toggle=validator]").find('.has-error').length==0){
			insertNewProject(dataForm);
		}
	})
	
	//tab format campagin input
	$("#formatTabForm").off().on('keyup', 'input', function( e ) {
	    if( e.which == 9 ) {
	        nextInput =  $(":input")[$(":input").index(document.activeElement)];
	        nextInput.focus();
	        if($(nextInput).is(':checkbox')){
	        	$(nextInput).attr('checked',true);
	        	addNewFormatRow();
	        }
	        else if ($(nextInput).hasClass('formatName')){
	        	thisRow = $(this).closest('.row');
	        	formatWidth = thisRow.find('.formatWidth').val();
	        	formatHeight = thisRow.find('.formatHeight').val();
	        	
	        	urlAjax = ROOT_PATH+'ajax/bannerFormat.php';
	        	dataForm = {
	        			formatWidth: formatWidth,
	        			formatHeight: formatHeight
	        	}
	        	
	        	$.ajax({url: urlAjax, type:"POST", data: dataForm,
	        		success: function(formatName){
	        			if(formatName!='')
	    	        		$(nextInput).val(formatName);
	        		}
	        	});
	        	
	        }
	    }
	} );

	$("#addNewFormat").off().on('click',function(){
		addNewFormatRow();
	});
	
	$("#save-single-banner").off().on('click',function(){
		urlAjax = ROOT_PATH+'ajax/banner.php';
		idBanner = $("#bannerId").val();
		idFormato = $("#idFormato").val();
		width = $("#bannerWidth").val();
		height = $("#bannerHeight").val();
		saveSingleBanner(idBanner,idFormato,width,height);
	});
	
	$(".pCol-md-3 .delete-item").off().on('click', function(e){
		e.preventDefault();
		if (window.confirm("Do you really want to delete this item?")) {
			colItem = $(this).closest(".pCol-md-3");
			deleteProjectItem(colItem);
		}
		
		return false;
		
	});
	
	$("#edit-campaign-page #delete-banner").off().on('click',function(e){
		e.preventDefault();
		if (window.confirm("Do you really want to delete this item?")) {
			selectedItem = $('#formats .formato.selected');
			deleteBanner(selectedItem);
		}
	});
	
	$("#edit-banner-page .formato").click(function(){
		updateTemplateSize($(this).attr("data-width"), $(this).attr("data-height"));
		$("#idFormato").val($(this).attr('data-id'));
	});
	
	$("#edit-campaign-page .formato").click(function(){
		$("#formats .formato").removeClass('selected');
		$('#formats .formato:not(.modified) .fa-pencil').addClass('sr-only');
		$(this).addClass('selected');
		updateTemplateSize($(this).attr("data-width"), $(this).attr("data-height"));
	});
	
	$("#edit-campaign-page #edit-banner").off().on('click',function(e){
		e.preventDefault();
		
		$('#formats .formato.selected').find('.fa-pencil').removeClass('sr-only');
		$('#formats .formato').removeClass('editing');
		$('#formats .formato.selected').addClass('editing');
	});
	
	//aggiungere input su idTemp in modo che sia sempre valorizzato
	$("#edit-campaign-page #save-banner").off().on('click',function(e){
		e.preventDefault();
		$('#formats .formato.selected').removeClass('editing');
		$('#formats .formato.selected').addClass('modified');
		
		idBanner = $('#formats .formato.selected').attr('data-id');
		idTemp = $("#editor .template.selected").attr('data-id');
		
		saveBanner(idBanner, idTemp);
		
	});
	
	$(".form-group .files a").click(function(e){
		e.preventDefault();
		
		var fileUploadID = $(this).closest('.files').attr('id');
		fileUploadID = fileUploadID.substring(fileUploadID.lastIndexOf("-") + 1, fileUploadID.length);
		if($("#modalareaselect").length > 0){
            $("#modalareaselect .modal-body").html("<img id='uploadedImg' width='100%' src='"+$(this).attr('href')+"' />");
	        $('#modalareaselect').modal('show');
	        
	        switch(fileUploadID){
	    		case 'bannerUp':
	    			 var coordArea = $.parseJSON($("#coordMainAreaBanner").val());
	    		break;		    	        		
	    		case 'logoUp':
	    			 var coordArea = $.parseJSON($("#coordMainAreaLogo").val());
	    		break;
	    	}
	        
	       
	        
	        var x1=0, y1=0, x2=0, y2=0;
	        var windowWidth,originWidth,windowHeight,originHeight,propWidth,propHeight;
	        
	       
	        var ias = $('img#uploadedImg').imgAreaSelect({
	        	instance: true,
	            handles: true,
	            show: true,
	            x1 : coordArea.x1, y1 : coordArea.y1, x2 : coordArea.x2, y2: coordArea.y2,
	            onSelectEnd: function (img, selection) {
	            	windowWidth = $(img).width();
	            	originWidth =$(img).prop("naturalWidth");
	            	windowHeight = $(img).height();
	            	originHeight =$(img).prop("naturalHeight");
	            	
	            	propWidth = originWidth / windowWidth;
	            	propHeight = originHeight / windowHeight;
	            	
	            	x1 =  Math.ceil(selection.x1 * propWidth);
	            	y1 =  Math.ceil(selection.y1 * propHeight);
	            	x2 =  Math.ceil(selection.x2 * propWidth);
	            	y2 =  Math.ceil(selection.y2 * propHeight);
	            }
	        });
	        
	       /*var edit_box = $('img#uploadedImg').imgAreaSelect({ instance: true });
            //give the coordinates here
           edit_box.setSelection(coordArea.x1, coordArea.y1, coordArea.x2, coordArea.y2, true);
           edit_box.setOptions({ show: true });
           edit_box.update();*/
	        
	        $('#modalareaselect .btn-primary').off().on('click', function(){
	        	var urlAjax =  ROOT_PATH+'ajax/banner.php';
	        	var typeFile = fileUploadID;
	        	
	        	switch(typeFile){
	        		case 'bannerUp':
	        			$("#coordMainAreaBanner").val('{"x1":'+x1+',"x2":'+x2+',"y1":'+y1+',"y2":'+y2+'}');
	        		break;		    	        		
	        		case 'logoUp':
	        			$("#coordMainAreaLogo").val('{"x1":'+x1+',"x2":'+x2+',"y1":'+y1+',"y2":'+y2+'}');
	        		break;
	        	}
	        	
	        	$('#modalareaselect .btn-secondary').click();
	        	ias.cancelSelection();
	        });
        }
		
		return false;
	});
});
