class LayerImageLogo extends Layer {
    constructor(...args) {        
        super(...args);
        
        this._ready = false;
        
        this._orizzontalAlign = "center";
        this._varticalAlign = "middle";
        
        this._orizImage = null;
        this._vertImage = null;
        this._imgElement = null;
        
        this._resolve = null;
        this._reject = null;
        this.className = "LayerImageLogo";
    }
    
    setOptions(obj){
        
        for(let i in obj){
            if(true){ //mi da errore l'editor se non metto un if
                switch(i){
                    case "urlV":
                        if(obj[i].url !== false)
                            this.setVerticalImage(obj[i].url);
                        break;
                    case "urlH":
                        if(obj[i].url !== false)
                            this.setOrizontaleImage(obj[i].url);
                        break;
                    case "align":
                        this.setOrizzontalAlign(obj[i]);
                        break;
                    case "valign":
                        this.setVerticalAlign(obj[i]);
                        break;
                }
            }
        }
        
    }
    
    setVerticalImage(image){
        this._vertImage = image;
        if(this._orizImage === null){
            this.setOrizontaleImage(image);
        }
    }
    
    setOrizontaleImage(image){
        this._orizImage = image;
        if(this._vertImage === null){
            this.setVerticalImage(image);
        }
    }
 
    /*valori accettati left,center,right*/
    setOrizzontalAlign(align){
        this._orizzontalAlign = align;
    }
    
    /*valori accettati top,middle,bottom*/
    setVerticalAlign(align){
        this._varticalAlign = align;
    }
    
    prepareImages(){
        this._imgElement= document.createElement('img');
        var that = this;
        var imageData = (this.isLayoutVerticale() ?  this._orizImage : this._vertImage);
        if(imageData === null){
            
            console.log("non ci è immagine");
            return false;
        }
        this._imgElement.onload = function() {
            that._ready = true;
            that._resolve(1);
        };
        this._imgElement.src = 'data:image/svg+xml;base64,\n' + imageData;
        
    }
    
    drawMe(){
        if(!this._ready)
            return false;
        
        var x = this.getRealX();
        var y = this.getRealY();
        var ctx = this.getContext();
        
        if(this._backgroundColor && this._backgroundColor !== "transparent"){
            ctx.fillStyle = this._backgroundColor;;
            ctx.fillRect(x,y,this.width,this.height);
        }
        
        //crome mi streccia l'immagine e fa bene .. firefox no e se non sono presente le dimensioni nell'svg non lo disegna nemmeno
        var newHeight = this.height, newWidth = this.width;
        var imgRatio = this._imgElement.width/this._imgElement.height;
        //if(this.width > this.height){
            //quindi tocca prima l'altezza
            if(this.height*imgRatio <= this.width){
                newWidth = this.height*imgRatio;
            }else if(this.width/imgRatio <= this.height){
                newHeight = this.width/imgRatio;
            }else{
                console.log("non so che cosa fare");
            }
            
            //centro l'immagine
            var newX = this.x, newY = this.y;
            
            newX += (this.width - newWidth)/2;
            newY += (this.height - newHeight)/2;
                
        //}else{}
        
        ctx.drawImage(this._imgElement,newX,newY,newWidth,newHeight);
        
       
        ctx.save();
        ctx.restore();
        
        for(let i = 0; i < this._childs.length; i++){
            this._childs[i].drawMe("red");
        }
    }
    
    _internalPrepare(){
        var that = this;
        var p = new Promise(function(resolve,reject){
            
            that._resolve = resolve;
            that._reject = reject;
            that.prepareImages();
        });
        return p;
    }
    
}