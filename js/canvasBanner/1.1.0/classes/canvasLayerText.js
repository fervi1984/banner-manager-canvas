class LayerText extends Layer {
    constructor(...args) {        
        super(...args);
        
        this.font = "/fonts/Open_Sans/OpenSans-Regular.ttf";
        this.fontColor = "#000000";
        this.fontsSize = [20,50];
        this.text = ["undefined text"];
        this.lineaHeightMolt = 1;
        
        this._fontStep = 1;
        this._actualFont = 13;
        this._ciSta = false;
        this._actualTextIndex = 0;
        this._ready = false;
        this._lines = [];
        
        this._orizzontalAlign = "c";
        this._varticalAlign = "m";
        
        this._font = undefined;
        
        this._calculateRicorsive = true;
        this._resetValueOnCalculate = true;
        
        this._resolve = null;
        this._reject = null;
        this.className = "LayerText";
    }
    
    
    setFont(font){
        this.font = font;
    }
    
    
    setFontColor(color){
        this.fontColor = color;
    }
    
    setOptions(obj){
        super.setOptions(obj);
        for(let i in obj){
            if(true){ //mi da errore l'editor se non metto un if
                switch(i){
                    case "font":
                        this.setFont(obj[i]);
                        break;
                    case "color":
                        this.setFontColor(obj[i]);
                        break;
                    case "size":
                        this.setFontSizeMax(obj[i][1]);
                        this.setFontSizeMin(obj[i][0]);
                        break;
                    case "interlinea":
                        this.setLineaHeightMolt(obj[i]);
                        break;
                    case "align":
                        this.setOrizzontalAlign(obj[i]);
                        break;
                    case "valign":
                        this.setVerticalAlign(obj[i]);
                        break;
                    case "text":
                        this.setTexts(obj[i]);
                        break;
                }
            }
        }
        
    }
 
    /**
    * Setta il Valore di moltiplicazione della linea rispetto al suo font
    * @param {float} lineaHeight - a lineaHeight value e deve essere maggiore di 1.
    */
    setLineaHeightMolt(lineaHeight){
        this.lineaHeightMolt = lineaHeight < 1 ? 1 : lineaHeight;
    }
    
    /*valori accettati lcr => left,center,right*/
    setOrizzontalAlign(align){
        this._orizzontalAlign = align;
    }
    
    /*valori accettati tmb => top,middle,bottom*/
    setVerticalAlign(align){
        this._varticalAlign = align;
    }
    
    setFontSizeMax(max){
        this.fontsSize[1] = max;
    }
    
    setFontSizeMin(min){
        this.fontsSize[0] = min;
    }
    
    setTexts(testi){
        this.text = testi;
    }
    
    prepareForAnalisis(){
        this._actualTextIndex = 0;
        this._actualFont = this.fontsSize[1];
    }
    
    formatText(){
        if(this._resetValueOnCalculate){
            this.prepareForAnalisis();
        }
        
        if(this._font){
            this._calculateFontSpace();
        }else{
            this.loadFontName(this.font,this.fontLoadedCb.bind(this));
        }
    }
    
    fontLoadedCb(fontObj){
        this._font = fontObj;
        this._calculateFontSpace();
    }
    
    getTextTotHeight(){
        return this._lines.length * this._actualFont * this.lineaHeightMolt;
    }
    
    /**
     *canDecreseFontOrString([currentFont,[currentTextIndex]])
     *funzione che mi chiede se posso decrese il font o il testo secondo le regole pensate
     *
     *@param {int} currentFont - font attuale da verificare se si può decrescere
     *@param {int} currentTextIndex - indice del testo da utilizzare per capire se si può
     *@return {bool} - indica se posso decresere il font
     */
    canDecreseFontOrString(currentFont,currentTextIndex){
        var can = true;
        currentFont = typeof currentFont !== "undefined" ? currentFont : this._actualFont;
        currentTextIndex = typeof currentTextIndex !== "undefined" ? currentTextIndex : this._actualTextIndex;
        currentFont -= this._fontStep;
        
        if(currentFont<this.fontsSize[0]){
            currentTextIndex++;
            if(currentTextIndex>=this.text.length){
               can = false;
            }   
        }
        
        return can;
    }
    
    decreaseFontOrString(currentFont,currentTextIndex){
        currentFont = typeof currentFont !== "undefined" ? currentFont : this._actualFont;
        currentTextIndex = typeof currentTextIndex !== "undefined" ? currentTextIndex : this._actualTextIndex;
        
        //diminuisco il font di uno step
        currentFont -= this._fontStep;
        
        //se il font è + piccolo del minimo consentito allora cambio frase e risetto il font al massimo consentibile
        if(currentFont<this.fontsSize[0]){
            currentTextIndex++;
            currentFont = this.fontsSize[0];
        }
        
        this._actualFont = currentFont;
        this._actualTextIndex = currentTextIndex;
    }
    
    _calculateFontSpace(textIndex,fontSize,decrease){
        
        var calculateText = "";
        if(typeof textIndex !== "undefined"){
            if(this.text.length > textIndex){
                this._actualTextIndex = textIndex;
            }
        }
        
        
        if(typeof fontSize !== "undefined"){
            this._actualFont = fontSize;
        }
        
        
        if(typeof decrease !== "undefined" && decrease){
            
            if(this.canDecreseFontOrString()){
                this.decreaseFontOrString();
            }else{
                this._ready = true;
                this._ciSta = false;
                this._lines = [];
                if(this._reject){
                    this._reject(this);
                }
                
                return false;
            }
            
            
        }
        
        calculateText = this.text[this._actualTextIndex];
        var numberLine = 1;
        this._lines = [];
        
        var cars = calculateText.split("\n");
        var maxNumberLine = Math.floor(this.height/(this._actualFont*this.lineaHeightMolt));
        var maxWidth = this.width;
        
        //per ogni a capo
        for (var ii = 0; ii < cars.length; ii++,numberLine <= maxNumberLine) {
    
            var line = "";
            var words = cars[ii].split(" ");
            //per ogni a parola
            for (var n = 0; n < words.length; n++,numberLine <= maxNumberLine) {
                
                var testLine = line.length > 0 ? line + " " + words[n] : words[n];
                var testWidth = this._font.getAdvanceWidth(testLine, this._actualFont);
                
                if (testWidth > maxWidth) {
                    
                    var questaParola = words[n];
                    var testWidthSingle = this._font.getAdvanceWidth(questaParola, this._actualFont);
            
                    if(testWidthSingle > maxWidth){
                        //la singola parola non ci sta in linea da sola .. devo decrescere il tutto
                        if(this._calculateRicorsive){
                            return this._calculateFontSpace(this._actualTextIndex,this._actualFont,true);
                        }else{
                            
                            this._ready = true;
                            this._ciSta = false;
                            this._lines = [];
                            if(this._reject){
                                this._reject(this);
                            }
                            
                            return false;
                        }
                        
                        
                    }else if (numberLine <= maxNumberLine) {
                        this._lines.push(line);
                        line = words[n];
                    }else if(numberLine > maxNumberLine) {
                        //ci sono troppe linee
                        if(this._calculateRicorsive){
                            return this._calculateFontSpace(this._actualTextIndex,this._actualFont,true);
                        }else{
                            
                            this._ready = true;
                            this._ciSta = false;
                            this._lines = [];
                            if(this._reject){
                                this._reject(this);
                            }
                            
                            return false;
                        }
                    }
                    
                    numberLine++;
                }
                else {
                    line = testLine;
                }
            }
            
            if (numberLine <= maxNumberLine) {
                this._lines.push(line);
            }else{
                //ci sono troppe linee
                if(this._calculateRicorsive){
                    return this._calculateFontSpace(this._actualTextIndex,this._actualFont,true);
                }else{
                    
                    this._ready = true;
                    this._ciSta = false;
                    this._lines = [];
                    if(this._reject){
                        this._reject(this);
                    }
                    
                    return false;
                }
            }
            
            numberLine++;
        }
        
        
        
        this._ready = true;
        this._ciSta = true;
        if(this._resolve){
            this._resolve(1);
        }
        
    }
    
    drawMe(){
        
        if(!this._ready)
            return false;
        
        var x = this.getRealX();
        var y = this.getRealY();
        var ctx = this.getContext();
        
        ctx.fillStyle = this._backgroundColor;
        ctx.fillRect(x,y,this.width,this.height);
        
        if(this._ciSta){
            
            ctx.fillStyle = this.fontColor;
            //this._font.getPath()
            for(let i = 0; i < this._lines.length;i++){
                let lineaHeight = this._actualFont*this.lineaHeightMolt;
                let lineaWidth = this._font.getAdvanceWidth(this._lines[i], this._actualFont);
                let tx = x;
                let ty = y + ((i+1)*(lineaHeight));
                
                if(this._orizzontalAlign == "r"){
                    tx += this.width - lineaWidth;
                }else if(this._orizzontalAlign == "c"){
                    tx += (this.width - lineaWidth)/2;
                }
                
                
                if(this._varticalAlign == "m"){
                    ty += (this.height - this._lines.length*lineaHeight)/2;
                }else if(this._varticalAlign == "b"){
                    ty += this.height - this._lines.length*lineaHeight;
                }
                
                if(lineaHeight>this._actualFont){
                    ty -= ((lineaHeight - this._actualFont)/2);
                }
                
                let s = this._font.getPath(this._lines[i], tx, ty, this._actualFont);
                s.fill = this.fontColor;
                s.draw(ctx);
                
            }
            
        }
        
        //
       
        ctx.save();
        ctx.restore();
        
        for(let i = 0; i < this._childs.length; i++){
            this._childs[i].drawMe("red");
        }
    }
    
    _internalPrepare(){
        var that = this;
        var p = new Promise(function(resolve,reject){
            
            that._resolve = resolve;
            that._reject = reject;
            that.formatText();
        });
        return p;
    }
    
}