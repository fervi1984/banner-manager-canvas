class Layer {
    constructor(parent) {
       this.parent = parent;
       this.outBox = {width: 0, height: 0};
       this.priority = 0;
       this._origin = 1;
       this.x = 0;
       this.y = 0;
       this.width = 0;
       this.height = 0;
       this.name = new Date().getTime();
       
       this._backgroundColor = "transparent";
       this.padding = {top:0,right:0,bottom:0,left:0};
       this._childs = [];
       this._ready = true;
       this._callbackReady = null;
       
       this._cta = null;
       
       this.className = "Layer";
       this._layoutOrientation = null;
    }
    
    /**
     *getPosition
     *funzione che ritorna la posizione assoluta del blocco
     *
     *@return <json> posizione top/left del blocco
     */
    getPosition(){
        return {"x":this.x,"y":this.y};
    }
    
    
    /**
     *getSize
     *funzione che ritorna le dimensioni del blocco
     *
     *@return <json> dimensioni del blocco
     */
    getSize(){
        return {"width":this.width,"height":this.height};
    }
    
    /**
     *getBound
     *funzione che ritorna il confine del blocco
     *
     *@return <json> posizione top/left e dimensione del blocco
     */
    getBound(){
        return {
            "x":this.x,"y":this.y,"width":this.width,"height":this.height,"ref":this
        };
    }
    
    /**
     *getBounds
     *funzione che ritorna il confine del blocco e dei suoi figli il livello incrementato di 1
     *
     *@param int livelloPadre attuale del blocco
     *
     *@return <json> posizione top/left e dimensione del blocco
     */
    getBounds(livelloPadre){
        var j = this.getBound();
        j.livello = livelloPadre +1;
        if(this._childs.length > 0){
            j.childs = [];
            for(var i = 0; i < this._childs.length; i++){
                var cooretinate = this._childs[i].getBounds(j.livello);
                j.childs.push(cooretinate);
            }
        }
        return j;
    }
    
    loadFontName(fontName,cb){
        this.parent.loadFontName(fontName,cb);
    }
    
    getVariableWithName(variableName){
        return this.parent.getVariableWithName(variableName);
    }
    
    setName(name){
        this.name = name;
        return this;
    }
    
    setOrigin(o){
        this._origin = o;
    }
    
    setPriority(p){
        this.priority = p;
    }
    
    setBackgroundColor(color){
        this._backgroundColor = color;
    }
    
    getContext(){
        return this.parent.getContext();
    }

    setOptions(obj){
        if(obj.link){
            this._cta = obj.link;
        }
    }
    
    
    /**
     *setLayoutOrientation
     *funzione che setta il valore dell'orientamento del blocco attuale
     *
     *@param bool isVerticale indica se l'orientamento è verticale o orizzontale
     */
    setLayoutOrientation(isVerticale){
        this._layoutOrientation = isVerticale;
    }
    
    /**
     *isLayoutVerticale
     *funzione che mi ritorna il valore dell'orientamento del layout
     *
     *@returns {bool} ritorna vero se l'orientamento è verticale o false se non lo è
     */
    isLayoutVerticale(){
        if(this._layoutOrientation===null){
            this.calcOrientation();
        }
        return this._layoutOrientation;
    }
    
    calcOrientation(){
        this.setLayoutOrientation((this.height*this._origin / this.width)>1);
    }
    
    removeAll(){
        for(var i = 0; i < this._childs.length; i++){
            this._childs[i].removeAll();
            delete this._childs[i];
       }
    }
    
    drawMe(){
       
       var ctx = this.getContext();
       ctx.fillStyle = this._backgroundColor;
       
       ctx.fillRect(this.x,this.y,this.width,this.height);
       for(var i = 0; i < this._childs.length; i++){
            this._childs[i].drawMe();
       }
    }
    
    
    findMinorityReport(max){
        if(this.priority > max){
            max = this.priority;
        }
        
        for(let i = 0; i < this._childs.length; i++){
            max = this._childs[i].findMinorityReport(max);
        }
        
        return max;
    }
    
    setPadding(...args){
        switch(args[0].length){
            case 1:
                this.padding.top = args[0][0];
                this.padding.right = args[0][0];
                this.padding.bottom = args[0][0];
                this.padding.left = args[0][0];
                break;
            case 2:
                this.padding.top = args[0][0];
                this.padding.right = args[0][1];
                this.padding.bottom = args[0][0];
                this.padding.left = args[0][1];
                break;
            case 3:
                this.padding.top = args[0][0];
                this.padding.right = args[0][1];
                this.padding.bottom = args[0][2];
                this.padding.left = args[0][1];
                break;
            case 4:
                this.padding.top = args[0][0];
                this.padding.right = args[0][1];
                this.padding.bottom = args[0][2];
                this.padding.left = args[0][3];
                break;
        }
    }
    
    findChild(childName){
        if(this.name == childName){
            return this;
        }else{
            for(let i = 0; i < this._childs.length; i ++){
                var childFound = this._childs[i].findChild(childName);
                if(childFound){
                    return childFound;
                }
            }
        }
        
        return false;
    }

    addChild(...args){
        this.calcOrientation();
        var obj = null;
        
        
        switch(args[0].type){
            case "txt":
                obj = new LayerText(this);
                break;
            case "imageLogo":
                obj = new LayerImageLogo(this);
                break;
            case "image":
                obj = new LayerImage(this);
                break;
            case "multipleTxt":
                obj = new LayerTextMultiple(this);
                break;
            default:
                obj = new Layer(this);
                break;
        }
        
        
        if(args[0].bk){
            obj.setBackgroundColor(args[0].bk);
        }
        
        if(args[0].name){
            obj.setOptions(this.getVariableWithName(args[0].name));
        }
        
        if(args[0].padding){
            obj.setPadding(args[0].padding);
        }
        
        if(args[0].name){
            obj.setName(args[0].name);
        }
        
        if(args[0].priority){
            obj.setPriority(args[0].priority);
        }
        
        var height;
        var width;
        var pxHeight, pxWidth;
        var sommaParziale = typeof args[1] !== "undefined" ? args[1] : 100;
        
        if(this.isLayoutVerticale()){
            
            height = args[0]["v-size"];
            pxHeight = this.height * height / sommaParziale;
            pxWidth = this.width;
        }else{
            width = args[0]["h-size"];
            pxWidth = this.width * width / sommaParziale;
            pxHeight = this.height;
        }
        
        var pxPadTop = pxHeight*obj.padding.top/100;
        var pxPadLeft = pxWidth*obj.padding.left/100;
        var pxPadBottom = pxHeight*obj.padding.bottom/100;
        var pxPadRight = pxWidth*obj.padding.right/100;
        var x = pxPadLeft;
        var y = pxPadTop;
        
        //aggiungo sempre le reali posizini del padre con i padding compresi
        y += this.getRealY();
        x += this.getRealX();
        
        if(this.isLayoutVerticale()){
            for(let i = 0; i < this._childs.length; i++){
                y += this._childs[i].outBox.height;
            }
        }else{
            for(let i = 0; i < this._childs.length; i++){
                x += this._childs[i].outBox.width;
            }
        }
        
        obj.outBox = {width: pxWidth,height:pxHeight};
        obj.width = pxWidth - pxPadLeft - pxPadRight;
        obj.height = pxHeight - pxPadTop - pxPadBottom;
        obj.x = x;
        obj.y = y;
        
        this._childs.push(obj);
        
        if(args[0].origin){
            obj.setOrigin(args[0].origin);
        }
        
        
        if(args[0].blocks){
            var newWidth,newHeight;
            var somma = 0;
            for(let j = 0; j < args[0].blocks.length; j++){
                let anotherChild = args[0].blocks[j];
                var scatti = obj.height*obj._origin / obj.width;
                var variazione = 1;
                if (scatti > 1){ // se è verticale
                    variazione = anotherChild["v-size"] * (anotherChild["v-weight"] - 0) * (scatti - 1);
                    if (anotherChild["v-weight"] >= 1) {
                        newHeight = anotherChild["v-size"] + variazione;
                    }else{
                        newHeight = anotherChild["v-size"] - variazione;
                    }
                    if (newHeight < 0) {
                        newHeight = 0;
                    }
                    
                    anotherChild["v-size"] = newHeight;
                    somma += anotherChild["v-size"];
                    
                }else{
                    variazione = anotherChild["h-size"] * (anotherChild["h-weight"] - 0) * ((1/scatti)-1);
                    
                    if (anotherChild["h-weight"] >= 1) {
                        newWidth = anotherChild["h-size"] + variazione;
                    }else{
                        newWidth = anotherChild["h-size"] - variazione;
                    }
                    if (newWidth < 0) {
                        newWidth = 0;
                    }
                    
                    anotherChild["h-size"] = newWidth;
                    somma += anotherChild["h-size"];
                }
              
                
            }
           
            for(let i = 0; i < args[0].blocks.length; i++){
                obj.addChild(args[0].blocks[i],somma);
            }
        }
        
    }
    
    child(value){
        if(typeof value === 'number' && isFinite(value) && Math.floor(value) === value){
            if(value < this._childs.length){
                
                return this._childs[value];
            }
        }else{
            for(var i = 0; i < this._childs.length; i++){
                if(this._childs[i].name == value){
                    return this._childs[i];
                }
           }
        }
        
        return false;
    }
    
    getRealX(){
       return this.x;
    }

    getRealY(){
       return this.y;
    }

    redrawAll(){
        this.parent.redrawAll();
    }
    
    isReady(){
        var childReady = true;
        for(var i = 0; i < this._childs.length; i++){
            if(!this._childs[i].isReady()){
                childReady = false;
                break;
            }
       }
       return this._ready && childReady;
    }
    
    _internalPrepare(){
        var p = new Promise(function(resolve){resolve(1);});
        return p;
    }
    
    prepare(){
        var _promiseList = [this._internalPrepare()];
        //nella versione base questo è sempre pronto
        for(var i = 0; i < this._childs.length; i++){
            if(!this._childs[i].isReady()){
                _promiseList.push(this._childs[i].prepare());
            }
        }
        
        return Promise.all(_promiseList);
        
    }
    
 }