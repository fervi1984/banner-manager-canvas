class LayerImage extends Layer {
    constructor(...args) {        
        super(...args);
        
        this._ready = false;
        
        this._images = [];
        this._imagesElement = [];
        
        this._resolve = null;
        this._reject = null;
        this.className = "LayerImage";
    }
    
    setOptions(objs){
        var image = {};
        var obj = [];
        //codice temporaneo da sistemare probabilmente per multiple images
        if(!(objs instanceof Array)){
            
            obj.push(objs);
        }
        for(let j = 0; j< obj.length; j++){
            image = {
                        data : "",
                        x1 : 0,
                        y1 : 0,
                        x2: 0,
                        y2: 0,
						forceHeight: false,
                        posx: "c",
                        posy: "m",
                        mime : "",
                        ref : null
                    };
					
            for(let i in obj[j]){
                if(true){ //mi da errore l'editor se non metto un if
                    switch(i){
                        case "url":
                            if(obj[j][i].url)
                                image.data = obj[j][i].url;
                            if(obj[j][i].mime)
                                image.mime = obj[j][i].mime;
                            break;
                        case "x1":
                            image.x1 = obj[j][i];
                            break;
                         case "y1":
                            image.y1 = obj[j][i];
                            break;
                        case "y2":
                            image.y2 = obj[j][i];
                            break;
                        case "x2":
                            image.x2 = obj[j][i];
                            break;
                        case "posx":
                            image.posx = obj[j][i];
                            break;
                        case "posy":
                            image.posy = obj[j][i];
                            break;
                        case "mime":
                            image.mime = obj[j][i];
                            break;
                        case "ref":
                            image.ref = obj[j][i];
                            break;
						case "forceHeight":
                            image.forceHeight = obj[j][i];
                            break;
                    }
                }
            }
            this._images.push(image);
        }
    }
    
    
    /*valori accettati top,middle,bottom*/
    setVerticalAlign(align){
        this._varticalAlign = align;
    }
    
    prepareImages(){
        
        //prevedere + immagini
        for(var i = 0; i < this._images.length; i++){
            
            this._imagesElement.push(document.createElement('img'));
            var that = this;
            this._imagesElement[i].onload = function() {
                that._ready = true;
                that._resolve(1);
            };
            this._imagesElement[i].src = 'data:image/' + this._images[i].mime + ';base64,\n' + this._images[i].data;
            //this._imagesElement[i].src = "/assets/lexus/image_50.jpg";   
        }
    }
    
    drawMe(){
        if(!this._ready)
            return false;
        
        var x = this.getRealX();
        var y = this.getRealY();
        var ctx = this.getContext();
        
        ctx.fillStyle = this._backgroundColor;
        ctx.fillRect(x,y,this.width,this.height);
        for(let i = 0; i < this._imagesElement.length; i++){
            
            if(this._images[i].ref !== null){
                var p = this.findChild(this._images[i].ref);
                
                if(p){
                    var dTransform = this._datiImmagineDiRerimento(this._imagesElement[i],this._images[i],p);
                    var newX = Math.ceil(p.x + dTransform.left);
                    var newY = Math.ceil(p.y + dTransform.top);
                    var newWidth = Math.ceil(dTransform.width);
                    var newHeight = Math.ceil(dTransform.height);
                    var scale = Math.floor(newWidth/this._imagesElement[i].naturalWidth*100)/100;
                    var pScale = this._imagesElement[i];
                    if(scale<1){
                        pScale = this.downScaleImage(this._imagesElement[i],scale);
                    }
					
					ctx.drawImage(pScale,newX,newY,newWidth,newHeight);
                    pScale = null;
                    
                }else{
                    //disegno normalmente
					if (this._images[i].forceHeight) {
						ctx.drawImage(this._imagesElement[i],x,y,this._imagesElement[i].width,this._imagesElement[i].height,x,y,this.width,this._images[i].forceHeight);
					} else {
						ctx.drawImage(this._imagesElement[i],x,y,this._imagesElement[i].width,this._imagesElement[i].height,x,y,this.width,this.height);
					}
                    
                }
            }else{
                //disegno normalmente
                ctx.drawImage(this._imagesElement[i],x,y,this._imagesElement[i].width,this._imagesElement[i].height,x,y,this.width,this.height);
            }
            
        }
        
       
        ctx.save();
        ctx.restore();
        
        for(let i = 0; i < this._childs.length; i++){
            this._childs[i].drawMe("red");
        }
    }
    
    
    // scales the image by (float) scale < 1
    // returns a canvas containing the scaled image.
    downScaleImage(img, scale) {
        let imgCV = document.createElement('canvas');
        imgCV.width = img.width;
        imgCV.height = img.height;
        let imgCtx = imgCV.getContext('2d');
        imgCtx.drawImage(img, 0, 0);
        return this.downScaleCanvas(imgCV, scale);
    }
    
    // scales the canvas by (float) scale < 1
    // returns a new canvas containing the scaled image.
    downScaleCanvas(cv, scale) {
        if (!(scale < 1) || !(scale > 0)) throw ('scale must be a positive number <1 ');
        scale = this.normaliseScale(scale);
        let sqScale = scale * scale; // square scale =  area of a source pixel within target
        let sw = cv.width; // source image width
        let sh = cv.height; // source image height
        let tw = Math.floor(sw * scale); // target image width
        let th = Math.floor(sh * scale); // target image height
        let sx = 0, sy = 0, sIndex = 0; // source x,y, index within source array
        let tx = 0, ty = 0, yIndex = 0, tIndex = 0; // target x,y, x,y index within target array
        let tX = 0, tY = 0; // rounded tx, ty
        let w = 0, nw = 0, wx = 0, nwx = 0, wy = 0, nwy = 0; // weight / next weight x / y
        // weight is weight of current source point within target.
        // next weight is weight of current source point within next target's point.
        let crossX = false; // does scaled px cross its current px right border ?
        let crossY = false; // does scaled px cross its current px bottom border ?
        
        let sBuffer = cv.getContext('2d').
        getImageData(0, 0, sw, sh).data; // source buffer 8 bit rgba
        
        let tBuffer = new Float32Array(3 * tw * th); // target buffer Float32 rgb
        let sR = 0, sG = 0,  sB = 0; // source's current point r,g,b
    
        for (sy = 0; sy < sh; sy++) {
            ty = sy * scale; // y src position within target
            tY = 0 | ty;     // rounded : target pixel's y
            yIndex = 3 * tY * tw;  // line index within target array
            crossY = (tY !== (0 | ( ty + scale ))); 
            if (crossY) { // if pixel is crossing botton target pixel
                wy = (tY + 1 - ty); // weight of point within target pixel
                nwy = (ty + scale - tY - 1); // ... within y+1 target pixel
            }
            for (sx = 0; sx < sw; sx++, sIndex += 4) {
                tx = sx * scale; // x src position within target
                tX = 0 |  tx;    // rounded : target pixel's x
                tIndex = yIndex + tX * 3; // target pixel index within target array
                crossX = (tX !== (0 | (tx + scale)));
                if (crossX) { // if pixel is crossing target pixel's right
                    wx = (tX + 1 - tx); // weight of point within target pixel
                    nwx = (tx + scale - tX - 1); // ... within x+1 target pixel
                }
                sR = sBuffer[sIndex    ];   // retrieving r,g,b for curr src px.
                sG = sBuffer[sIndex + 1];
                sB = sBuffer[sIndex + 2];
                if (!crossX && !crossY) { // pixel does not cross
                    // just add components weighted by squared scale.
                    tBuffer[tIndex    ] += sR * sqScale;
                    tBuffer[tIndex + 1] += sG * sqScale;
                    tBuffer[tIndex + 2] += sB * sqScale;
                } else if (crossX && !crossY) { // cross on X only
                    w = wx * scale;
                    // add weighted component for current px
                    tBuffer[tIndex    ] += sR * w;
                    tBuffer[tIndex + 1] += sG * w;
                    tBuffer[tIndex + 2] += sB * w;
                    // add weighted component for next (tX+1) px                
                    nw = nwx * scale
                    tBuffer[tIndex + 3] += sR * nw;
                    tBuffer[tIndex + 4] += sG * nw;
                    tBuffer[tIndex + 5] += sB * nw;
                } else if (!crossX && crossY) { // cross on Y only
                    w = wy * scale;
                    // add weighted component for current px
                    tBuffer[tIndex    ] += sR * w;
                    tBuffer[tIndex + 1] += sG * w;
                    tBuffer[tIndex + 2] += sB * w;
                    // add weighted component for next (tY+1) px                
                    nw = nwy * scale
                    tBuffer[tIndex + 3 * tw    ] += sR * nw;
                    tBuffer[tIndex + 3 * tw + 1] += sG * nw;
                    tBuffer[tIndex + 3 * tw + 2] += sB * nw;
                } else { // crosses both x and y : four target points involved
                    // add weighted component for current px
                    w = wx * wy;
                    tBuffer[tIndex    ] += sR * w;
                    tBuffer[tIndex + 1] += sG * w;
                    tBuffer[tIndex + 2] += sB * w;
                    // for tX + 1; tY px
                    nw = nwx * wy;
                    tBuffer[tIndex + 3] += sR * nw;
                    tBuffer[tIndex + 4] += sG * nw;
                    tBuffer[tIndex + 5] += sB * nw;
                    // for tX ; tY + 1 px
                    nw = wx * nwy;
                    tBuffer[tIndex + 3 * tw    ] += sR * nw;
                    tBuffer[tIndex + 3 * tw + 1] += sG * nw;
                    tBuffer[tIndex + 3 * tw + 2] += sB * nw;
                    // for tX + 1 ; tY +1 px
                    nw = nwx * nwy;
                    tBuffer[tIndex + 3 * tw + 3] += sR * nw;
                    tBuffer[tIndex + 3 * tw + 4] += sG * nw;
                    tBuffer[tIndex + 3 * tw + 5] += sB * nw;
                }
            } // end for sx 
        } // end for sy
    
        // create result canvas
        let resCV = document.createElement('canvas');
        resCV.width = tw;
        resCV.height = th;
        let resCtx = resCV.getContext('2d');
        let imgRes = resCtx.getImageData(0, 0, tw, th);
        let tByteBuffer = imgRes.data;
        // convert float32 array into a UInt8Clamped Array
        let pxIndex = 0; //  
        for (sIndex = 0, tIndex = 0; pxIndex < tw * th; sIndex += 3, tIndex += 4, pxIndex++) {
            tByteBuffer[tIndex] = 0 | ( tBuffer[sIndex]);
            tByteBuffer[tIndex + 1] = 0 | (tBuffer[sIndex + 1]);
            tByteBuffer[tIndex + 2] = 0 | (tBuffer[sIndex + 2]);
            tByteBuffer[tIndex + 3] = 255;
        }
        // writing result to canvas.
        resCtx.putImageData(imgRes, 0, 0);
        
        sBuffer = null;
        tBuffer = null;
        tByteBuffer = null;
        
        return resCV;
    }
    
    // normalize a scale <1 to avoid some rounding issue with js numbers
    normaliseScale(s) {
        if (s>1) throw('s must be <1');
        s = 0 | (1/s);
        var l = this.log2(s);
        var mask = 1 << l;
        var accuracy = 4;
        while(accuracy && l) { l--; mask |= 1<<l; accuracy--; }
        return 1 / ( s & mask );
    }
    
    log2(v) {
        // taken from http://graphics.stanford.edu/~seander/bithacks.html
        var b =  [ 0x2, 0xC, 0xF0, 0xFF00, 0xFFFF0000 ];
        var S =  [1, 2, 4, 8, 16];
        var i=0, r=0;
        
        for (i = 4; i >= 0; i--) {
          if (v & b[i])  {
            v >>= S[i];
            r |= S[i];
          } 
        }
        return r;
    }
    
    _datiImmagineDiRerimento(imgObj,imgInfo,blockObj){
        
        var x1 = imgInfo.x1;
        var y1 = imgInfo.y1;
        var x2 = imgInfo.x2;
        var y2 = imgInfo.y2;
        var posy = imgInfo.posy;
        var posx = imgInfo.posx;

        var imgOriginalWidth = imgObj.naturalWidth;
        var imgOriginalHeight = imgObj.naturalHeight;
        
        var imgRatio = imgOriginalHeight / imgOriginalWidth; /*moltiplicatore di altezza rispetto a larghezza*/

        var imgOriginalSafeWidth = x2 - x1;
        var imgOriginalSafeHeight = y2 - y1;
        var imgOriginalSafeCenterX = (imgOriginalSafeWidth / 2) + x1;
        var imgOriginalSafeCenterY = (imgOriginalSafeHeight / 2) + y1;


        var imgCalculatedWidth;
        var imgCalculatedHeight;

        var containerWidth = blockObj.width;
        var containerHeight = blockObj.height;

        var imgCalculatedY;
        var imgCalculatedX;

        var isVerticale;
        
        if( (containerHeight * imgOriginalSafeWidth) / imgOriginalSafeHeight > containerWidth ){
        
            isVerticale = false;
        
            //areaWidth deve essere grande come bannerWidth quindi
            imgCalculatedWidth = containerWidth * imgOriginalWidth / imgOriginalSafeWidth;

            //regolo la height proporzionalmente
            imgCalculatedHeight = imgCalculatedWidth * imgRatio;

            //regolo anche gli scostamenti nella stessa maniera
            imgCalculatedX = x1 * imgCalculatedWidth / imgOriginalWidth;

            if (posy == "t") {
                imgCalculatedY = (y1 * imgCalculatedHeight / imgOriginalHeight);
            }else if (posy == "m") {
                imgCalculatedY = (imgOriginalSafeCenterY * imgCalculatedHeight / imgOriginalHeight) - (containerHeight / 2);
            }else if (posy == "b") {
                imgCalculatedY = (y2 * imgCalculatedHeight / imgOriginalHeight) - containerHeight;
            }

        }else{
            isVerticale = true;
            //console.log("tengo l'altezza = is Verticale true");

            //safeHeight deve essere grande come containerHeight quindi
            imgCalculatedHeight = containerHeight * imgOriginalHeight / imgOriginalSafeHeight;

            //regolo la height proporzionalmente
            imgCalculatedWidth = imgCalculatedHeight / imgRatio;

            //regolo anche gli scostamenti nella stessa maniera
            imgCalculatedY = y1 * imgCalculatedHeight / imgOriginalHeight;

            if (posx == "l") {
                imgCalculatedX = (x1 * imgCalculatedWidth / imgOriginalWidth);
            }else if (posx == "c") {
                imgCalculatedX = (imgOriginalSafeCenterX * imgCalculatedWidth / imgOriginalWidth) - (containerWidth / 2);
            }else if (posx == "r") {
                imgCalculatedX = (x2 * imgCalculatedWidth / imgOriginalWidth) - containerWidth;
            }
        }
        
        return {
            width : imgCalculatedWidth,
            height : imgCalculatedHeight,
            left : (-1*imgCalculatedX),
            top : (-1*imgCalculatedY)
        };
        
    }
    
    
    _internalPrepare(){
        var that = this;
        var p = new Promise(function(resolve,reject){
            
            that._resolve = resolve;
            that._reject = reject;
            that.prepareImages();
        });
        return p;
    }
    
}