class BaseContainer {
    constructor(options) {
        options = typeof options !== "object" ? {} : options;
        let wrapper = document.createElement("div");
        let uniq = 'yxxxxxxy-yxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        });
        let style = "position:relative;overflow:hidden;";
        if(options.width){
           style += "width:" + options.width + "px;";
        }else{
            style += "width:100%;";
        }
        if(options.height){
           style += "height:" + options.height + "px;";
        }else{
            style += "height:100%;";
        }
        wrapper.setAttribute("style",style);
        wrapper.setAttribute("id","wrapper_" + uniq);
        
        if(!options.wrapper){
            //creo il wrapper
            document.write(wrapper.outerHTML);
            
        }else{
            options.wrapper.append(wrapper);
        }
        
        wrapper = document.getElementById("wrapper_" +uniq);
        let obj = document.createElement("canvas");
        this.width = wrapper.clientWidth;
        this.height = wrapper.clientHeight;
        obj.setAttribute("id",uniq);
        obj.setAttribute("width",this.width+"px");
        obj.setAttribute("height",this.height+"px");
        wrapper.append(obj);
        
        this._obj = document.getElementById(uniq);
        this._ctx = this._obj.getContext("2d");
        
        this._originalTemplate = {};
        if(options.temp){
            this._originalTemplate = options.temp;
        }
        
        this._variables = {};
        if(options.variable){
            this._variables = options.variable;
        }
        
        this._mainTemplate = JSON.stringify( this._originalTemplate );
        this._mainTemplate = JSON.parse(this._mainTemplate);
        this._copyTemplate = null;
        this._mainLayer = undefined;
        this._callbackDrawed = options.onDrawed ? options.onDrawed : null;
        this._fontsLoaded = {};
        
        this.debug = false;
        
        this._intervalObj = null;
        this._intervalTimeout = 20;
        this._intervalActivate = true;
    }
    
    getCanvas(){
        return this._obj;
    }
    
    getContext(){
        return this._ctx;
    }
    
    getBounds(){
        return this._mainLayer.getBounds(0);
    }
    
    getVariableWithName(vName){
        return this._variables[vName] || {};
    }
    
    loadFontName(fontName,cb){
        var that = this;
        if(this._fontsLoaded[fontName] && this._fontsLoaded[fontName].loaded){
            cb(this._fontsLoaded[fontName].object);
        }else if(this._fontsLoaded[fontName]){
            this._fontsLoaded[fontName].cbs.push(cb);
        }else{
            this._fontsLoaded[fontName] = {
                loaded : false,
                object: false,
                cbs : []
                };
            this._fontsLoaded[fontName].cbs.push(cb);
            opentype.load(fontName, function(err, font){
                if (err) {
                    console.log("impossibile caricare il font");
                } else {
                    
                    that._fontsLoaded[fontName].loaded = true;
                    that._fontsLoaded[fontName].object = font;
                    for(var i = 0; i< that._fontsLoaded[fontName].cbs.length; i++){
                        that._fontsLoaded[fontName].cbs[i](that._fontsLoaded[fontName].object);
                    }
                }
                
            });
        }
    }
    
    parse(){
        
        //in base alle dimensioni decido quanti layer fare per esempio questi tre
        this._copyTemplate = {};
        this._copyTemplate = JSON.stringify( this._mainTemplate );
        this._copyTemplate = JSON.parse(this._copyTemplate);
        //return false;
        switch(this._copyTemplate.type){
            case "txt":
                this._mainLayer = new LayerText(this);
                break;
            case "imageLogo":
                this._mainLayer = new LayerImageLogo(this);
                break;
            case "image":
                this._mainLayer = new LayerImage(this);
                break;
            default:
                this._mainLayer = new Layer(this);
                break;
        }
        
        this._mainLayer.width = this.width;
        this._mainLayer.height = this.height;
        this._mainLayer.setOrigin(this._copyTemplate.origin);
        if(this._copyTemplate.bk){
            this._mainLayer.setBackgroundColor(this._copyTemplate.bk);
        }
        if(this._copyTemplate.name){
            this._mainLayer.setName(this._copyTemplate.name);
        }
        if(this._copyTemplate.name){
            this._mainLayer.setOptions(this.getVariableWithName(this._copyTemplate.name));
        }
        this._mainLayer.calcOrientation();
        var newWidth,newHeight;
        var somma = 0;
        if(this._copyTemplate.blocks){
            for(let j = 0; j < this._copyTemplate.blocks.length; j++){
                
                var scatti = this._mainLayer.height*this._mainLayer._origin / this._mainLayer.width;
                var variazione = 1;
                if (scatti > 1){ // se è verticale
                    variazione = this._copyTemplate.blocks[j]["v-size"] * (this._copyTemplate.blocks[j]["v-weight"] - 0) * (scatti - 1);
                    if (this._copyTemplate.blocks[j]["v-weight"] >= 1) {
                        newHeight = this._copyTemplate.blocks[j]["v-size"] + variazione;
                    }else{
                        newHeight = this._copyTemplate.blocks[j]["v-size"] - variazione;
                    }
                    if (newHeight < 0) {
                        newHeight = 0;
                    }
                    
                    this._copyTemplate.blocks[j]["v-size"] = newHeight;
                    somma += this._copyTemplate.blocks[j]["v-size"];
                    
                }else{
                    variazione = this._copyTemplate.blocks[j]["h-size"] * (this._copyTemplate.blocks[j]["h-weight"] - 0) * ((1/scatti)-1);
                    if (this._copyTemplate.blocks[j]["h-weight"] >= 1) {
                        newWidth = this._copyTemplate.blocks[j]["h-size"] + variazione;
                    }else{
                        newWidth = this._copyTemplate.blocks[j]["h-size"] - variazione;
                    }
                    if (newWidth < 0) {
                        newWidth = 0;
                    }
                    
                    this._copyTemplate.blocks[j]["h-size"] = newWidth;
                    somma += this._copyTemplate.blocks[j]["h-size"];
                }
                
            }
            
            for(let i = 0; i < this._copyTemplate.blocks.length; i++){
                //push(new Layer(this,0,0,20,100).setName("parent1"));
                this._mainLayer.addChild(this._copyTemplate.blocks[i],somma);
            }
        }
        var that = this;
        this._mainLayer.prepare()
        .then(
            function success(){
                that.draw();
            },
            function reject() {
                console.log("qualcosa non ci sta");
                that.removeMinorElement();
            })
        .catch(
            function(e){
                console.log("errore indefinito");
                console.log(e);
            }
        );
    }
    
    draw(){
        this.getContext().clearRect(0,0,this.width,this.height);
        this._mainLayer.drawMe();
        this.removeClickableAree();
        this.drawClickableAree();
        if(this._callbackDrawed){
            this._callbackDrawed(this.getContext());
        }
        
        //una volta disegnato aggiungo gli eventi di listening
        window.addEventListener("resize",this.resizeEvent.bind(this),true);
    }
    
    resizeEvent(){
        
        if(this._intervalActivate){
            if(this._intervalObj !== null){
                clearTimeout(this._intervalObj);
                this._intervalObj = null;
            }
            
            this._intervalObj = setTimeout(this.resize.bind(this),this._intervalTimeout);
        }
        
    }
    
    resize(){
        let diffW = Math.abs(this.width - this._obj.parentNode.clientWidth);
        let diffH = Math.abs(this.height - this._obj.parentNode.clientHeight);
        if(diffW >10 || diffH > 10 ){
            
            //se la differenza di una delle 2 dimensione è maggio di 10 px allore ridisegno il tutto
            this._mainTemplate = JSON.stringify( this._originalTemplate );
            this._mainTemplate = JSON.parse(this._mainTemplate);
            this.width = this._obj.parentNode.clientWidth;
            this.height = this._obj.parentNode.clientHeight;
            this._obj.setAttribute("width",this.width+"px");
            this._obj.setAttribute("height",this.height+"px");
            this.parse();
        }else{
            
        }
    }
    
    redrawAll(){
        this.draw();
    }
    
    removeMinorElement(){
        var min = this._mainLayer.findMinorityReport(0);
        this.removeFromJsonPriority(this._mainTemplate.blocks,min);
        this._mainLayer.removeAll();
        delete this._mainLayer;
        this.parse();
        
    }
    
    removeFromJsonPriority(blocks,value){
        for(let i = 0; i < blocks.length;i++){
            if(blocks[i].priority >= value){
                blocks.splice(i, 1);
                this.removeFromJsonPriority(blocks,value);
                return false;
            }else{
                if(blocks[i].blocks){
                    this.removeFromJsonPriority(blocks[i].blocks,value);
                }
            }
        }
        
    }
    
    
    drawClickableAree(a){
        if(typeof a === "undefined"){
            console.log(this.getBounds());
        }
        var json = typeof a === "undefined" ? this.getBounds() : a;
        
        if(json.ref.className != "Layer" || json.ref._cta !== null){
            let styleOptions = "";
            var el = document.createElement("div");
            styleOptions += "top:"+Math.floor(json.y)+"px;";
            styleOptions += "left:"+Math.floor(json.x)+"px;";
            styleOptions += "width:"+Math.floor(json.width)+"px;";
            styleOptions += "height:"+Math.floor(json.height)+"px;";
            styleOptions += "z-index:"+json.livello+";";
            
            if(json.ref._cta !== null){
                //styleOptions += "cursor:pointer;";
                el.setAttribute("data-link",json.ref._cta);
                var ela = document.createElement("a");
                ela.setAttribute("href",json.ref._cta);
                ela.setAttribute("style","display:block;width:100%;height:100%;");
                ela.setAttribute("target","_blank");
                el.append(ela);
            }
            
            el.setAttribute("style",styleOptions);
            
            let classes = "overlayDiv";
            if (PHPIstest) {
                classes += " test";
            }
            el.setAttribute("class",classes);
            this._obj.parentNode.append(el);
    
        }
        
        
        if(json.childs && json.childs.length>0){
            for(var i = 0; i < json.childs.length; i++){
                this.drawClickableAree(json.childs[i]);
            }
        }
    }
    
    removeClickableAree(){
        let elem = this._obj.parentNode.querySelectorAll(".overlayDiv");
        for(let i = 0; i < elem.length; i++){
            elem[i].parentNode.removeChild(elem[i]);
        }
    }
    
    drawed(cb){
        this._callbackDrawed = cb;
    }
    
    //funzioni esterne
    
    getImage(type){
        //e.preventDefault();
        //e.stopPropagation();
        var data = false;
        switch(type){
            case "jpg":
                data = this.getCanvas().toDataURL("image/jpeg");
                break;
            case "png":
                data = this.getCanvas().toDataURL("image/png");
                break;
        }
        
        return data;
    }
    
    getPNG(){
        return this.getImage("png");
    }
    
    getJPG(){
        return this.getImage("jpg");
    }

}

