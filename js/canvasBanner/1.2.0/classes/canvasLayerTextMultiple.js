class LayerTextMultiple extends Layer {
    constructor(...args) {        
        super(...args);
        this._txtObj = [];
        this._resolve = null;
        
        this._varticalAlign = "middle";
        this._reject = null;
        this._ready = false;
        this.className = "LayerTextMultiple";
    }
    
    setOptions(arr){
        super.setOptions(arr);
        if(arr.texts){
            for(var i = 0; i < arr.texts.length; i++){
                var objTxt = new LayerText(this);
                
                objTxt.setOptions(arr.texts[i]);
                
                objTxt._calculateRicorsive = false;
                objTxt._resetValueOnCalculate = false;
                objTxt.prepareForAnalisis();
                
                this._txtObj.push(objTxt);
            }
        }
        if(arr.valign){
            this._varticalAlign = arr.valign;
        }
    }
    
    drawMe(){
        
        if(!this._ready)
            return false;
        
        var x = this.getRealX();
        var y = this.getRealY();
        var ctx = this.getContext();
        
        ctx.fillStyle = this._backgroundColor;
        ctx.fillRect(x,y,this.width,this.height);
        
        // prima di disegnarli .. ci calcolo l'altezza reale di tutti e gli setto di conseguenza l'altezza
        var altezzaTot = 0;
        for(let i = 0; i < this._txtObj.length; i++){
            altezzaTot+=this._txtObj[i].getTextTotHeight();
            this._txtObj[i].height = this._txtObj[i].getTextTotHeight();
        }

        var startingY = this.y;
        
        
        if(this._varticalAlign == "top"){
            //niente
        }else if(this._varticalAlign == "bottom"){
            startingY += this.height - altezzaTot;
        }else{
            //middle
            startingY += (this.height - altezzaTot)/2;
        }
        
        for(let i = 0; i < this._txtObj.length; i++){
            //a questo punto ne so le dimensioni e lo sposto i due blocchi di conseguenza
            //prima di disegnarli
            this._txtObj[i].y = startingY;
            this._txtObj[i].height = this._txtObj[i].getTextTotHeight();
            startingY += this._txtObj[i].getTextTotHeight();
            //this._varticalAlign
            this._txtObj[i].drawMe();
        }
        
        //
       
        ctx.save();
        ctx.restore();
        
        for(let i = 0; i < this._childs.length; i++){
            this._childs[i].drawMe();
        }
    }
    
    _internalPrepare(){
        var that = this;
        var p = new Promise(function(resolve,reject){
            
            that._resolve = resolve;
            that._reject = reject;
            that.formatNotChild();
        });
        return p;
    }
    
    
    formatNotChild(childIndex){
        
        var that = this;
        childIndex = typeof childIndex === "undefined" ? 0 : childIndex;
        
        if(childIndex>= this._txtObj.length){
            //allora sono tutti pronti e anche io
            that._ready = true;
            this._resolve(this);
        }else{
            if(childIndex===0){
                for(let i = 0; i < this._txtObj.length; i++){
                    
                    this._txtObj[i].outBox = {width: this.outBox.width,height:this.outBox.height};
                    this._txtObj[i].width = this.width;
                    this._txtObj[i].height = this.height;
                    this._txtObj[i].x = this.x;
                    this._txtObj[i].y = this.y;
                    
                }
            }else{
                //per calcolare lo spazio che mi serve devo toglie dall'altezza totale lo spazio degli altri blocchi
                var totHeight = this.height;
                for(let i = 0; i < childIndex;i++){
                    totHeight -= this._txtObj[i].getTextTotHeight();
                }
                this._txtObj[childIndex].height = totHeight;
            }
            
            this._txtObj[childIndex].prepare().then(function(){
                
                childIndex = childIndex +1;
                that.formatNotChild(childIndex);
            },function(obj){
                
                for(var i = 0; i < that._txtObj.length; i++){
                    if(that._txtObj[i].canDecreseFontOrString()){
                        that._txtObj[i].decreaseFontOrString();
                    }else{
                        //non ci sta il blocco
                        return that._reject(that);
                        
                    }    
                }
                that.formatNotChild(0);
            });
        }
        
        
    }
    
}