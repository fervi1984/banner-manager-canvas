class LayerImageLogo extends Layer {
    constructor(...args) {        
        super(...args);
        
        this._ready = false;
        
        this._orizzontalAlign = "center";
        this._varticalAlign = "middle";
        
        this._orizImage = null;
        this._vertImage = null;
        this._imgElement = null;
        
        this._resolve = null;
        this._reject = null;
        this.className = "LayerImageLogo";
    }
    
    setOptions(obj){
        
        for(let i in obj){
            if(true){ //mi da errore l'editor se non metto un if
                switch(i){
                    case "urlV":
                        if(obj[i].url !== false)
                            this.setVerticalImage(obj[i].url);
                        break;
                    case "urlH":
                        if(obj[i].url !== false)
                            this.setOrizontaleImage(obj[i].url);
                        break;
                    case "align":
                        this.setOrizzontalAlign(obj[i]);
                        break;
                    case "valign":
                        this.setVerticalAlign(obj[i]);
                        break;
                }
            }
        }
        
    }
    
    setVerticalImage(image){
        this._vertImage = image;
        if(this._orizImage === null){
            this.setOrizontaleImage(image);
        }
    }
    
    setOrizontaleImage(image){
        this._orizImage = image;
        if(this._vertImage === null){
            this.setVerticalImage(image);
        }
    }
 
    /*valori accettati left,center,right*/
    setOrizzontalAlign(align){
        this._orizzontalAlign = align;
    }
    
    /*valori accettati top,middle,bottom*/
    setVerticalAlign(align){
        this._varticalAlign = align;
    }
    
    prepareImages(){
        this._imgElement= document.createElement('img');
        var that = this;
        var imageData = (this.isLayoutVerticale() ?  this._orizImage : this._vertImage);
        if(imageData === null){
            
            console.log("non ci è immagine");
            return false;
        }
        this._imgElement.onload = function() {
            that._ready = true;
            that._resolve(1);
        };
        this._imgElement.src = 'data:image/svg+xml;base64,\n' + imageData;
        //console.log(imageData);
    }
    
    drawMe(){
        if(!this._ready)
            return false;
        
        var x = this.getRealX();
        var y = this.getRealY();
        var ctx = this.getContext();
        
        if(this._backgroundColor && this._backgroundColor !== "transparent"){
            ctx.fillStyle = this._backgroundColor;
            ctx.fillRect(x,y,this.width,this.height);
        }
        
        //crome mi streccia l'immagine e fa bene .. firefox no e se non sono presente le dimensioni nell'svg non lo disegna nemmeno
        var thisHeight = parseInt(this.height), thisWidth = parseInt(this.width);
        var newHeight = thisHeight, newWidth = thisWidth;
        var imgRatio = this._imgElement.width/this._imgElement.height;
        //if(this.width > this.height){
            //quindi tocca prima l'altezza
            if(thisHeight*imgRatio <= thisWidth){
                newWidth = thisHeight*imgRatio;
            }else if(thisWidth/imgRatio <= thisHeight){
                newHeight = thisWidth/imgRatio;
            }else{
                console.log("non so che cosa fare");
            }
            
            //centro l'immagine
            var newX = this.x, newY = this.y;
            
            newX += (thisWidth - newWidth)/2;
            newY += (thisHeight - newHeight)/2;
                
        //}else{}
        var scale = Math.floor(newWidth/this._imgElement.naturalWidth*100)/100;
        var pScale = this._imgElement;
        if(scale<0.9){
            pScale = this.downScaleImage(this._imgElement,newWidth,newHeight);
        } else {
            newX = this.x + (thisWidth-this._imgElement.width)/2;
            newY = this.y + (thisHeight-this._imgElement.height)/2;   
        }
        
        ctx.drawImage(pScale,newX,newY);
        
        ctx.save();
        ctx.restore();
        
        for(let i = 0; i < this._childs.length; i++){
            this._childs[i].drawMe("red");
        }
    }
    
    _internalPrepare(){
        var that = this;
        var p = new Promise(function(resolve,reject){
            
            that._resolve = resolve;
            that._reject = reject;
            that.prepareImages();
        });
        return p;
    }
    
    // scales the image by (float) scale < 1
    // returns a canvas containing the scaled image.
    downScaleImage(img, w,h) {
        let imgCV = document.createElement('canvas');
        imgCV.width = img.width;
        imgCV.height = img.height;
        let imgCtx = imgCV.getContext('2d');
        
        imgCtx.drawImage(img, 0, 0);
        this.resample_single(imgCV, w,h,false);
        return imgCV;
    }
    
    /**
    * Hermite resize - fast image resize/resample using Hermite filter. 1 cpu version!
    * 
    * @param {HtmlElement} canvas
    * @param {int} width
    * @param {int} height
    * @param {boolean} resize_canvas if true, canvas will be resized. Optional.
    */
   resample_single(canvas, width, height, resize_canvas) {
       var width_source = canvas.width;
       var height_source = canvas.height;
       width = Math.round(width);
       height = Math.round(height);
   
       var ratio_w = width_source / width;
       var ratio_h = height_source / height;
       var ratio_w_half = Math.ceil(ratio_w / 2);
       var ratio_h_half = Math.ceil(ratio_h / 2);
   
       var ctx = canvas.getContext("2d");
       var img = ctx.getImageData(0, 0, width_source, height_source);
       var img2 = ctx.createImageData(width, height);
       var data = img.data;
       var data2 = img2.data;
   
       for (var j = 0; j < height; j++) {
           for (var i = 0; i < width; i++) {
               var x2 = (i + j * width) * 4;
               var weight = 0;
               var weights = 0;
               var weights_alpha = 0;
               var gx_r = 0;
               var gx_g = 0;
               var gx_b = 0;
               var gx_a = 0;
               var center_y = (j + 0.5) * ratio_h;
               var yy_start = Math.floor(j * ratio_h);
               var yy_stop = Math.ceil((j + 1) * ratio_h);
               for (var yy = yy_start; yy < yy_stop; yy++) {
                   var dy = Math.abs(center_y - (yy + 0.5)) / ratio_h_half;
                   var center_x = (i + 0.5) * ratio_w;
                   var w0 = dy * dy; //pre-calc part of w
                   var xx_start = Math.floor(i * ratio_w);
                   var xx_stop = Math.ceil((i + 1) * ratio_w);
                   for (var xx = xx_start; xx < xx_stop; xx++) {
                       var dx = Math.abs(center_x - (xx + 0.5)) / ratio_w_half;
                       var w = Math.sqrt(w0 + dx * dx);
                       if (w >= 1) {
                           //pixel too far
                           continue;
                       }
                       //hermite filter
                       weight = 2 * w * w * w - 3 * w * w + 1;
                       var pos_x = 4 * (xx + yy * width_source);
                       //alpha
                       gx_a += weight * data[pos_x + 3];
                       weights_alpha += weight;
                       //colors
                       if (data[pos_x + 3] < 255)
                           weight = weight * data[pos_x + 3] / 250;
                       gx_r += weight * data[pos_x];
                       gx_g += weight * data[pos_x + 1];
                       gx_b += weight * data[pos_x + 2];
                       weights += weight;
                   }
               }
               data2[x2] = gx_r / weights;
               data2[x2 + 1] = gx_g / weights;
               data2[x2 + 2] = gx_b / weights;
               data2[x2 + 3] = gx_a / weights_alpha;
           }
       }
       //clear and resize canvas
       if (resize_canvas === true) {
           canvas.width = width;
           canvas.height = height;
       } else {
           ctx.clearRect(0, 0, width_source, height_source);
       }
   
       //draw
       ctx.putImageData(img2, 0, 0);
   }
    
}