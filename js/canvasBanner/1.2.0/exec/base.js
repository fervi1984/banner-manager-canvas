( function(w,d) {
    if (!w.bannerContent) {
        w.bannerContent = new BaseContainer({"temp":PHPmainTemplate,"variable":PHPVariable,"width":PHPWidth,"height":PHPHeight,"onDrawed": () => {
			var elem = d.getElementById('loaderWrapper');
			if (elem){
				elem.parentNode.removeChild(elem);
			}
			
		}});
        w.bannerContent.parse();
		w.addEventListener('resize', () => {
			var body = document.querySelector("body");
			var el = document.createElement("div");
			el.id = "loaderWrapper";
			el.setAttribute("style","position:absolute;top:0;bottom:0;left:0;right:0;z-index:9099; background-color:black;");
			var loader = document.createElement("div");
			loader.setAttribute("class","loader");
			el.appendChild(loader);
			body.appendChild(el);
		}, true);
    }else{
        console.log("esiste gia un banner .. gestire casi multiple");
    }
    
})(window,document);
