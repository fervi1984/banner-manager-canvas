<html>
	<head>
        <script src="<?php echo ROOT_PATH; ?>plugins/opentype/opentype.js"></script>
    <style>
        body{margin:0;}
		
		.disabled {
			pointer-events: none;
			cursor: default;
		}
		 
		.button {
			padding: 5px 15px;
			background-color: #cccccc;	
		}
		 
		.overlayDiv.test {
			position: absolute;
			border: none;
		}
		
		.overlayDiv.test:hover{
			border: 1px dotted black;
			background-color: rgba(255,0,0,0.3);
		}
		 
		.loader {
			border: 3px solid #f3f3f3; /* Light grey */
			border-top: 3px solid #3498db; /* Blue */
			border-radius: 50%;
			width: 18px;
			height: 18px;
			position: absolute;
			top:50%;
			left:50%;
			margin-top:-9px;
			margin-left:-9px;
			animation: spin 2s linear infinite;
		}

		@keyframes spin {
			0% { transform: rotate(0deg); }
			100% { transform: rotate(360deg); }
		}
		 
    </style>
    </head>
    
    <body>
		<script src="<?php echo ROOT_PATH; ?>js/projectCanvas?<?php echo $scriptVariable; ?>"></script>
    </body>
</html>