
(function (d,w) {
	'use strict';

	w.banner = function(divContainer, options = null){
		if (options == null) {
			options = {"nothing": true};
		}
		return this.init(divContainer, options);
	};

	banner.prototype = {
		bannerNode : null,
		priority : {
			previous: null,
			current: null,
			last: 0
		},
		options : {
			minWidth: null,
			fonts: {
				adjustment: 0.5,
				min: 10
			}
		},

		init : function(bannerNode, options){
			var that = this;
			that.options = w.mergeJSON(that.options,options);
			that.bannerNode = bannerNode;

			that.initTexts();
			that.initPriorities();

			that.initLayout();

			w.onresize = function(){
				that.onWindowResize();
			};
		},
		initTexts : function(){
			var that = this;
			var texts = that.bannerNode.querySelectorAll(".sizerText");
			for (var i = 0; i < texts.length; i++) {
				if(texts[i].innerHTML == ""){
					texts[i].parentNode.removeChild(texts[i]);
				}
				texts[i].setAttribute("data-origsize",parseFloat(window.getComputedStyle(texts[i], null).getPropertyValue("font-size")));
				texts[i].setAttribute("data-currSize",parseFloat(window.getComputedStyle(texts[i], null).getPropertyValue("font-size")));
			}
		},
		initPriorities : function(){
			var that = this;
			var priorities = that.bannerNode.querySelectorAll("[data-priority]");
			for (var i = 0; i < priorities.length; i++) {
				if(parseInt(priorities[i].getAttribute("data-priority")) > that.priority.last){
					that.priority.last = parseInt(priorities[i].getAttribute("data-priority"));
				}
			}
			that.priority.current = that.priority.last;
		},
		initLayout : function(){
			var that = this;

			that.updateBlockSizes();
			that.updatePaddings();
			that.updateImages();
			that.updateFontSizes();

		},
		updateLayout : function(){
			var that = this;
			var retFunctions = that.updateBlockSizes() && that.updatePaddings() && that.updateImages() && that.updateFontSizes();
			var retSforo = true;

			var exts = that.bannerNode.querySelectorAll(".sizerExt");
			for (var i = 0; i < exts.length; i++){
				if(that.checkSforo(exts[i])){
					retSforo = false;
				}
			}
			return retFunctions && retSforo;
		},
		updateBlockSizes : function(){
			var that = this;

			var areas = that.bannerNode.querySelectorAll(".area");
			for (var iArea = 0; iArea < areas.length; iArea++) {
				var area = areas[iArea];
				var totaleImpesato = 0;
				//var scatti = (area.clientHeight*parseFloat(area.getAttribute("data-origin")) / area.clientWidth) - 1;
				var scatti = (area.clientHeight*parseFloat(area.getAttribute("data-origin")) / area.clientWidth);
				// console.log("("+area.clientHeight+"*"+parseFloat(area.getAttribute("data-origin"))+" / "+area.clientWidth+") - 1");
				// console.log("("+area.clientHeight*parseFloat(area.getAttribute("data-origin")) / area.clientWidth+") - 1");

				//if (scatti > 0){ // se è verticale
				if (scatti > 1){ // se è verticale
					area.removeClass("hSetting");
					area.addClass("vSetting");
					var newHeight;
					for (var iBlock = 0; iBlock < area.children.length; iBlock++) {
						if(!area.children[iBlock].hasClass("removed")){
							var variazione = parseFloat(area.children[iBlock].getAttribute("data-v-size")) * (parseFloat(area.children[iBlock].getAttribute("data-v-weight")) - 0) * (scatti - 1);
							if (parseFloat(area.children[iBlock].getAttribute("data-v-weight")) >= 1) {
								newHeight = parseFloat(area.children[iBlock].getAttribute("data-v-size")) + variazione;
							}else{
								newHeight = parseFloat(area.children[iBlock].getAttribute("data-v-size")) - variazione;
							}
							if (newHeight < 0) {
								newHeight = 0;
							}
							totaleImpesato += newHeight;
							area.children[iBlock].setAttribute("data-h-new",newHeight);
						}
					}
					for (var iBlock = 0; iBlock < area.children.length; iBlock++) {
						if(!area.children[iBlock].hasClass("removed")){
							var newHeight = parseFloat(area.children[iBlock].getAttribute("data-h-new")) * area.clientHeight / totaleImpesato;
							area.children[iBlock].style.height = Math.floor(newHeight) + "px";
							area.children[iBlock].style.width = "100%";
						}
					}
				}else{ //se è orizzontale
					area.removeClass("vSetting");
					area.addClass("hSetting");
					var newWidth;
					for (var iBlock = 0; iBlock < area.children.length; iBlock++) {
						if(!area.children[iBlock].hasClass("removed")){
							var variazione = parseFloat(area.children[iBlock].getAttribute("data-h-size")) * (parseFloat(area.children[iBlock].getAttribute("data-h-weight")) - 0) * ((1/scatti)-1);
							// console.log(((1/scatti)-1));
							// if(iBlock == 2){
							// 	console.log(area.children[iBlock].getAttribute("data-h-size"));
							// 	console.log(area.children[iBlock].getAttribute("data-h-weight"));
							// 	console.log(scatti);
							// }
							if (parseFloat(area.children[iBlock].getAttribute("data-h-weight")) >= 1) {
								newWidth = parseFloat(area.children[iBlock].getAttribute("data-h-size")) + variazione;
							}else{
								newWidth = parseFloat(area.children[iBlock].getAttribute("data-h-size")) - variazione;
							}
							if (newWidth < 0) {
								newWidth = 0;
							}
							totaleImpesato += newWidth;
							area.children[iBlock].setAttribute("data-w-new",newWidth);
						}
					}
					for (var iBlock = 0; iBlock < area.children.length; iBlock++) {
						if(!area.children[iBlock].hasClass("removed")){
							var newWidth = parseFloat(area.children[iBlock].getAttribute("data-w-new")) * area.clientWidth / totaleImpesato;
							area.children[iBlock].style.width = Math.floor(newWidth) + "px";
							area.children[iBlock].style.height = "100%";
						}
					}
				}
			}
			return true; //nulla può andare storto
		},
		updateImages : function(){
			var that = this;

			var images = that.bannerNode.querySelectorAll(".dynamicImage");
			for (var i = 0; i < images.length; i++) {
				var image = images[i];
				var x1 = parseFloat(image.getAttribute("data-x1"));
				var y1 = parseFloat(image.getAttribute("data-y1"));
				var x2 = parseFloat(image.getAttribute("data-x2"));
				var y2 = parseFloat(image.getAttribute("data-y2"));

				var imgOriginalWidth = image.naturalWidth;
				var imgOriginalHeight = image.naturalHeight;
				var imgRatio = imgOriginalHeight / imgOriginalWidth; /*moltiplicatore di altezza rispetto a larghezza*/

				var imgOriginalSafeWidth = x2 - x1;
				var imgOriginalSafeHeight = y2 - y1;
				var imgOriginalSafeCenterX = (imgOriginalSafeWidth / 2) + x1;
				var imgOriginalSafeCenterY = (imgOriginalSafeHeight / 2) + y1;


				var imgCalculatedWidth;
				var imgCalculatedHeight;

				var containerWidth = image.parentNode.clientWidth;
				var containerHeight = image.parentNode.clientHeight;

				var imgCalculatedY;
				var imgCalculatedX;

				var isVerticale;
				//console.log(containerHeight * imgOriginalHeight / imgOriginalSafeHeight +">"+ containerWidth);
				if( (containerHeight * imgOriginalSafeWidth) / imgOriginalSafeHeight > containerWidth ){
				//if( (containerHeight * imgOriginalHeight / imgOriginalSafeHeight) > containerWidth ){
					isVerticale = false;
					//console.log("tengo la larghezza = is Verticale false");

					//areaWidth deve essere grande come bannerWidth quindi
					imgCalculatedWidth = containerWidth * imgOriginalWidth / imgOriginalSafeWidth;

					//regolo la height proporzionalmente
					imgCalculatedHeight = imgCalculatedWidth * imgRatio;

					//regolo anche gli scostamenti nella stessa maniera
					imgCalculatedX = x1 * imgCalculatedWidth / imgOriginalWidth;

					if (image.getAttribute("data-posy") == "t") {
						imgCalculatedY = (y1 * imgCalculatedHeight / imgOriginalHeight);
					}else if (image.getAttribute("data-posy") == "m") {
						imgCalculatedY = (imgOriginalSafeCenterY * imgCalculatedHeight / imgOriginalHeight) - (containerHeight / 2);
					}else if (image.getAttribute("data-posy") == "b") {
						imgCalculatedY = (y2 * imgCalculatedHeight / imgOriginalHeight) - containerHeight;
					}

				}else{
					isVerticale = true;
					//console.log("tengo l'altezza = is Verticale true");

					//safeHeight deve essere grande come containerHeight quindi
					imgCalculatedHeight = containerHeight * imgOriginalHeight / imgOriginalSafeHeight;

					//regolo la height proporzionalmente
					imgCalculatedWidth = imgCalculatedHeight / imgRatio;

					//regolo anche gli scostamenti nella stessa maniera
					imgCalculatedY = y1 * imgCalculatedHeight / imgOriginalHeight;

					if (image.getAttribute("data-posx") == "l") {
						imgCalculatedX = (x1 * imgCalculatedWidth / imgOriginalWidth);
					}else if (image.getAttribute("data-posx") == "c") {
						imgCalculatedX = (imgOriginalSafeCenterX * imgCalculatedWidth / imgOriginalWidth) - (containerWidth / 2);
					}else if (image.getAttribute("data-posx") == "r") {
						imgCalculatedX = (x2 * imgCalculatedWidth / imgOriginalWidth) - containerWidth;
					}
				}

				image.style.width = imgCalculatedWidth+"px";
				image.style.height = imgCalculatedHeight+"px";
				image.style.left = (-1*imgCalculatedX)+"px";
				image.style.top = (-1*imgCalculatedY)+"px";

				return true; //nulla può andare storto qui
			}
		},
		updatePaddings : function(){
			var that = this;
			var blocks = that.bannerNode.querySelectorAll("[data-padding]");
			for (var i = 0; i < blocks.length; i++) {
				var block = blocks[i];
				var padding = block.getAttribute("data-padding");
				var width = block.clientWidth;
				var height = block.clientHeight;
				block.style.paddingTop = height / 100 * padding;
				block.style.paddingBottom = height / 100 * padding;
				block.style.paddingLeft = width / 100 * padding;
				block.style.paddingRight = width / 100 * padding;
			}
			return true; //nulla può andare storto qui
		},
		updateFontSizes : function(){
			var that = this;
			var exts = that.bannerNode.querySelectorAll(".sizerExt");
			var ret = true;
			for (var i = 0; i < exts.length; i++) {
				var ext = exts[i];
				var ints = ext.querySelectorAll(".sizerInt");
				if (that.checkSforo(ext)) {
					for (var j = 0; j < ints.length; j++) {
						that.fontDecrease(ints[j]);
					}
					if(that.checkSforo(ext)){
						that.updateFontSizes(ext);
					}else{
//						return true;
					}
				}else{
					for (var j = 0; j < ints.length; j++) {
						that.fontIncrease(ints[j]);
					}
					if(that.checkSforo(ext)){
						for (var j = 0; j < ints.length; j++) {
							that.fontDecrease(ints[j]);
						}
//							return true;
					}else{
						if(that.checkMaxsize(ext)){
//								return true;
						}else{
							that.updateFontSizes(ext);
						}
					}

				}


		   }
			return true;
		},
		checkSforo : function(ext){
			var that = this;
			var int = ext.querySelector(".sizerInt");
			var sfora = false;
			if ((int.clientHeight > ext.clientHeight && parseFloat(window.getComputedStyle(int, null).getPropertyValue("height")) > parseFloat(window.getComputedStyle(int, null).paddingTop)  + parseFloat(window.getComputedStyle(int, null).paddingBottom))
			||  (int.clientWidth  > ext.clientWidth  && parseFloat(window.getComputedStyle(int, null).getPropertyValue("width"))  > parseFloat(window.getComputedStyle(int, null).paddingLeft) + parseFloat(window.getComputedStyle(int, null).paddingRight ))) {
				return true;
			}else{
				return false;
			}
		},
		checkMaxsize : function(ext){
			var that = this;
		   var texts = ext.querySelectorAll(".sizerText");
		   var allMax = true;
		   for (var i = 0; i < texts.length; i++) {
		      var text = texts[i];
		      if(parseFloat(text.getAttribute("data-currsize")) < parseFloat(text.getAttribute("data-origsize"))){
		         allMax = false;
		      }
		   }
		   return allMax;
		},
		fontDecrease : function(ext){
			var that = this;
		   var texts = ext.querySelectorAll(".sizerText");
			for (var i = 0; i < texts.length; i++) {
		      var text = texts[i];
		      var newSize = parseFloat(text.getAttribute("data-currsize")) - that.options.fonts.adjustment;
		      text.setAttribute("data-currsize", newSize);
		      if (newSize >= that.options.fonts.min) {
		         text.style.fontSize = newSize + "px";
			      text.style.lineHeight = (newSize+4) + "px";
		      }else{
					//testo.style.display = "none";
					if(!text.hasClass("textTooSmall")){
						text.addClass("textTooSmall");
						that.updatePriority(that.priority.current - 1);
					}
					return false; //NON SO SE E' GIUSTO
					//removeByPriority(nodeBanner);
		      }
		   }
			return true;
		},
		fontIncrease : function(ext){
			var that = this;
		   var texts = ext.querySelectorAll(".sizerText");
			for (var i = 0; i < texts.length; i++) {
		      var text = texts[i];
		      var newSize = parseFloat(text.getAttribute("data-currsize")) + that.options.fonts.adjustment;
				text.removeClass("textTooSmall");
		      text.setAttribute("data-currsize", newSize);
		      if (newSize <= text.getAttribute("data-origsize")) {
		         text.style.fontSize = newSize + "px";
			      text.style.lineHeight = (newSize+4) + "px";
		      }
		   }
			return true; //niente può andare storto qui
		},



		updatePriority : function(priority){
			var that = this;

			if(1 <= priority && priority <= that.priority.last){
				that.priority.previous = that.priority.current;
				that.priority.current = priority;


				if(that.priority.previous < that.priority.current && that.priority.current <= that.priority.last){
					//rimetto
					var removables = that.bannerNode.querySelectorAll('[data-priority="'+that.priority.current+'"]');
					for (var i = 0; i < removables.length; i++) {
						removables[i].removeClass("removed");
					}
				}else if(that.priority.previous > that.priority.current && that.priority.previous > 0){
					//tolgo
					var removables = that.bannerNode.querySelectorAll('[data-priority="'+that.priority.previous+'"]');
					for (var i = 0; i < removables.length; i++) {
						removables[i].addClass("removed");
					}
				}
				var smalls = that.bannerNode.querySelectorAll(".textTooSmall");
				for (var i = 0; i < smalls.length; i++) {
					smalls[i].removeClass("textTooSmall");
				}

				return that.updateLayout();
			}else{
				return false;
			}

		},

		resetPriority : function(){
			var that = this;
			that.priority.current = that.priority.last;
			that.priority.previous = null;

			var removables = that.bannerNode.querySelectorAll('[data-priority]');
			for (var i = 0; i < removables.length; i++) {
				removables[i].removeClass("removed");
			}
			return true;

		},
		onWindowResize : function(){
			var that = this;
			that.resetPriority();
			that.updateLayout();
		}
	};
})(document,window);
