<?php
	switch($par2){
		case "A":
		default:
			$json = '{
				"classes": "main",
				"origin": 1.5,
				"blocks": [
					{
						"classes": "",
						"priority": 1,
						"v-size": 60,
						"v-weight": 1,
						"h-size": 50,
						"h-weight": 1,
						"area": {
							"classes": "",
							"origin": 2,
							"blocks": [
								{
									"classes": "logo",
									"priority": 1,
									"v-size": 17,
									"v-weight": 1,
									"h-size": 15,
									"h-weight": 1,
									"html": "{logo}"

								},{
									"classes": "image background",
									"priority": 2,
									"v-size": 43,
									"v-weight": 2.5,
									"h-size": 46,
									"h-weight": 1.7,
									"html": "{image}"
								}
							]
						}
					},{
						"classes": "",
						"priority": 3,
						"v-size": 40,
						"v-weight": 2.4,
						"h-size": 50,
						"h-weight": 2,
						"area": {
							"classes": "",
							"origin": 10,
							"blocks": [
								{
									"classes": "",
									"priority": 3,
									"v-size": 80,
									"v-weight":4,
									"h-size": 100,
									"h-weight": 1,
									"area": {
										"classes": "",
										"origin": 3.5,
										"blocks": [
											{
												"classes": "",
												"priority": 3,
												"v-size": 60,
												"v-weight": 2,
												"h-size": 70,
												"h-weight": 1,
												"html": "
															<div class=\"sizerExt\">
																<div class=\"texts\">
																	<div class=\"sizerInt\">
																		<div class=\"title sizerText\">{title}</div>
																		<div class=\"subtitle sizerText\">{subtitle}</div>
																		<div class=\"content sizerText\">{content}</div>
																	</div>
																</div>
															</div>
														"
											},{
												"classes": "cta",
												"priority": 4,
												"v-size": 40,
												"v-weight": 1,
												"h-size": 30,
												"h-weight": 1,
												"html": "
															<div class=\"sizerExt\">
																<div class=\"sizerInt btn\">
																	<span class=\"sizerText\">{cta}</span>
																</div>
															</div>
														"
											}
										]
									}
								},{
									"classes": "web",
									"priority": 5,
									"v-size": 20,
									"v-weight": 1,
									"h-size": 0,
									"h-weight": 1,
									"html": "
												<div class=\"sizerExt\">
													<div class=\"sizerInt\">
														<div class=\"url sizerText\">{url}</div>
													</div>
												</div>
											"
								}
							]
						}
					}
				]
			}';
			break;

		case "B":
			$json = '{
				"classes": "main",
				"origin": 2,
				"blocks": [
					{
						"classes": "logo",
						"priority": 1,
						"v-size": 17,
						"v-weight": 1,
						"h-size": 10,
						"h-weight": 1,
						"html": "{logo}"
					},{
						"classes": "image background",
						"priority": 2,
						"v-size": 43,
						"v-weight": 2,
						"h-size": 27,
						"h-weight": 1,
						"html": "{image}"
					},{
						"classes": "",
						"priority": 3,
						"v-size": 40,
						"v-weight": 1,
						"h-size": 40,
						"h-weight": 1,
						"area": {
							"classes": "",
							"origin": 2,
							"blocks": [
								{
									"classes": "",
									"priority": 3,
									"v-size": 70,
									"v-weight": 4,
									"h-size": 73,
									"h-weight": 1,
									"html": "
										<div class=\"sizerExt\">
											<div class=\"texts\">
												<div class=\"sizerInt\">
													<div class=\"title sizerText\">{title}</div>
													<div class=\"subtitle sizerText\">{subtitle}</div>
													<div class=\"content sizerText\">{content}</div>
												</div>
											</div>
										</div>
									"
								},{
									"classes": "cta",
									"priority": 4,
									"v-size": 30,
									"v-weight": 1,
									"h-size": 50,
									"h-weight": 1,
									"html": "
										<div class=\"sizerExt\">
											<div class=\"sizerInt btn\">
												<span class=\"sizerText\">{cta}</span>
											</div>
										</div>
									"
								}


							]
						}
					}
				]
			}';
			break;

		case "C":
			 $json = '{
				"classes": "main",
				"origin": 2,
				"blocks": [
					{
						"classes": "image background",
						"priority": 2,
						"v-size": 55,
						"v-weight": 1,
						"h-size": 30,
						"h-weight": 1,
						"html": "{image}"
					},{
						"classes": "",
						"priority": 1,
						"v-size": 45,
						"v-weight": 1,
						"h-size": 70,
						"h-weight": 1,
						"area": {
							"classes": "",
							"origin": 1,
							"blocks": [
								{
									"classes": "",
									"priority": 3,
									"v-size": 80,
									"v-weight": 1,
									"h-size": 75,
									"h-weight": 1,
									"area": {
										"classes": "",
										"origin": 4,
										"blocks": [
											{
												"classes": "",
												"priority": 3,
												"v-size": 80,
												"v-weight": 1,
												"h-size": 80,
												"h-weight": 1,
												"html": "
															<div class=\"sizerExt\">
																<div class=\"texts\">
																	<div class=\"sizerInt\">
																		<div class=\"subtitle sizerText\">{subtitle}</div>
																		<div class=\"title sizerText\">{title}</div>
																		<div class=\"content sizerText\">{content}</div>
																	</div>
																</div>
															</div>
														"
											},{
												"classes": "cta",
												"priority": 4,
												"v-size": 20,
												"v-weight": 1,
												"h-size": 20,
												"h-weight": 1,
												"html": "
															<div class=\"sizerExt\">
																<div class=\"sizerInt btn\">
																	<span class=\"sizerText\">{cta}</span>
																</div>
															</div>
														"
											}
										]
									}
								},{
									"classes": "logo",
									"priority": 1,
									"v-size": 20,
									"v-weight": 1,
									"h-size": 20,
									"h-weight": 1,
									"html": "{logo}"
								}
							]
						}
					}
				]
			}';
			break;
		case "D":
			 $json = '{
				"classes": "main",
				"origin": 2,
				"blocks": [
					{
						"classes": "backgrounded",
						"priority": 1,
						"v-size": 45,
						"v-weight": 1,
						"h-size": 40,
						"h-weight": 1,
						"area": {

							"classes": "",
							"origin": 3,
							"blocks": [
								{
									"classes": "logo",
									"priority": 1,
									"v-size": 33,
									"v-weight": 1,
									"h-size": 20,
									"h-weight": 1,
									"html": "{logo}"
								},
								{
									"classes": "",
									"priority": 2,
									"v-size": 67,
									"v-weight": 1,
									"h-size": 75,
									"h-weight": 1,
									"area": {
										"classes": "",
										"origin": 4,
										"blocks": [
											{
												"classes": "",
												"priority": 2,
												"v-size": 80,
												"v-weight": 1,
												"h-size": 80,
												"h-weight": 1,
												"html": "
															<div class=\"sizerExt\">
																<div class=\"texts\">
																	<div class=\"sizerInt\">
																		<div class=\"subtitle\">{subtitle}</div>
																		<div class=\"content\">{content}</div>
																	</div>
																</div>
															</div>
														"
											}
										]
									}
								}
							]
						}
					},{
						"classes": "no-overflow",
						"priority": 3,
						"v-size": 55,
						"v-weight": 1,
						"h-size": 60,
						"h-weight": 1,
						"area": {
							"classes": "",
							"origin": 0.8,
							"blocks": [
								{
									"classes": "",
									"priority": 3,
									"v-size": 55,
									"v-weight": 1,
									"h-size": 55,
									"h-weight": 1,
									"html": "<div class=\"sizerExt\">
												<div class=\"sizerInt\">
													<div class=\"title\">{title}</div>
													<div class=\"cta\">{cta}</div>
												</div>
											</div>"
								},{
									"classes": "image background",
									"priority": 4,
									"v-size": 55,
									"v-weight": 1,
									"h-size": 45,
									"h-weight": 1,
									"html": "{image}"
								}
							]
						}
					}
				]
			}';
			break;
	}



	$json = str_replace("\t","",$json);
	$json = str_replace("\n","",$json);
	$json = str_replace("\r","",$json);
	$json = str_replace("\r\n","",$json);
	$jsonTemplate = json_decode($json, true);
?>


<html>
	<head>

		<script src="<?php echo ROOT_PATH; ?>banner/lib/thepotcore/thepotcore.js"></script>
		<script src="<?php echo ROOT_PATH; ?>banner/banner.js"></script>
		<link href="https://fonts.googleapis.com/css?family=Open+SansMuli:200,300,400,600,700,800,900|Muli:200,300,400,600,700,800,900|Rubik:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

		<link href="<?php echo ROOT_PATH; ?>banner/template-common.css" rel="stylesheet">
		<link href="<?php echo ROOT_PATH; ?>banner/templates/<?php echo $par2; ?>.css" rel="stylesheet">
		<?php
		include(ROOT_DIR."js/variabili.php");
		?>


		<style>
			*{
				box-sizing: border-box;
			}

			html,
			body{
				margin: 0px;
				padding: 0px;
				overflow: hidden;
			}

			#container{
				width: <?php if($width > 0 ){ echo $width; }else{ echo "100%"; } ?>;
				height: <?php if($height > 0 ){ echo $height; }else{ echo "100%"; } ?>;
				overflow: hidden;
			}
			#banner{
				font-family: 'Muli', sans-serif;
				background-color: <?php echo ($objBanner->getBgBanner()?$objBanner->getBgBanner():'transparent') ?>
			}
			#banner .area .blocco.logo{
				z-index: 10;
			}
			<?php
				echo $objBanner->getStyle();
			?>

		</style>
	</head>
	<body>
		<div id="container">
			<div id="banner"><?php
			   printArea($objBanner, $jsonTemplate);
			?></div>
		</div>
		<script>

			var theBanner = new banner(document.getElementById('banner'));

			// var nodeBanner = getNode("#banner");
			// window.onload = function() {
			// 	init(nodeBanner);
			// 	layout(nodeBanner);
			// };
			// window.onresize = function() {
			// 	 showAllPriority(nodeBanner);
			// 	 setTimeout(function(){
			// 	layout(nodeBanner);
			// },5000);
			// };

		</script>
	</body>
</html>


<?php

function printArea($objBanner, $area){
   echo '<div class="area ' . $area["classes"] . '" data-origin="' . $area["origin"] . '">';
   foreach($area["blocks"] as $block){
	   printBlock($objBanner, $block);
   }
   echo '</div>';

};

function printBlock($objBanner, $block){
   $data = $objBanner->getJson();
	$padding = "";
	if(array_key_exists("padding", $block)){
		$padding = $data["padding"];
	}
   echo '<div class="blocco ' . $block["classes"] . '" data-priority= "'. $block["priority"] .'" data-padding="' . $padding . '" data-v-size="' . $block["v-size"] . '" data-v-weight="' . $block["v-weight"] . '" data-h-size="' . $block["h-size"] . '" data-h-weight="' . $block["h-weight"] . '">';
	   if(array_key_exists("html", $block)){
		   printHtml($objBanner, $block["html"]);
	   }
	   if(array_key_exists("area", $block)){
		   printArea($objBanner, $block["area"]);
	   }
   echo "</div>";
};

function printHtml($objBanner, $html){
   /*$data = $objBanner->getJson();
   
   $html = @str_replace('{image}', '<img class="dynamicImage" src="' . ROOT_PATH ."assets/" . (($data["image"]["url"])? $data["image"]["url"] : $objBanner->getFileBanner()) . '">', $html);
   $html = @str_replace('{title}', ($data["title"])? $data["title"]: $objBanner->getHeadText(), $html);
   $html = @str_replace('{subtitle}', ($data["subtitle"])?$data["subtitle"]: $objBanner->getSubHeadText(), $html);
   $html = @str_replace('{content}', $data["content"], $html);
   $html = @str_replace('{cta}', ($data["cta"]["text"])?$data["cta"]["text"]: $objBanner->getCtaText(), $html);
   $html = @str_replace('{url}', ($data["cta"]["url"])?$data["cta"]["url"]: $objBanner->getCtaLink(), $html);
   $html = @str_replace('{logo}', '<img src="' . ROOT_PATH ."assets/" . (($data["logo"]["urlV"])? $data["logo"]["urlV"] : $objBanner->getFileLogo()) . '" class="onlyV"><img src="' . ROOT_PATH ."assets/" . (($data["logo"]["urlH"])? $data["logo"]["urlH"] : $objBanner->getFileLogo()) . '" class="onlyH">', $html);
	*/
	
	$data = $objBanner->getJson();
	#$html = str_replace('{image}', '<img class="dynamicImage" src="' . ASSET_PATH . $data["image"]["url"] . '" data-x1="'. $data["image"]["x1"] . '" data-y1="' . $data["image"]["y1"] . '" data-x2="' . $data["image"]["x2"] . '" data-y2="' . $data["image"]["y2"] . '" data-posx="' . $data["image"]["posx"] . '" data-posy="' . $data["image"]["posy"] . '">', $html);
	if(!empty($data) && is_array($data) && array_key_exists("secondaryimage",$data)){
		$html = str_replace('{secondaryimage}', '<img class="dynamicImage" src="' . ASSET_PATH . $data["secondaryimage"]["url"] . '" data-x1="'. $data["secondaryimage"]["x1"] . '" data-y1="' . $data["secondaryimage"]["y1"] . '" data-x2="' . $data["secondaryimage"]["x2"] . '" data-y2="' . $data["secondaryimage"]["y2"] . '" data-posx="' . $data["secondaryimage"]["posx"] . '" data-posy="' . $data["secondaryimage"]["posy"] . '">', $html);
	}
	if(!empty($data) && is_array($data) && array_key_exists("tertiaryimage",$data)){
		$html = str_replace('{tertiaryimage}', '<img class="dynamicImage" src="' . ASSET_PATH . $data["tertiaryimage"]["url"] . '" data-x1="'. $data["tertiaryimage"]["x1"] . '" data-y1="' . $data["tertiaryimage"]["y1"] . '" data-x2="' . $data["tertiaryimage"]["x2"] . '" data-y2="' . $data["tertiaryimage"]["y2"] . '" data-posx="' . $data["tertiaryimage"]["posx"] . '" data-posy="' . $data["tertiaryimage"]["posy"] . '">', $html);
	}
	
	$coordBannerJson = $objBanner->getCoordMainAreaBanner();
	$coordBannerArray = json_decode($coordBannerJson,true);
	$coordLogoJson = $objBanner->getCoordMainAreaLogo();
	$coordLogoArray = json_decode($coordLogoJson, true);
	
	$html = @str_replace('{image}', '<img class="dynamicImage" src="' . ASSET_PATH . (($data["image"]["url"])? $data["image"]["url"] : $objBanner->getFileBanner()) . '"  data-x1="'. ($data["image"]["x1"]?$data["image"]["x1"]:$coordBannerArray["x1"]) . '" data-y1="' . ($data["image"]["y1"]?$data["image"]["y1"]:$coordBannerArray["y1"]) . '" data-x2="' . ($data["image"]["x2"]?$data["image"]["x2"]:$coordBannerArray["x2"]) . '" data-y2="' . ($data["image"]["y2"]?$data["image"]["y2"]:$coordBannerArray["y2"]) . '" data-posx="' . ($data["image"]["posx"]?$data["image"]["posx"]:'c') . '" data-posy="' . ($data["image"]["posy"]?$data["image"]["posy"]:'m') . '">', $html);
	$html = @str_replace('{title}', "<div style='color: ".$objBanner->getColorText()."; background-color: ".$objBanner->getBgText()." '>".(($data["title"])? implode("|",$data["title"]): $objBanner->getHeadText())."</div>", $html);
	$html = @str_replace('{subtitle}', "<div style='color: ".$objBanner->getColorText()."; background-color: ".$objBanner->getBgText()." '>".(($data["subtitle"])? implode("|",$data["subtitle"]): $objBanner->getSubHeadText())."</div>", $html);
	$html = @str_replace('{content}', implode("|",$data["content"]), $html);
	$html = @str_replace('{cta}', "<span style='color: ".$objBanner->getColorText()."'>".(($data["cta"]["text"])? implode("|",$data["cta"]["text"]): $objBanner->getCtaText())."</span>", $html);
	$html = @str_replace('{url}', "<span style='color: ".$objBanner->getColorText()."'>".(($data["url"])? implode("|",$data["url"]): $objBanner->getCtaLink())."</span>", $html);
	$html = @str_replace('{logo}', '<img src="' . ASSET_PATH . (($data["logo"]["urlV"])? $data["logo"]["urlV"] : $objBanner->getFileLogo()) . '" class="onlyV"><img src="' . ASSET_PATH . (($data["logo"]["urlH"])? $data["logo"]["urlH"] : $objBanner->getFileLogo()) . '" class="onlyH">', $html);
	$html = str_replace('{jap}', "日本語の場合はランダムに生成された文章以外に", $html);
   echo $html;
}

?>
